package de.tarent.invio.spring.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import de.tarent.invio.spring.model.AnalysedFingerprint;
import de.tarent.invio.spring.model.AnalysedTransmitter;
import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.InvioGeoPoint;
import de.tarent.sellfio.web.model.SignalSource;
import de.tarent.sellfio.web.model.WifiAp;
import de.tarent.sellfio.web.util.InvioGeoPointDeserializer;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FingerprintQualityTest {

    List<SignalSource> transmitters;
    List<Fingerprint> fingerprints;

    @Before
    public void setupTestData() {
        String wifiFps = "[{\"histogram\":{\"00:1b:8f:8a:82:90\":{\"-87\":0.5,\"-85\":0.5},\"00:1b:8f:92:72:b0\":{\"-10\":1.0}},\"id\":\"FP-1\",\"point\":{\"latitude\":50.709082,\"longitude\":7.056376}},{\"histogram\":{\"00:1b:8f:8a:82:90\":{\"-82\":0.5,\"-75\":0.5},\"00:1b:8f:92:72:b2\":{\"-81\":1.0}},\"id\":\"FP-2\",\"point\":{\"latitude\":50.714572,\"longitude\":7.056033}}]";
        fingerprints = deserializeFingerprints(wifiFps);

        String wifiTrans = "[\n" +
                "   {\n" +
                "      \"lat\" : \"50.72482423835371\",\n" +
                "      \"name\" : \"indoor 5\",\n" +
                "      \"lon\" : \"7.055930680216423\",\n" +
                "      \"height\" : \"180\",\n" +
                "      \"networks\" : [\n" +
                "         {\n" +
                "            \"channelNumber\" : \"11\",\n" +
                "            \"macAddress\" : \"00:1b:8f:8a:82:90\",\n" +
                "            \"band\" : \"2.4\",\n" +
                "            \"name\" : \"Indoor 5\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"vendor\" : \"EDIMAX\"\n" +
                "   },\n" +
                "   {\n" +
                "      \"lat\" : \"50.72997886420842\",\n" +
                "      \"name\" : \"indoor 4\",\n" +
                "      \"lon\" : \"7.05598298742654\",\n" +
                "      \"height\" : \"180\",\n" +
                "      \"networks\" : [\n" +
                "         {\n" +
                "            \"channelNumber\" : \"11\",\n" +
                "            \"macAddress\" : \"00:1b:8f:92:72:b0\",\n" +
                "            \"band\" : \"2.4\",\n" +
                "            \"name\" : \"Indoor 4\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"vendor\" : \"EDIMAX\"\n" +
                "   },\n" +
                "   {\n" +
                "      \"lat\" : \"50.73505601953079\",\n" +
                "      \"name\" : \"indoor 3\",\n" +
                "      \"lon\" : \"7.0650885489948125\",\n" +
                "      \"height\" : \"30\",\n" +
                "      \"networks\" : [\n" +
                "         {\n" +
                "            \"channelNumber\" : \"11\",\n" +
                "            \"macAddress\" : \"00:1b:8f:92:72:b2\",\n" +
                "            \"band\" : \"2.4\",\n" +
                "            \"name\" : \"Indoor 3\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"vendor\" : \"EDIMAX\"\n" +
                "   }]";

        Type barType = new TypeToken<ArrayList<WifiAp>>() {
        }.getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(InvioGeoPoint.class, new InvioGeoPointDeserializer());

        transmitters = gsonBuilder.create().fromJson(wifiTrans, barType);


    }

    public List<Fingerprint> deserializeFingerprints(String json){
        Type fooType = new TypeToken<ArrayList<Fingerprint>>() {
        }.getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(InvioGeoPoint.class, new InvioGeoPointDeserializer());
        return gsonBuilder.create().fromJson(json, fooType);
    }




    @Test
    public void testCalculateQualityMeasures() {
        FingerprintQuality quality = new FingerprintQuality();

        List<AnalysedFingerprint> calcWifi = quality.calculateQualityMeasuresGeneric(fingerprints, transmitters, 1.0);

        // two fingerprints with three aps each
        assertEquals(2, calcWifi.size());
        assertEquals(3, calcWifi.get(0).getData().size());
        assertEquals(3, calcWifi.get(1).getData().size());

        // test data for one ap
        AnalysedTransmitter transmitter = calcWifi.get(0).getData().get(1);
        assertEquals(-10.f, transmitter.getMean(), 0.f);
        assertEquals(0.f, transmitter.getStandardDeviation(), 0.f);
        assertEquals(2323.5f, transmitter.getDistance(), 0.6f);
        assertEquals(-0.0043f, transmitter.getMeanToDistanceRatio(), 0.0001f);
        assertEquals(2.f, transmitter.getConstancy(), 0.f);

        // test data for missing ap
        transmitter = calcWifi.get(1).getData().get(1);
        assertEquals(-1.f, transmitter.getMean(), 0.f);
        assertEquals(-1.f, transmitter.getStandardDeviation(), 0.f);
        assertEquals(-1.f, transmitter.getDistance(), 0.f);
        assertEquals(-1.f, transmitter.getMeanToDistanceRatio(), 0.f);
        assertEquals(-1.f, transmitter.getConstancy(), 0.f);

        // does not test the result automatically (if you know how to do it without big effort do it or tell)
        // but you can see, that there is no -1 in the resulting json, that it's valid and that -10.x is not replaced
        Gson gson = new Gson();
        String json = gson.toJson(calcWifi);

        json = json.replaceAll(":-1.0,", ":\"\",");
        json = json.replaceAll(":-1,", ":\"\",");
        json = json.replaceAll(":-1.0}", ":\"\"}");
        json = json.replaceAll(":-1}", ":\"\"}");
        // System.out.println(json);
    }

    @Test
    public void testScale() {
        FingerprintQuality quality = new FingerprintQuality();
        List<AnalysedFingerprint> calcWifi = quality.calculateQualityMeasuresGeneric(fingerprints, transmitters, 10.0);

        AnalysedTransmitter transmitter = calcWifi.get(0).getData().get(1);
        assertEquals(232.3, transmitter.getDistance(), 0.5f);
    }


    @Test
    public void testConstancy() {
        String testFps = "[" +
                         "{\"histogram\":{\"00:1b:8f:8a:82:90\":{\"-87\":0.5,\"-85\":0.5},\"00:1b:8f:92:72:b0\":{\"-10\":1.0}},\"id\":\"FP-1\",\"point\":{\"latitude\":50.709082,\"longitude\":7.056376}}," +
                         "{\"histogram\":{\"00:1b:8f:8a:82:90\":{\"-40\":0.5,\"-85\":0.5},\"00:1b:8f:92:72:b2\":{\"-81\":1.0}},\"id\":\"FP-2\",\"point\":{\"latitude\":50.714572,\"longitude\":7.056033}}," +
                         "{\"histogram\":{\"00:1b:8f:8a:82:90\":{\"-40\":0.2,\"-41\":0.7,\"-42\":0.1},\"00:1b:8f:92:72:b2\":{\"-81\":1.0}},\"id\":\"FP-3\",\"point\":{\"latitude\":50.714572,\"longitude\":7.056033}}," +
                         "{\"histogram\":{\"00:1b:8f:8a:82:90\":{\"-40\":0.333,\"-41\":0.333,\"-42\":0.333},\"00:1b:8f:92:72:b2\":{\"-81\":1.0}},\"id\":\"FP-3\",\"point\":{\"latitude\":50.714572,\"longitude\":7.056033}}]";

        fingerprints = deserializeFingerprints(testFps);
        FingerprintQuality quality = new FingerprintQuality();
        List<AnalysedFingerprint> calcWifi = quality.calculateQualityMeasuresGeneric(fingerprints, transmitters, 1.0);
        AnalysedTransmitter transmitter = calcWifi.get(0).getData().get(0);
        // 2 is assumed since 2*0.5+2*0.5=2 and 2 is the smallest scan number that
        // produces a valid integer when multiplied with any of the probabilities:
        assertEquals(2, transmitter.getConstancy());
        transmitter = calcWifi.get(0).getData().get(1);
        // 2 is assumed here because the denormalization method contains a loop starts with 2 as its lowest iterator.
        // This is quite stupid, since it causes these to transmitters to be equal in constancy, which is extremely inaccurate:
        assertEquals(2, transmitter.getConstancy());
         transmitter = calcWifi.get(1).getData().get(0);
        assertEquals(2, transmitter.getConstancy());
         transmitter = calcWifi.get(2).getData().get(0);
        assertEquals(10, transmitter.getConstancy());
        transmitter = calcWifi.get(3).getData().get(0);
        assertEquals(3, transmitter.getConstancy());

    }

}
