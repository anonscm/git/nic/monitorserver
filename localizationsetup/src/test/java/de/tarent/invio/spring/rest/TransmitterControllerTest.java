package de.tarent.invio.spring.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tarent.invio.spring.AbstractTest;
import de.tarent.sellfio.web.dao.MapDAO;
import de.tarent.sellfio.web.dao.TransmitterDAO;
import de.tarent.sellfio.web.model.Map;
import de.tarent.sellfio.web.model.NetworkNode;
import de.tarent.sellfio.web.model.Transmitter;
import de.tarent.sellfio.web.model.WifiAp;
import de.tarent.sellfio.web.util.MapServerUtils;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;

/**
 * Created by mley on 30.10.15.
 */
@Transactional("txName")
public class TransmitterControllerTest extends AbstractTest {

    @Autowired
    TransmitterController transmitterController;

    @Autowired
    MapDAO mapDAO;

    @Autowired
    TransmitterDAO transmitterDAO;

    @Autowired
    SessionFactory sessionFactory;

    private final String mapName = "tarent3og";

    @Before
    public void setup() throws IOException {

        MapServerUtils mapServerUtils = Mockito.spy(new MapServerUtils());
        doAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                String url = (String) invocation.getArguments()[0];
                String filename  = url.substring(url.lastIndexOf("/")+1);


                StringWriter sw = new StringWriter();
                InputStream inputStream = ClassLoader.getSystemResourceAsStream(filename);

                if(inputStream == null) {
                    throw new IOException("FAILZ. File not found");
                }

                org.apache.commons.io.IOUtils.copy(inputStream, sw);
                return sw.toString();
            }
        }).when(mapServerUtils).downloadTextFile(anyString());

        ReflectionTestUtils.setField(transmitterController, "mapServerUtils", mapServerUtils);

        Map map = new Map(mapName);
        mapDAO.store(map);

        String[] macAddresses = new String[]{
                "80:1f:02:a7:40:5c",
                "80:1f:02:a7:42:e6",
                "80:1f:02:a7:3f:96",
                "00:1c:57:51:60:d0",
                "00:1c:57:51:60:d2",
                "00:1c:57:51:60:d3",
                "00:1c:57:41:75:50",
                "00:1c:57:41:75:52",
                "00:1c:57:41:75:53",
                "00:1c:57:51:60:d1",
                "00:1c:57:41:75:51",
                "00:1b:8f:92:72:b0",
                "00:1b:8f:92:72:b2",
                "00:1b:8f:92:72:b3",
                "00:1b:8f:8a:82:90",
                "00:1b:8f:8a:82:92",
                "00:1b:8f:8a:82:93",
                "A0:1b:8f:92:72:b1", // remaining unknown APS
                "A0:1b:8f:8a:82:91",

        };

        for (String m : macAddresses) {
            Transmitter t = new Transmitter(map.getId(), true, "wifi", m);
            transmitterDAO.save(t);
        }


        String identFragment = "bec26202-a8d8-4a94-b0fc-9ac1de37daa6_10_";
        for (int i = 3; i <= 13; i++) {
            Transmitter t = new Transmitter(map.getId(), true, "btle", identFragment + i);
            transmitterDAO.save(t);
        }

    }

    @After
    public void cleanUp() {
        sessionFactory.getCurrentSession().createQuery("delete from BtleTransmitter b").executeUpdate();
        sessionFactory.getCurrentSession().createQuery("delete from NetworkNode n").executeUpdate();
        sessionFactory.getCurrentSession().createQuery("delete from WifiAp b").executeUpdate();
        sessionFactory.getCurrentSession().createQuery("delete from Transmitter t").executeUpdate();

        sessionFactory.getCurrentSession().createQuery("delete from Map").executeUpdate();
    }

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testReplaceEmpty() {
        TransmitterController.Transmitters transmitters1 = transmitterController.getTransmitters(mapName);

        transmitterController.replaceTransmitters(mapName, transmitters1);

        TransmitterController.Transmitters transmitters2 = transmitterController.getTransmitters(mapName);

        assertEquals(transmitters1.unknownBtle.size(), transmitters2.unknownBtle.size());
        assertEquals(transmitters1.unknownWifi.size(), transmitters2.unknownWifi.size());
    }

    @Test
    public void testReplace() throws IOException {
        testLoadAndSave();

        TransmitterController.Transmitters transmitters = transmitterController.getTransmitters(mapName);

        WifiAp ap = new WifiAp();
        ap.setName("WLXAP-9002");
        ap.setHeight(200);
        ap.setVendor("Zisqo");
        ap.getNetworks().add(new NetworkNode(transmitters.unknownWifi.get(0)));
        ap.getNetworks().add(new NetworkNode(transmitters.unknownWifi.get(1)));

        transmitters.wifi.clear();

        transmitters.wifi.add(ap);

        transmitters = transmitterController.replaceTransmitters(mapName, transmitters);

        assertEquals(1, transmitters.wifi.size());

        ap = transmitters.wifi.get(0);

        assertEquals("WLXAP-9002", ap.getName());
    }

    @Test
    public void testLoadAndSave() throws IOException {
        TransmitterController.Transmitters transmitters = transmitterController.getTransmitters(mapName);

        //simulate wire
        String json = objectMapper.writeValueAsString(transmitters);
        transmitters = objectMapper.readValue(json, TransmitterController.Transmitters.class);

        WifiAp ap = new WifiAp();
        ap.setName("WLXAP-9001");
        ap.setHeight(200);
        ap.setVendor("D-Link");
        ap.getNetworks().add(new NetworkNode(transmitters.unknownWifi.get(0)));
        ap.getNetworks().add(new NetworkNode(transmitters.unknownWifi.get(1)));
        ap.getNetworks().add(new NetworkNode(transmitters.unknownWifi.get(2)));

        transmitters.wifi.add(ap);

        transmitters = transmitterController.replaceTransmitters(mapName, transmitters);

        assertEquals(1, transmitters.wifi.size());

        ap = transmitters.wifi.get(0);

        assertEquals("WLXAP-9001", ap.getName());
        assertEquals(3, ap.getNetworks().size());
        assertEquals("80:1f:02:a7:40:5c", ap.getNetworks().get(0).getMacAddress());

    }

    @Test
    public void testImport() throws IOException {

        TransmitterController.Transmitters transmitters = transmitterController.getTransmitters(mapName);
        assertEquals(0, transmitters.wifi.size());
        assertEquals(0, transmitters.btle.size());

        assertEquals(19, transmitters.unknownWifi.size());
        assertEquals(11, transmitters.unknownBtle.size());

        transmitterController.importData(mapName);

        transmitters = transmitterController.getTransmitters(mapName);

        assertEquals(5, transmitters.wifi.size());
        assertEquals(11, transmitters.btle.size());

        assertEquals(2, transmitters.unknownWifi.size());
        assertEquals(2, transmitters.unknownBtle.size());

    }


}
