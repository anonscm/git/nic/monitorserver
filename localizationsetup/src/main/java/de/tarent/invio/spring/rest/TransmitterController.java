package de.tarent.invio.spring.rest;

import de.tarent.sellfio.web.dao.MapDAO;
import de.tarent.sellfio.web.dao.TransmitterDAO;
import de.tarent.sellfio.web.model.BtleTransmitter;
import de.tarent.sellfio.web.model.Map;
import de.tarent.sellfio.web.model.NetworkNode;
import de.tarent.sellfio.web.model.Transmitter;
import de.tarent.sellfio.web.model.WifiAp;
import de.tarent.sellfio.web.util.MapServerUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by mley on 30.10.15.
 */
@RestController
@RequestMapping("/transmitter")
public class TransmitterController {

    @Autowired
    private TransmitterDAO transmitterDAO;

    @Autowired
    private MapDAO mapDAO;

    @Autowired
    private MapServerUtils mapServerUtils;

    @Value("${btle.uuid}")
    private String uuid;

    /**
     * Get wifi and btle transmitters for a map.
     * @param map name of the map
     * @return Transmitters object.
     */
    @RolesAllowed("")
    @RequestMapping(method = RequestMethod.GET, path = "/{map}")
    public Transmitters getTransmitters(@PathVariable("map") String map) {
        final Transmitters result = new Transmitters();

        result.mapId = mapDAO.getByName(map).getId();
        result.btle = transmitterDAO.getIBeacons(map);
        result.wifi = transmitterDAO.getWifiAps(map);


        final Set<Transmitter> knownTransmitters = new HashSet<>();

        result.unknownBtle = transmitterDAO.getTransmitters(map, "btle");
        result.unknownWifi = transmitterDAO.getTransmitters(map, "wifi");

        if(result.btle != null ) {
            for (BtleTransmitter b : result.btle) {
                knownTransmitters.add(b.getTransmitter());
            }
            removeKnownTransmitters(result.unknownBtle, knownTransmitters);
            knownTransmitters.clear();
        }

        if(result.wifi != null) {
            for (WifiAp w : result.wifi) {
                for (NetworkNode n : w.getNetworks()) {
                    knownTransmitters.add(n.getTransmitter());
                }
            }
            removeKnownTransmitters(result.unknownWifi, knownTransmitters);
        }

        return result;

    }

    private void removeKnownTransmitters(List<Transmitter> unknown, Set<Transmitter> knownTransmitters) {
        for(final Iterator<Transmitter> i = unknown.iterator(); i.hasNext();) {
            if(knownTransmitters.contains(i.next())) {
                i.remove();
            }
        }
    }

    /**
     * Creates new or updates existing transmitters.
     * @param map name of the map
     * @param t the Transmitters object.
     * @return The saved Transmitters object.
     */
    @RolesAllowed("")
    @RequestMapping(method = RequestMethod.POST, path = "/{map}")
    public Transmitters replaceTransmitters(@PathVariable("map") String map, @RequestBody Transmitters t) {
        final Map m = mapDAO.getByName(map);

        transmitterDAO.removeAllSignalSources(map);

        for (BtleTransmitter btle : t.btle) {
            btle.getTransmitter().setMap_id(m.getId());
            transmitterDAO.save(btle);
        }
        for (WifiAp wifi : t.wifi) {
            wifi.setMap_id(m.getId());
            for (NetworkNode n : wifi.getNetworks()) {
                n.getTransmitter().setMap_id(m.getId());
            }
            transmitterDAO.save(wifi);
        }

        return getTransmitters(map);
    }


    /**
     * Import existing transmitter information from json files and store it in the DB.
     * @param map name of the map
     * @return array of imported transmitter types or null if map does not exist.
     * @throws IOException when loading json from mapserver fails
     */
    @RolesAllowed("")
    @RequestMapping(method = RequestMethod.GET, path = "/import/{map}")
    public List<String> importData(@PathVariable("map") String map) throws IOException {
        final Map m = mapDAO.getByName(map);
        if (m == null) {
            return null;
        }
        final List<String> result = new ArrayList<>();

        final List<WifiAp> existingAps = transmitterDAO.getWifiAps(map);
        if (existingAps.size() == 0) {
            importWifi(m);
            result.add("wifi");
        }

        final List<BtleTransmitter> existingIBeacons = transmitterDAO.getIBeacons(map);
        if (existingIBeacons.size() == 0) {
            importBtle(m);
            result.add("btle");
        }


        return result;

    }

    private Transmitter getOrCreateTransmitter(Map map, String type, String identifier) {
        Transmitter t = transmitterDAO.getTransmitterByMapIdTypeIdentifier(map.getId(), type, identifier);
        if (t == null) {
            t = new Transmitter(map.getId(), true, type, identifier);
        }
        return t;
    }

    private void importBtle(Map map) throws IOException {
        final List<BtleTransmitter> btleTransmitters = mapServerUtils.getBtleTransmitters(map.getName());

        for (BtleTransmitter b : btleTransmitters) {
            if(b.getUuid() == null || b.getUuid().trim().isEmpty()) {
                b.setUuid(uuid);
            }
            final String identifier = b.getUuid() + "_" + b.getMajorId() + "_" + b.getMinorId();
            final Transmitter t = getOrCreateTransmitter(map, "btle", identifier);
            b.setTransmitter(t);
            transmitterDAO.save(b);
        }
    }


    private void importWifi(Map map) throws IOException {
        final List<WifiAp> wifiTransmitters = mapServerUtils.getWifiTransmitters(map.getName());

        for (WifiAp ap : wifiTransmitters) {
            ap.setMap_id(map.getId());
            ap.setId(0);
            for (NetworkNode n : ap.getNetworks()) {
                final Transmitter t =getOrCreateTransmitter(map, "wifi", n.getMacAddress());
                n.setTransmitter(t);
            }
            transmitterDAO.save(ap);
        }

    }

    /**
     * Simple Response object class.
     */
    @Data
    public static class Transmitters {
        List<BtleTransmitter> btle;
        List<WifiAp> wifi;
        List<Transmitter> unknownBtle;
        List<Transmitter> unknownWifi;
        int mapId;

    }
}
