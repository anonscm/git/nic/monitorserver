package de.tarent.invio.spring.model;

import lombok.Data;

import java.util.List;

/**
 * An AnalysedFingerprint corresponds to the (logical, i.e. multi-) lines in our analysis table. It contains the data
 * for all the columns, i.e. the transmitters of one type (wifi/btle).
 */
@Data
public class AnalysedFingerprint {

    /**
     * Analyis data for each Transmitter that was visible at this Fingerprint.
     */
    private List<AnalysedTransmitter> data;

    /**
     * The latitude of this Fingerprint.
     */
    private double lat;
    /**
     * The longitude of this Fingerprint.
     */
    private double lon;

    /**
     * The ID of this Fingerprint.
     */
    private String id;


    /**
     * Construct a new AnalysedFingerprint initialising all fields at once.
     * @param id {@link #id}
     * @param lat {@link #lat}
     * @param lon {@link #lon}
     * @param data {@link #data}
     */
    public AnalysedFingerprint(String id, double lat, double lon, List<AnalysedTransmitter> data) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.data = data;
    }

}

