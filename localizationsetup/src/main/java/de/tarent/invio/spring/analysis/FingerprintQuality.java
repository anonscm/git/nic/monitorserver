package de.tarent.invio.spring.analysis;

import com.google.gson.Gson;
import de.tarent.invio.spring.model.AnalysedFingerprint;
import de.tarent.invio.spring.model.AnalysedTransmitter;
import de.tarent.sellfio.web.dao.FingerprintDAO;
import de.tarent.sellfio.web.dao.TransmitterDAO;
import de.tarent.sellfio.web.model.BtleTransmitter;
import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.InvioGeoPoint;
import de.tarent.sellfio.web.model.NetworkNode;
import de.tarent.sellfio.web.model.SignalSource;
import de.tarent.sellfio.web.model.WifiAp;
import de.tarent.sellfio.web.util.FingerprintsUtil;
import de.tarent.sellfio.web.util.MapServerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * This class does interesting calculations on the fingerprint and transmitter data that can then be sent to the
 * monitoring frontend.
 */
@Component
public class FingerprintQuality {

    /**
     * Pattern to match fingerprint id format.
     */
    private static final Pattern FP_PATTERN = Pattern.compile("FP-\\d+");

    @Autowired
    private MapServerUtils mapServerUtils;

    @Autowired
    private FingerprintDAO fingerprintDAO;

    @Autowired
    private TransmitterDAO transmitterDAO;

    /**
     * Get a JSON representation of the fingerprint quality calculations regarding the WIFI access points.
     *
     * @param mapName the name of the map (on server ${map.baseurl}, see {@link MapServerUtils})
     * @return a plain JSON string
     * @throws IOException when not all map resources can be found
     */
    public String getJsonWifi(String mapName) throws IOException {
        final Gson gson = new Gson();
        String json = gson.toJson(calculateWifiQuality(mapName));

        json = fixMinusOneValues(json);

        return json;
    }


    /**
     * Get a JSON representation of the fingerprint quality calculations regarding the BTLE beacons.
     *
     * @param mapName the name of the map (on server ${map.baseurl}, see {@link MapServerUtils})
     * @return a plain JSON string
     * @throws IOException when not all map resources can be found
     */
    public String getJsonBtle(String mapName) throws IOException {
        final Gson gson = new Gson();
        String json = gson.toJson(calculateBtleQuality(mapName));

        json = fixMinusOneValues(json);

        return json;
    }

    private String fixMinusOneValues(String json) {
        json = json.replaceAll(":-1.0,", ":\"\",");
        json = json.replaceAll(":-1,", ":\"\",");
        json = json.replaceAll(":-1.0}", ":\"\"}");
        json = json.replaceAll(":-1}", ":\"\"}");

        return json;
    }


    private List<AnalysedFingerprint> calculateWifiQuality(String mapName) throws IOException {
        List<Fingerprint> fpsWifi = fingerprintDAO.getByMapAndType(mapName, "wifi", false);
        fpsWifi = FingerprintsUtil.getFingerprintsWithCalculatedProbability(fpsWifi);
        final List<? extends SignalSource> aps = transmitterDAO.getWifiAps(mapName);
        final double scale = mapServerUtils.getScale(mapName);
        final List<AnalysedFingerprint> calcWifi = calculateQualityMeasuresGeneric(fpsWifi, aps, scale);
        return calcWifi;
    }


    private List<AnalysedFingerprint> calculateBtleQuality(String mapName) throws IOException {
        List<Fingerprint> fpsBtle = fingerprintDAO.getByMapAndType(mapName, "btle", false);
        fpsBtle = FingerprintsUtil.getFingerprintsWithCalculatedProbability(fpsBtle);
        final List<? extends SignalSource> ibeacons = transmitterDAO.getIBeacons(mapName);
        final double scale = mapServerUtils.getScale(mapName);
        final List<AnalysedFingerprint> calcBtle = calculateQualityMeasuresGeneric(fpsBtle, ibeacons, scale);
        return calcBtle;
    }


    /**
     * The calculations are the same for any type of transmitter, WIFI or BTLE.
     *
     * @param fingerprints the fingerprints
     * @param transmitters the transmitters
     * @param scale        the scale of the map (from the osm-file; necessary to get the distances right)
     * @return List with one AnalysedFingerprint for each input Fingerprint.
     */
    public List<AnalysedFingerprint> calculateQualityMeasuresGeneric(List<Fingerprint> fingerprints,
                                                                     List<? extends SignalSource> transmitters,
                                                                     double scale) {

        final List<AnalysedFingerprint> analysed = new ArrayList<>();

        boolean onlyFPIds = true;
        for (Fingerprint fp : fingerprints) {
            final List<AnalysedTransmitter> translist = analyseAllTransmittersForFingerprint(transmitters, fp, scale);
            final AnalysedFingerprint tmpFp = new AnalysedFingerprint(fp.getId(),
                    fp.getPoint().getLatitudeE6() / 1E6,
                    fp.getPoint().getLongitudeE6() / 1E6,
                    translist);
            analysed.add(tmpFp);

            // check if fingerprint ids are in the form of "FP-1"
            if (onlyFPIds && !(FP_PATTERN.matcher(fp.getId()).matches()) ) {
                onlyFPIds = false;
            }
        }

        sortFingerprints(analysed, onlyFPIds);

        return analysed;
    }

    /**
     * Sorts the list of fingerprints by their IDs.
     *
     * @param fingerprints List of fingerprints
     * @param properFPIds  flag that all fingerprint IDs are the pattern "FP-\d+"
     */
    private void sortFingerprints(List<AnalysedFingerprint> fingerprints, final boolean properFPIds) {
        Collections.sort(fingerprints, new Comparator<AnalysedFingerprint>() {

            @Override
            public int compare(AnalysedFingerprint fp1, AnalysedFingerprint fp2) {

                // if all fingerprint ids are in proper format, we can sort by the numeric part of the id
                if (properFPIds) {
                    final int id1 = Integer.parseInt(fp1.getId().substring(3));
                    final int id2 = Integer.parseInt(fp2.getId().substring(3));

                    return Integer.compare(id1, id2);
                }

                // else we just sort alphabetically
                return fp1.getId().compareTo(fp2.getId());
            }
        });
    }

    private List<AnalysedTransmitter> analyseAllTransmittersForFingerprint(List<? extends SignalSource> transmitters,
                                                                           Fingerprint fp,
                                                                           double scale) {
        final List<AnalysedTransmitter> translist = new ArrayList<>();

        if (transmitters != null) {
            for (SignalSource transmitter : transmitters) {

                if (transmitter instanceof BtleTransmitter) {
                    final BtleTransmitter ibeacon = (BtleTransmitter) transmitter;
                    final String combiId =ibeacon.getTransmitter().getIdentifier();
                    translist.add(calculateQualityMeasures(combiId, combiId, ibeacon.getLat(), ibeacon.getLon(),
                            fp, scale));
                } else if (transmitter instanceof WifiAp) {
                    final WifiAp w = (WifiAp) transmitter;
                    for (NetworkNode n : w.getNetworks()) {
                        translist.add(calculateQualityMeasures(n.getName(), n.getMacAddress(), w.getLat(), w.getLon(),
                                fp, scale));
                    }
                }

            }
        }
        return translist;
    }


    /**
     * Calculate the quality measures for one fingerprint, with the relevant transmitter details already extracted.
     * The name/id split is due to the wifi transmitters supporting multiple (virtual) networks on different mac
     * addresses.
     *
     * @param name  the name of the transmitter as it shall be displayed
     * @param id    the ID of the network of the transmitter as it is named in the histogram
     * @param lat   latitude of the transmitter
     * @param lon   longitude of the transmitter
     * @param fp    the fingerprint
     * @param scale the scale of the map (from the osm-file; necessary to get the distances right)
     * @return the AnalysedTransmitter. If the transmitter was not visible in the fingerprint at all then all numerical
     * values will be -1, so that it can be filtered out.
     */
    public AnalysedTransmitter calculateQualityMeasures(String name,
                                                        String id,
                                                        double lat,
                                                        double lon,
                                                        Fingerprint fp,
                                                        double scale) {

        AnalysedTransmitter tmpTrans;
        final Map<Integer, Float> histogram = fp.getHistogram().get(id);

        if (histogram != null) {
            // calculate mean
            float mean = 0;
            final Set<Integer> keys = histogram.keySet();
            for (Integer db : keys) {
                // already normalized
                mean += (float) db * fp.getHistogram().get(id).get(db);
            }

            // calculate standard deviation
            float variance = 0;
            for (Integer db : keys) {
                variance += ((float) db - mean) * ((float) db - mean);
            }

            variance = variance / keys.size();
            final double stddev = Math.sqrt(variance);

            // calculate distance
            final InvioGeoPoint transPoint = new InvioGeoPoint(lat, lon);
            final double distance = fp.getPoint().calculateDistanceTo(transPoint) / scale;

            // calculate meanToDistanceRatio
            final double ratio = mean / distance;

            // calculate constancy, TODO: this is currently rather speculative/useless. Can be fixed when we have
            //                            absolute counts of the measured signal strengths.
            final int constancy = denormalize(fp.getHistogram().get(id));

            tmpTrans = new AnalysedTransmitter(id, name, mean, stddev, distance, ratio, constancy, lat, lon);
        } else {
            tmpTrans = new AnalysedTransmitter(id, name, -1, -1, -1, -1, -1, lat, lon);
        }
        return tmpTrans;
    }


    private int denormalize(Map<Integer, Float> values) {
        // for "faking" constancy, should be saved in the future unnormalized
        final int numberOfScans = 15; // see AdminFingerprintManager class in admin app
        int factor = 0;

        final Set<Integer> keys = values.keySet();
        for (int i = 2; i <= numberOfScans; i++) {
            boolean foundFactor = true;
            for (Integer db : keys) {
                final float b = i * values.get(db);
                final float r = Math.abs(b - Math.round(b));
                if (r > 0.001f) {
                    foundFactor = false;
                    break;
                }
            }

            if (foundFactor) {
                factor = i;
                break;
            }
        }

        int result = 0;
        for (Integer db : keys) {
            result += Math.round(values.get(db) * factor);
        }

        return result;
    }


    /**
     * Overwrite the injectes MapServerUtils for testing purposes.
     *
     * @param mapServerUtils the new MapServerUtils
     */
    public void setMapServerUtils(MapServerUtils mapServerUtils) {
        this.mapServerUtils = mapServerUtils;
    }

}
