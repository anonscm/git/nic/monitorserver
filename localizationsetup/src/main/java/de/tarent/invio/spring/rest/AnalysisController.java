package de.tarent.invio.spring.rest;

import de.tarent.invio.spring.analysis.FingerprintQuality;
import de.tarent.sellfio.web.util.MapServerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;

/**
 * The AnalysisController provides analysis data about the fingerprints and transmitters of a specific map.
 */
@RestController
@RequestMapping("/analysis")
public class AnalysisController {

    @Autowired
    private FingerprintQuality calculations;

    /**
     * @param map  the name of the map.
     * @param type the type of the fingerprints, either "WIFI" or "BTLE".
     * @return the JSON string containing the analysis results.
     * See https://evolvis.org/plugins/mediawiki/wiki/nic/index.php/JSON-Formats#Mapserver_Output
     * @throws IOException when not all resources can be found
     */
    @RolesAllowed("")
    @RequestMapping(method = RequestMethod.GET)
    public String getCalculations(@RequestParam("map") String map, @RequestParam("type") String type)
            throws IOException {
        String result = null;

        if (MapServerUtils.WIFI.equalsIgnoreCase(type)) {
            result = calculations.getJsonWifi(map);
        } else if (MapServerUtils.BTLE.equalsIgnoreCase(type)) {
            result = calculations.getJsonBtle(map);
        }

        return result;
    }

}
