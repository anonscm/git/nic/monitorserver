package de.tarent.invio.spring.model;

import lombok.Data;

/**
 * An AnalysedTransmitter contains the analysis data for a transmitter regarding a single
 * fingerprint (see AnalysedFingerprint).
 * Let's call this the "parent fingerprint". It is not backreferenced here, but it will own this object.
 * <p/>
 * TODO: the fields that could be empty should probably be objects so that we can construct the AnalysedTransmitter
 * without them, instead of setting them to -1. That would make it more correct and easier for the JS table in
 * the frontend. On the other hand... the frontend can filter them out easily.
 */
@Data
public class AnalysedTransmitter {

    private String id;

    /**
     * Name of the Transmitter (e.g. wifi SSID)
     */
    private String name;


    /**
     * The mean of the strengths that were measured for this transmitter at the ("parent"-) fingerprint.
     */
    private float mean;

    /**
     * The standard deviation of the measured signal strengths as measured at the ("parent"-) fingerprint.
     * See https://de.wikipedia.org/wiki/Standardabweichung
     */
    private double standardDeviation;

    /**
     * The real distance between this transmitter and it's ("parent"-) fingerprint as calculated with the given data
     * in the respective files from the mapserver.
     */
    private double distance;

    /**
     * What the name says. Might be of limited use, because the strength is not linear.
     */
    private double meanToDistanceRatio;


    /**
     * At the moment a pretty speculative value, which indicates how often this transmitter might have been seen at
     * the ("parent"-) fingerprint.
     * TODO: when we store the absolute number of measurements that showed a specific signal strength in our
     * fingerprints, instead of only the ratio, then we can use that to calculate the real constancy ratio (i.e.
     * "times visible" / "number of scans").
     */
    private int constancy;

    /**
     * Latitude of this transmitters position.
     */
    private double lat;
    /**
     * Longitude of this transmitters position.
     */
    private double lon;

    /**
     * Construct a new AnalysedTransmitter, initialising all fields at once.
     *
     * @param id                  {@link #id}
     * @param mean                {@link #mean}
     * @param standardDeviation   {@link #standardDeviation}
     * @param distance            {@link #distance}
     * @param meanToDistanceRatio {@link #meanToDistanceRatio}
     * @param constancy           {@link #constancy}
     * @param lat
     * @param lon
     */
    public AnalysedTransmitter(String id,
                               String name,
                               float mean,
                               double standardDeviation,
                               double distance,
                               double meanToDistanceRatio,
                               int constancy, double lat, double lon) {
        this.id = id;
        this.name = name;
        this.mean = mean;
        this.standardDeviation = standardDeviation;
        this.distance = distance;
        this.meanToDistanceRatio = meanToDistanceRatio;
        this.constancy = constancy;
        this.lat = lat;
        this.lon = lon;
    }

}
