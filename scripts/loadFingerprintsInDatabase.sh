# Re-upload all fingerprint files to the mapserver so that it can now
# store the fingerprints in the database. See SEL-319. The files can
# be ignored after that but are not deleted for safety reasons.
#
# Skript (or rather the mapserver) is idempotent, has no parameters and uses no relative paths.

for file in /var/lib/tomcat7/webapps/maps/*/
do

echo "Loading " $file "..."
curl -F "fingerprintsType=wifi"  -F "mapName=`basename $file`" -F "fingerprintsJson=<$file/fingerprints/fingerprints_data.json" http://localhost:8080/mapserver/fingerprints/upload
curl -F "fingerprintsType=btle"  -F "mapName=`basename $file`" -F "fingerprintsJson=<$file/fingerprints/fingerprints_btle_data.json" http://localhost:8080/mapserver/fingerprints/upload
echo $file " loaded!\n"

done
