// fix for missing window.location.origin in IE
if (!window.location.origin) {
    window.location.origin = window.location.protocol + "//" + window.location.host;
}

var serverEndpoint = window.location.origin + '/localizationsetup/';

var mapserverEndpoint = window.location.origin + '/mapserver/';

var SELLFIO_DEBUG = false;
if (SELLFIO_DEBUG) {
serverEndpoint = 'http://invio-mapserver.lan.tarent.de:8080/localizationsetup/';
    //In case in debug mode the mapserver is in a different machine, then change localhost with the mapserver's IP
    mapserverEndpoint = 'http://invio-mapserver.lan.tarent.de:8080/mapserver/';
}

var angularApp = angular.module('angularApp', ['ngRoute', 'ngCookies', 'ui.bootstrap', 'ui.bootstrap.datetimepicker',
    'ui-rangeSlider']);

// when window is resized, calculate the optimal size for the table and the map
$(window).resize(function () {

    var bodyRect = document.body.getBoundingClientRect();
    var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)

    var tables = $('.table-scroll');
    var map = $('#l10nMap');

    var tableRect = tables[0].getBoundingClientRect();
    var tableOffset = tableRect.top - bodyRect.top;

    var space = viewportHeight - tableOffset - 90;

    tables.height(space)

    var localizationRect = $('#localizationTable')[0].getBoundingClientRect();
    var l10nOffset = localizationRect.top - bodyRect.top;
    space = viewportHeight - l10nOffset - 60;

    // fallback if table and map are not next to each other
    if (space < 200) {
        space = '70vh'
    }

    //TODO detect if table and map are not next to each other and set sizes accordingly

    map.height(space);
});

