angularApp.controller('TmMapController', function ($scope, $rootScope, $filter, $http, $timeout, config, leaflet) {

    $scope.data = null;

    /** array of transmitter symbols currently displayed on the map */
    var apMarkers = [];

    /** leaflet map object */
    var map = null;

    /** leaflet map object */
    var map = null;
    var currentMap = null;

    var lf = leaflet();

    var icons = {
        wifi: L.icon({
            iconUrl: 'images/wifi_48x32.png',
            iconSize: [48, 32],
            iconAnchor: [24, 16],
            popupAnchor: [0, 0]
        }),
        btle: L.icon({
            iconUrl: 'images/btle_48x32.png',
            iconSize: [48, 32],
            iconAnchor: [24, 16],
            popupAnchor: [0, 0]
        })
    };

    function initMap() {
        var cfg = config();

        if (cfg.selectedMap === currentMap) {
            return;
        }

        currentMap = cfg.selectedMap;

        lf.loadMapConfig(cfg.selectedMap, function () {
            // clean up old map
            var mapElement = document.getElementById('l10nMap');
            var parent = mapElement.parentElement;
            parent.removeChild(mapElement);

            // create new map element
            var newMapElement = document.createElement('div');
            newMapElement.id = 'l10nMap';
            parent.appendChild(newMapElement);

            // create new leaflet map
            map = L.map('l10nMap', {
                attributionControl: false
            });


            // add tile layer
            L.tileLayer(cfg.tileUrl, {
                minZoom: cfg.minZoom,
                maxZoom: cfg.maxZoom,
                tms: true
            }).addTo(map);

            // center map properly
            var center = [
                (cfg.boundingBox[0][0] + cfg.boundingBox[1][0]) / 2, (cfg.boundingBox[0][1] + cfg.boundingBox[
                    1][1]) / 2
            ];
            map.setView(center);
            map.setMaxBounds(cfg.boundingBox);
            map.fitBounds(cfg.boundingBox);

            if ($scope.data != null) {
                showTransmitters();
            }

        });

    }



    function showTransmitters() {
        addAPsToMap($scope.data.wifi, icons.wifi);
        addAPsToMap($scope.data.btle, icons.btle);
    }



    function addMarker(ap, icon) {
        var apMarker = L.marker([ap.lat, ap.lon], {
            icon: icon,
            title: ap.name,
            draggable: true
        });
        apMarker.addTo(map);
        apMarker.ap = ap;

        apMarker.on('click', function(e){
            $rootScope.$broadcast('SELECTED_TRANSMITTER', e.target.ap);
            $scope.$apply();
        });

        apMarker.on('dragend', function (e) {
            e.target.ap.lat = e.target._latlng.lat;
            e.target.ap.lon = e.target._latlng.lng;
            $scope.saveState = 'dirty';
            $rootScope.$broadcast('SAVESTATECHANGE', 'dirty');
            $scope.$apply();
        });
        apMarkers.push(apMarker);

    }

    /**
     * adds the given transmitters to the map
     * @param aps map of transmitters
     * @param icon icon to use
     */
    function addAPsToMap(aps, icon) {
        //hideAps();

        aps.forEach(function(ap) {
            addMarker(ap, icon);
        });

    }

    /**
     * removes all transmitter icons from the map
     */
    function hideAps() {
        if (map != null) {
            apMarkers.forEach(function (marker) {
                map.removeLayer(marker);
            });

            apMarkers.length = 0;
        }
    }

    $scope.$on('tmDataLoaded', function (event, data) {
        hideAps();
        $scope.data = data;
        if (map != null) {
            showTransmitters();
        }
    });

    function disableCrosshair() {
        $('.leaflet-container').css('cursor','');
        map.off('click', recordClick);
    }

    function recordClick(e) {
        disableCrosshair();

        newTransmitter.lat = e.latlng.lat;
        newTransmitter.lon = e.latlng.lng;

        addMarker(newTransmitter, icons[newTransmitterType]);

        $rootScope.$broadcast('NEW_TRANSMITTER', {transmitter: newTransmitter, type: newTransmitterType});
        $rootScope.$apply();
    }

    var newTransmitterType;
    var newTransmitter;

    $scope.$on('ADD_TRANSMITTER', function (event, transmitterType, transmitter) {
        newTransmitterType = transmitterType;
        newTransmitter = transmitter;
        $('.leaflet-container').css('cursor','crosshair');
        map.on("click", recordClick);
    });

    $scope.$on('CANCEL_ADD_TRANSMITTER', disableCrosshair);

    $scope.$on('MAPSELECTED', function () {
        map = null;
        data = null;
        initMap();
    });

    $scope.whenReady(function () {
        initMap();
    });

});
