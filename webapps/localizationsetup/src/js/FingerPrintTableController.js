angularApp.controller('FingerPrintTableController', function ($scope, $rootScope, $filter, $http, $timeout, $compile,
                                                              $window, config, FingerprintService) {

    $scope.selectedAllRows = true;
    $scope.selectedAllColumns = true;

    $scope.fpTables = [];
    $scope.transmitterActiveWifi = [];
    $scope.transmitterActiveBtle = [];
    $scope.selectedFpTable = 'wifi';

    var tabs;

    /**
     * Function to format a value in the fingerprints table
     * @param column name of the column, the value shall be display
     * @param value the value as number
     * @returns {*} the formatted value
     */
    var formatValue = function (column, value) {
        // all values shall have two decimal points, except constancy, which is an integer
        if (column !== 'constancy') {
            value = $filter('number')(value, 2)
        }

        return value;
    };

    var initTable = function (table) {
        table.setSubColumnAlias('mean', 'MEAN', 'mean signal strength in db');
        table.setSubColumnAlias('standardDeviation', 'SD', 'standard deviation of signal strength');
        table.setSubColumnAlias('distance', 'DIST', 'distance to signal transmitter in m');
        table.setSubColumnAlias('meanToDistanceRatio', 'M/DIST', 'ratio of mean signal strength to distance');
        table.setSubColumnAlias('constancy', 'CON', 'constancy');
        table.setSubColumnAlias('lat', 'LAT', "latitude")
        table.setSubColumnAlias('lon', 'LNG', "longitude")
        table.selectAllSubColumns(true);
        table.unselectSubColumn('lat');
        table.unselectSubColumn('lon');
        table.update();
    };

    function selectedTab(event, ui) {
        var toShow = 'wifi';
        if (ui.newPanel.selector === '#tab-btle') {
            toShow = 'btle';
        }

        $scope.selectedFpTable = toShow;

        $rootScope.$broadcast('tabSelected', toShow);
    }

    $rootScope.$on('MAPSELECTED', function () {
        // clear tables
        $scope.fpTables = [];
    });

    var updateTableSize = function () {
        //when table data is loaded resize table to window size
        $timeout(function () {
            angular.element($window).triggerHandler('resize');
        }, 100);
    };

    var loadTransmittersOnlyActiveByMap = function (table, map, type) {

        //Get all transmitters from database and load in table.columns
        FingerprintService.getTransmittersByMap(map, type, function (data) {
            table.columns = data;


            //If allTransmitters were loaded then we get only those which are active to map the checkboxes
            FingerprintService.getTransmittersOnlyActiveByMap(map, type, function (data) {
                var onlyActiveTransmitters = data;

                table.columns = table.columns.sort();
                var arr = $scope.transmitterActiveWifi;
                if (type == 'btle') {
                    arr = $scope.transmitterActiveBtle;
                }


                //Set if it is active or not
                for (var i = 0; i < table.columns.length; i++) {
                    for (var j = 0; j < onlyActiveTransmitters.length; j++) {
                        if (table.columns[i] == onlyActiveTransmitters[j]) {
                            arr[i] = true;
                            break;
                        }
                    }
                }
                table.selectAllColumns();
                table.update();

                updateTableSize();

            }, function () {
            });

        }, function () {
        });
    };

    $scope.sliders = {};

    /**
     * Save min/max value for the slider widgets
     * @param type current type of table ('wifi' or 'btle')
     * @param fpData the fingerprint data
     */
    var saveMinMax = function (type, fpData) {
        $scope.sliders[type] = {};


        /**
         * save a min/max value
         * @param valueId the value id. e.g. 'constancy'
         * @param valueDescription humane readably description of the value
         * @param source the data source object from the fingerprint data
         */
        var saveMinMaxToArr = function (valueId, valueDescription, source) {
            if (typeof source[valueId] !== 'undefined' && source[valueId] !== '') {
                if (!$scope.sliders[type][valueId]) {
                    $scope.sliders[type][valueId] = {
                        desc: valueDescription,
                        min: Number.MAX_VALUE,
                        max: -Number.MAX_VALUE,
                        minValue: Number.MAX_VALUE,
                        maxValue: -Number.MAX_VALUE
                    }
                }
                $scope.sliders[type][valueId].min = Math.min($scope.sliders[type][valueId].min, source[valueId]);
                $scope.sliders[type][valueId].max = Math.max($scope.sliders[type][valueId].max, source[valueId]);
                $scope.sliders[type][valueId].minValue = $scope.sliders[type][valueId].min;
                $scope.sliders[type][valueId].maxValue = $scope.sliders[type][valueId].max;
            }
        }


        fpData.forEach(function (fp) {
            fp.data.forEach(function (value) {
                saveMinMaxToArr('constancy', 'Constancy', value);
                saveMinMaxToArr('distance', 'Distance', value);
                saveMinMaxToArr('mean', 'Mean', value);
                saveMinMaxToArr('meanToDistanceRatio', 'Mean to distance ratio', value);
                saveMinMaxToArr('standardDeviation', 'Standard Deviation', value);
            });
        });


    }

    var processFpData = function (fpDataAp, fpDataIBeacon) {
        $scope.fpTables = {};

        var wifiTable = null;
        var btleTable = null;


        if (fpDataAp) {
            wifiTable = new FPTable('fpTable-0', 'Access Points', fpDataAp, 'transmitterActiveWifi',
                $filter, $scope, $compile, formatValue);
            loadTransmittersOnlyActiveByMap(wifiTable, config().selectedMap, 'wifi');
            $scope.fpTables.wifi = wifiTable;
            saveMinMax('wifi', fpDataAp);
        }

        if (fpDataIBeacon) {
            btleTable = new FPTable('fpTable-1', 'iBeacons', fpDataIBeacon, 'transmitterActiveBtle',
                $filter, $scope, $compile, formatValue);
            loadTransmittersOnlyActiveByMap(btleTable, config().selectedMap, 'btle');
            $scope.fpTables.btle = btleTable;
            saveMinMax('btle', fpDataIBeacon);
        }

        // following modified elements must be in DOM to initialize them,
        // so this has to be done after angular update are applied
        $timeout(function () {

            if (wifiTable) {
                initTable(wifiTable);
                $scope.updateTables('wifi');

            }
            if (btleTable) {
                initTable(btleTable);
                $scope.updateTables('btle');
            }


            if (tabs) {
                tabs.tabs('refresh');
            } else {
                tabs = $('#tabs').tabs({
                    activate: selectedTab
                });
            }

            $('.dropdown-menu').on('click', function (e) {
                if ($(this).hasClass('dropdown-menu-form')) {
                    e.stopPropagation();
                }
            });

            updateTableSize();

        }, 100);
    };

    $scope.$on('tmDataLoaded', function (event, data) {
        processFpData(data.fpDataAp, data.fpDataIBeacon);
    });


    $scope.$on('fpDataLoaded', function (event, data) {
        processFpData(data.fpDataAp, data.fpDataIBeacon);
    });

    $scope.setTransmitterActive = function (id) {
        var isActive = false;
        var name = '';

        if ($scope.selectedFpTable == 'wifi') {
            isActive = $scope.transmitterActiveWifi[id];
            $scope.fpTables.wifi.columns = $scope.fpTables.wifi.columns.sort();
            name = $scope.fpTables.wifi.columns[id];
        } else {
            isActive = $scope.transmitterActiveBtle[id];
            $scope.fpTables.btle.columns = $scope.fpTables.btle.columns.sort();
            name = $scope.fpTables.btle.columns[id];
        }

        FingerprintService.updateTransmitterActive(config().selectedMap, name, isActive, function (data) {
            console.log(name + ' = ' + isActive);
        }, function () {
            console.log('Could not update transmitter ' + name + ' data to set active = ' + isActive);

            //Revert changes in HTML and JavaScript
            if ($scope.selectedFpTable == 'wifi') {
                $scope.transmitterActiveWifi[id] = !isActive;
                $('#transmitterActiveWifi' + id).prop('checked', !isActive);
            } else {
                $scope.transmitterActiveBtle[id] = !isActive;
                $('#transmitterActiveBtle' + id).prop('checked', !isActive);
            }
        });
    };

    $scope.toggleColumns = function (fpTable, selected) {
        if (selected) {
            fpTable.selectAllColumns();
        } else {
            fpTable.unselectAllColumns();
        }

        fpTable.update();
    };

    $scope.toggleRows = function (fpTable, selected) {
        if (selected) {
            fpTable.selectAllRows();
        } else {
            fpTable.unselectAllRows();
        }

        fpTable.update();
    };

    /**
     * Update the filter in the table
     * @param type type of the table to be updated (currently 'wifi' or 'btle')
     */
    $scope.updateTables = function (type) {
        Object.keys($scope.sliders[type]).forEach(function (key) {
            $scope.fpTables[type].setValueRange(key, $scope.sliders[type][key].minValue, $scope.sliders[type][key].maxValue);
        });
    };

    var processTmData = function (fpDataAp, fpDataIBeacon) {
        $scope.tmTables = {};

        var wifiTable = null;
        var btleTable = null;


        if (fpDataAp) {
            wifiTable = new FPTable('fpTable-0', 'Access Points', fpDataAp, 'transmitterActiveWifi',
                $filter, $scope, $compile, formatValue);
            loadTransmittersOnlyActiveByMap(wifiTable, config().selectedMap, 'wifi');
            $scope.fpTables.wifi = wifiTable;
            saveMinMax('wifi', fpDataAp);
        }

        if (fpDataIBeacon) {
            btleTable = new FPTable('fpTable-1', 'iBeacons', fpDataIBeacon, 'transmitterActiveBtle',
                $filter, $scope, $compile, formatValue);
            loadTransmittersOnlyActiveByMap(btleTable, config().selectedMap, 'btle');
            $scope.fpTables.btle = btleTable;
            saveMinMax('btle', fpDataIBeacon);
        }

        // following modified elements must be in DOM to initialize them,
        // so this has to be done after angular update are applied
        $timeout(function () {

            if (wifiTable) {
                initTable(wifiTable);
                $scope.updateTables('wifi');

            }
            if (btleTable) {
                initTable(btleTable);
                $scope.updateTables('btle');
            }


            if (tabs) {
                tabs.tabs('refresh');
            } else {
                tabs = $('#tabs').tabs({
                    activate: selectedTab
                });
            }

            $('.dropdown-menu').on('click', function (e) {
                if ($(this).hasClass('dropdown-menu-form')) {
                    e.stopPropagation();
                }
            });

            updateTableSize();

        }, 100);
    };
});