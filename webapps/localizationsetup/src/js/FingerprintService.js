angularApp.factory('FingerprintService', function ($rootScope, $http) {

    function getFingerPrints(map, success, error) {
        var data = {
            fpDataAp: false,
            fpDataIBeacon: false
        };

        //FIXME properly use promise-queueing https://docs.angularjs.org/api/ng/service/$q

        function checkResult() {
            if (data.fpDataAp === false || data.fpDataIBeacon === false) {
                // not all responses received yet.
                return;
            }

            if (data.fpDataAp === null && data.fpDataIBeacon === null) {
                // both responses had errors
                error();
                return;
            }

            // at least one response was successful
            success(data);

        }

        $http.get(serverEndpoint + 'analysis?type=WIFI&map=' + map).success(function (result) {
            data.fpDataAp = result;
            checkResult();
        }).error(function () {
            data.fpDataAp = null;
            checkResult();
        });

        $http.get(serverEndpoint + 'analysis?type=BTLE&map=' + map).success(function (result) {
            data.fpDataIBeacon = result;
            checkResult();
        }).error(function () {
            data.fpDataIBeacon = null;
            checkResult();
        });

    }

    function getTransmittersOnlyActiveByMap(map, type, success, error) {
        $http.get(mapserverEndpoint + 'fingerprints/transmitter/download?mapName=' + map + '&transmittersType=' +
            type + '&onlyactive=true').success(function (result) {
            success(result);
        }).error(function () {
            error();
        });
    }


    function getTransmittersByMap(map, type, success, error) {
        $http.get(mapserverEndpoint + 'fingerprints/transmitter/download?mapName=' + map + '&transmittersType=' +
            type + '&onlyactive=false').success(function (result) {
            success(result);
        }).error(function () {
            error();
        });
    }


    function updateTransmitterActive(map, identifier, active, success, error) {
        $.post(mapserverEndpoint + 'fingerprints/transmitter/update/',
            {mapName: map, transmitter: identifier, active: active}).success(function (result) {
                success();
            }).error(function () {
                error();
            });
    }

    /**
     * Returns an object containing wifi and btle transmitters
     * @param map name of the map
     * @param success success callback
     * @param error error callback
     */
    function getTransmitters(map, success, error) {
        $http.get(serverEndpoint + '/transmitter/' + map).success(function (result) {
            success(result);
        }).error(function () {
            error();
        });

    }

    /**
     * Saves the transmitters to the server
     * @param map name of the map
     * @param transmitters the transmitters objects. Same structure as
     * returned by getTransmitters.
     * @param success success callback
     * @param error error callback
     */
    function saveTransmitters(map, transmitters, success, error) {
        $http({
            method: 'POST',
            url: serverEndpoint + '/transmitter/'+ map,
            data: transmitters,
            headers: {
                'Content-Type': 'application/json'
            }}).success(function (result) {
            success(result);
        }).error(function () {
            error();
        });


    }


    return {
        getFingerPrints: getFingerPrints,
        updateTransmitterActive: updateTransmitterActive,
        getTransmittersOnlyActiveByMap: getTransmittersOnlyActiveByMap,
        getTransmittersByMap: getTransmittersByMap,
        getTransmitters: getTransmitters,
        saveTransmitters: saveTransmitters
    };
});