/**
 * Configure routes
 */
angularApp.config(function ($routeProvider) {
    $routeProvider.when('/l10n-analysis', {
        templateUrl: 'template/analysis.html',
        reloadOnSearch: false
    });
    $routeProvider.when('/l10n-editor', {
        templateUrl: 'template/editor.html',
        reloadOnSearch: false
    })
    ;

    moment.locale('de');
});


/**
 * start the application
 */
angularApp.run(function ($rootScope, osiam, $location) {
    $rootScope.app = 'localizationsetup';


    $rootScope.$on('ready', function (event, data) {
        if ($location.path()!='/l10n-editor' )
            $location.path('/l10n-analysis');
    });
    // do login
    osiam.ensureLoggedIn();
});