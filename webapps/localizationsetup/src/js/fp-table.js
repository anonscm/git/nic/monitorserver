var FPTable = function FPTable(elementName, name, data, transmitterActiveMapping, filter, scope, compile,
                               valueFormatter) {
    this.elementName = elementName;
    this.name = name;
    this.sourceData = data;
    this.ngFilter = filter('filter');
    this.ngScope = scope;
    this.ngCompile = compile;
    this.valueFormatter = valueFormatter;

    this.transmitterActiveMapping = transmitterActiveMapping;
    this.data = data;

    this.rowObjectId = 'id';
    this.colArrayId = 'data';
    this.colObjectId = 'id';
    this.latitudeId = 'lat';
    this.longitudeId = 'lon';
    this.updateTimeFrameMs = 200;
    this.inclusiveFiltering = true;

    this.filterString = '';
    this.selectedRows = [];
    this.selectedColumns = [];
    this.selectedSubCols = [];
    this.aliasedSubColumns = [];

    //This field gets initialized dynamically from the database
    //this.columns = this.extractColumns(); //old way parsing the data
    this.columns = [];

    this.rows = this.extractRows();
    this.subColumns = this.extractSubColumns();

    this.transmitterLatLngs = this.extractTransmitterLatLngs();

    this.filterRanges = {};

    this.selectAllColumns();
    this.selectAllRows();
    this.selectAllSubColumns(true);

    this.updateTimeout = null;

    this.legend = this.extractSubColumns(this.sourceData);

    this.update();
};

/**
 * Provides a mechanism for scheduled updates. If some data changes we dont want to update immediately because
 * more data could change in a given timeframe (this.updateTimeFrameMs)
 *
 * This is useful for sliders when the rendering is slow
 * (there is a limit in what a frame can take in javascript to render, which will be exceeded for big data tables
 * when using sliders)
 */
FPTable.prototype.update = function () {
    var fpTable = this;

    if (this.updateTimeout) {
        clearTimeout(this.updateTimeout);
    }

    this.updateTimeout = setTimeout(function () {
        fpTable.doUpdate();
    }, this.updateTimeFrameMs);
};

FPTable.prototype.doUpdate = function () {
    if (this.updateTimeout) {
        clearTimeout(this.updateTimeout);
    }

    this.reset();
    this.filter();

    var data = this.data;

    var columns = this.columns.sort();
    var sortedColumns = this.sortColumns(columns);

    var tableContent = '<thead><tr><th rowspan=2>#</th>';

    var i, j, subColumn;

    for(i = 0; i < sortedColumns.length; i++){
        var columnIndex = columns.indexOf(sortedColumns[i]);
        var cls = this.hasLatLng(sortedColumns[i]) ? 'thTransmitter' : 'thTransmitterNoLocation'
        tableContent += '<th class="' + cls + '" colspan="' + this.selectedSubColsCount() + '">' + sortedColumns[i] +
            '<input id="' + this.transmitterActiveMapping + columnIndex + '" type="checkbox" ng-model="' +
            this.transmitterActiveMapping + '[' + columnIndex + ']"'
            + 'ng-change="setTransmitterActive(' + columnIndex + ')">' + '</th>';
    }

    tableContent += '</tr><tr>';

    for (i = 0; i < sortedColumns.length; i++) {
        for (subColumn in this.subColumns) {
            if (this.selectedSubCols[subColumn]) {
                tableContent += '<th title="' + this.legend[subColumn] + '" >' + this.subColumns[subColumn] + '</th>';
            }
        }
    }

    tableContent += '</tr></thead><tbody>';

    for (i = 0; i < data.length; i++) {
        var dataRow = data[i];

        tableContent += '<tr><th scope="row">' + dataRow[this.rowObjectId] + '</th>';

        for (var x = 0; x < sortedColumns.length; x++) {
            var containsDataColumn = false;
            for (j = 0; j < dataRow[this.colArrayId].length; j++) {
                var dataColumn = dataRow[this.colArrayId][j];

                if (sortedColumns[x] === dataColumn[this.colObjectId]) {
                    for (subColumn in this.selectedSubCols) {
                        if (this.selectedSubCols[subColumn]) {
                            var subColumnData = dataColumn[subColumn];
                            if (subColumnData || subColumnData === 0) {

                                var value = subColumnData.toString();
                                if(this.valueFormatter) {
                                    value = this.valueFormatter(subColumn, subColumnData);
                                }

                                tableContent += '<td>' + value + '</td>';
                                containsDataColumn = true;
                            } else {
                                tableContent += '<td></td>';
                                containsDataColumn = true;
                            }
                        }
                    }

                    break;
                }
            }

            if (!containsDataColumn) {
                var selectedSubColsCount = Math.max(this.selectedSubColsCount(), 1);
                for (j = 0; j < selectedSubColsCount; j++) {
                    tableContent += '<td></td>';
                }
            }
        }

        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    $('#' + this.elementName).html(
      this.ngCompile(tableContent)(this.ngScope)
    );
};

FPTable.prototype.sortColumns = function (columns) {
    var table = this;
    var sortedColumns = [];

    columns.forEach(function(col) {
        if (table.selectedColumns[col] === true) {
            sortedColumns.push(col);
        }
    });

    var dataColumns = this.extractColumns();

    //sort columns so that transmitters who have no lat/lng are on the end
    sortedColumns.sort(function (a, b){
        //comment this in to also sort transmitters to the end that have a position but no values
        //
        //if(dataColumns.indexOf(a) === -1){
        //    return 1;
        //} else if(dataColumns.indexOf(b) === -1){
        //    return -1;
        //}

        var aHasLatLng = table.hasLatLng(a);
        var bHasLatLng = table.hasLatLng(b);

        if(!aHasLatLng && bHasLatLng){
            return 1;
        } else if(aHasLatLng && !bHasLatLng){
            return -1;
        }

        return 0;
    });

    return sortedColumns;
}

FPTable.prototype.hasLatLng = function (column) {
    return this.transmitterLatLngs.hasOwnProperty(column);
}

FPTable.prototype.extractTransmitterLatLngs = function () {
    var table = this;
    var transmitterLatLngs = {};
    table.data.forEach(function (row) {
        var columns = row[table.colArrayId];
        columns.forEach(function (column) {
            var id = column[table.colObjectId];
            var lat = 0;
            var lng = 0;

            if(column.hasOwnProperty(table.latitudeId) && column.hasOwnProperty(table.longitudeId)) {
                lat = column[table.latitudeId];
                lng = column[table.longitudeId];
            }

            if(!transmitterLatLngs.hasOwnProperty(id) && lat != 0 && lng != 0) {
                transmitterLatLngs[id] = {lat: lat, lng: lng};
            }
        });
    });
    return transmitterLatLngs;
}

FPTable.prototype.selectedSubColsCount = function () {
    var selectedCount = 0;

    for (var subColumn in this.subColumns) {
        if (this.selectedSubCols[subColumn]) {
            selectedCount++;
        }
    }

    return selectedCount;
};

FPTable.prototype.reset = function () {
    this.data = $.extend(true, [], this.sourceData);
};

FPTable.prototype.filter = function () {
    var table = this;

    var prop;

    table.data.forEach(function (row) {
        var columns = row[table.colArrayId];

        var deleteColumns = [];
        columns.forEach(function (column) {
            if (table.inclusiveFiltering) {
                //remove column even if only one filter does not match
                for (prop in table.filterRanges) {
                    if (column[prop] < table.filterRanges[prop][0] ||
                        column[prop] > table.filterRanges[prop][1]) {
                        var colObjectId = column[table.colObjectId];
                        if (deleteColumns.indexOf(colObjectId) === -1) {
                            deleteColumns.push(colObjectId);
                        }
                    }
                }
            } else {
                //remove column only if all filters do not match
                for (prop in table.filterRanges) {
                    if (column[prop] < table.filterRanges[prop][0] ||
                        column[prop] > table.filterRanges[prop][1]) {
                        delete column[prop];
                    }
                }
            }

            //remove sub columns that are not selected and are empty
            for (prop in column) {
                if ((table.selectedSubCols[prop] === false && prop !== table.colObjectId) || (column[
                        prop] === '' && prop !== table.colObjectId)) {
                    delete column[prop];
                }
            }
        });

        //remove columns that are not selected or do not match the filter
        columns = columns.filter(function (column) {
            if (Object.keys(column).length === 1 || table.selectedColumns[column[table.colObjectId]] ===
                false || deleteColumns.indexOf(column[table.colObjectId]) !== -1) {
                return false;
            }
            return true;
        });

        row[table.colArrayId] = columns;
    });

    //remove rows that are not selected or do not contain any data
    table.data = table.data.filter(function (row) {
        if (table.selectedRows[row[table.rowObjectId]] !== true || row[table.colArrayId].length === 0) {
            return false;
        }
        return true;
    });

    //angularjs basic string filter
    table.data = this.ngFilter(table.data, table.filterString);
};

FPTable.prototype.setValueRange = function (subColumn, min, max) {
    this.filterRanges[subColumn] = [min, max];
    this.update();
};

FPTable.prototype.setSubColumnAlias = function (subColumn, alias, legendText) {
    this.subColumns[subColumn] = alias;
    this.aliasedSubColumns.push(subColumn);
    this.legend[subColumn] = legendText;
    this.update();
};

FPTable.prototype.extractColumns = function () {
    var table = this;
    var cols = [];
    table.data.forEach(function (row) {
        var columns = row[table.colArrayId];
        columns.forEach(function (column) {
            var id = column[table.colObjectId];
            if (cols.indexOf(id) === -1) {
                cols.push(id);
            }
        });
    });
    return cols;
};


FPTable.prototype.extractRows = function () {
    var table = this;
    var rows = [];

    table.data.forEach(function (row) {
        rows.push(row[table.rowObjectId]);
    });

    return rows;
};

FPTable.prototype.extractSubColumns = function () {
    var table = this;
    var subCols = {};

    table.data.forEach(function (row) {
        var columns = row[table.colArrayId];
        columns.forEach(function (column) {
            for (var prop in column) {
                if (!subCols.hasOwnProperty(prop) && prop !== table.colObjectId) {
                    subCols[prop] = prop;
                }
            }
        });
    });
    return subCols;
};

FPTable.prototype.selectAllColumns = function () {
    for (var i = 0; i < this.columns.length; i++) {
        this.selectedColumns[this.columns[i]] = true;
    }
};

FPTable.prototype.selectAllRows = function () {
    for (var i = 0; i < this.rows.length; i++) {
        this.selectedRows[this.rows[i]] = true;
    }
};

FPTable.prototype.selectAllSubColumns = function (aliasedOnly) {
    for (var subColumn in this.subColumns) {
        if (this.aliasedSubColumns.indexOf(subColumn) != -1 || !aliasedOnly) {
           this.selectedSubCols[subColumn] = true;
        }
    }
};

FPTable.prototype.unselectAllColumns = function () {
    for (var i = 0; i < this.columns.length; i++) {
        this.selectedColumns[this.columns[i]] = false;
    }
};

FPTable.prototype.unselectAllRows = function () {
    for (var i = 0; i < this.rows.length; i++) {
        this.selectedRows[this.rows[i]] = false;
    }
};

FPTable.prototype.unselectAllSubColumns = function () {
    for (var subColumn in this.subColumns) {
        this.selectedSubCols[subColumn] = false;
    }
};

FPTable.prototype.selectSubColumn = function(subColumn){
    this.selectedSubCols[subColumn] = true;
}

FPTable.prototype.unselectSubColumn = function(subColumn){
    this.selectedSubCols[subColumn] = false;
}