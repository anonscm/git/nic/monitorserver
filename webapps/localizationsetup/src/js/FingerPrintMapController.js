angularApp.controller('FingerPrintMapController',
    function ($scope, $rootScope, $filter, $http, $timeout, config, leaflet) {

        $scope.colorLegendFields = ['-100 db', '-80 db', '-60 db', '-40 db', '-20 db'];

        /** current transmitter type, wifi or btle */
        $scope.transmitterType = 'wifi';

        /** map of wifi APs */
        var wifiAps = {};

        /** map of BTLE transmitters */
        var btleAps = {};

        /** currently selected transmitter type map, wifiAps or btleAps */
        var visibleAps = {};

        /** array of transmitter symbols currently displayed on the map */
        var apMarkers = [];

        /** map of fingerprints */
        var fps = {};


        /** connecting lines from FP to transmitters */
        var lines = [];

        /** array of triangles of fingerprints */
        var fpTriangles = [];

        var fpHeatMap = {};

        var currentMap = null;
        var virtualAPs = {};
        var popupOpen = false;
        var selectedAp = null;

        var lf = leaflet();

        $scope.dynamicPopover = {
            content: [],
            templateUrl: 'myPopoverTemplate.html',
            title: 'Select transmitter'
        };


        $scope.showHeatMapforVAP = function (apid) {
            setOpacityOfVisibleAps(0.4);
            $scope.dynamicPopover.marker.setOpacity(1);
            showHeatMap(apid);
            $scope.closePopup();

        };


        $scope.closePopup = function () {
            setTimeout(function () {
                if (popupOpen) {
                    $('#ap-select').trigger('click');
                    popupOpen = false;
                }
            }, 10);

        };

        function onFingerPrintDataLoaded(fpDataAp, fpDataIBeacon) {
            wifiAps = {};
            btleAps = {};
            fps = {};

            // save locations of fingerprints and transmitters for map
            if (fpDataIBeacon) {
                saveFPsAndAPs(fpDataIBeacon, btleAps, 'btle');
                visibleAps = btleAps;
                $scope.transmitterType = 'btle';
            }
            if (fpDataAp) {
                saveFPsAndAPs(fpDataAp, wifiAps, 'wifi');
                visibleAps = wifiAps;
                $scope.transmitterType = 'wifi';

            }

            createTriangles();

            // display fingerprints and APs
            showFingerprints();
        }


        function saveFPsAndAPs(dataIn, dataOut, type) {
            dataIn.forEach(function (fp) {

                if (!fps[fp.id]) {
                    // save fingerprint with id and position
                    fps[fp.id] = {
                        id: fp.id,
                        pos: [fp.lat, fp.lon],
                        transmitters: {
                            wifi: {},
                            btle: {}
                        }
                    };

                }

                // iterate over all transmitters
                fp.data.forEach(function (ap) {
                    // save transmitter with id and position
                    dataOut[ap.id] = {
                        pos: [ap.lat, ap.lon],
                        id: ap.id,
                        name: ap.name && ap.name !== '' ? ap.name : ap.id
                    };

                    if (!fps[fp.id].transmitters[type][ap.id]) {
                        fps[fp.id].transmitters[type][ap.id] = {};
                    }

                    // copy remaining values to ap
                    for (var key in ap) {
                        if (ap.hasOwnProperty(key)) {
                            fps[fp.id].transmitters[type][ap.id][key] = ap[key];
                        }
                    }
                });
            });

        }


        var icons = {
            WIFI: L.icon({
                iconUrl: 'images/wifi_48x32.png',
                iconSize: [48, 32],
                iconAnchor: [24, 16],
                popupAnchor: [0, 0]
            }),
            BTLE: L.icon({
                iconUrl: 'images/btle_48x32.png',
                iconSize: [48, 32],
                iconAnchor: [24, 16],
                popupAnchor: [0, 0]
            }),
            FP: L.icon({
                iconUrl: 'images/crosshair_48x48.png',
                iconSize: [48, 48],
                iconAnchor: [24, 24],
                popupAnchor: [0, 0]
            })
        };


        function createTriangles() {
            var fpLocations = [];

            // iterate over all fingerprints
            for (var key in fps) {
                if (fps.hasOwnProperty(key)) {
                    var fp = fps[key];

                    // store position
                    fpLocations.push({x: fp.pos[0], y: fp.pos[1], id: key});
                }
            }

            // returned array contains objects with triangles
            fpTriangles = triangulate(fpLocations);
        }


        /**
         * Show the heatmap for a transmitter
         * @param apId id of transmitter
         */
        function showHeatMap(apId) {
            if (apId != null) {


                dimFingerprintIcons(true);

                ColorUtil.setColorOfTrianglePoints(fpTriangles, -100, -20, function (point) {
                    var fp = fps[point.id];
                    var ap = fp.transmitters[$scope.transmitterType][apId];
                    if (typeof ap !== 'undefined') {
                        return ap.mean;
                    } else {
                        return -100;
                    }
                }, {'': 'rgb(255, 255, 255)'});

                fpHeatMap.setFpTriangles(fpTriangles);

            }
        }


        function initMap() {
            var cfg = config();
            lines = [];

            if (cfg.selectedMap === currentMap) {
                return;
            }

            currentMap = cfg.selectedMap;

            lf.loadMapConfig(cfg.selectedMap, function () {
                // clean up old map
                var mapElement = document.getElementById('l10nMap');
                var parent = mapElement.parentElement;
                parent.removeChild(mapElement);

                // create new map element
                var newMapElement = document.createElement('div');
                newMapElement.id = 'l10nMap';
                parent.appendChild(newMapElement);

                // create new leaflet map
                map = L.map('l10nMap', {
                    attributionControl: false
                });

                // add tile layer
                L.tileLayer(cfg.tileUrl, {
                    minZoom: cfg.minZoom,
                    maxZoom: cfg.maxZoom,
                    tms: true
                }).addTo(map);

                // center map properly
                var center = [
                    (cfg.boundingBox[0][0] + cfg.boundingBox[1][0]) / 2, (cfg.boundingBox[0][1] + cfg.boundingBox[
                        1][1]) / 2
                ];
                map.setView(center);
                map.setMaxBounds(cfg.boundingBox);
                map.fitBounds(cfg.boundingBox);

                map.on('click', function () {
                    hideLines();
                    setOpacityOfVisibleAps(1);
                    fpHeatMap.setFpTriangles([]);
                    $scope.closePopup();
                });

                fpHeatMap = new LHeatMap(map);
            });
        }


        /**
         * Test if AP is a virtual AP
         * @param ap id of accesspoint or accesspoint object
         * @returns {*} true if AP is a virtual AP
         */
        function isVirtualAP(ap) {
            if (typeof(ap) === 'string') {
                ap = visibleAps[ap];
            }
            if (!ap) {
                return false;
            }
            return virtualAPs[ap.pos[0]] && virtualAPs[ap.pos[0]][ap.pos[1]];


        }


        function forEachVirtualAP(visitor) {
            for (var lat in virtualAPs) {
                if (virtualAPs.hasOwnProperty(lat)) {
                    for (var lon in virtualAPs[lat]) {
                        if (virtualAPs[lat].hasOwnProperty(lon)) {
                            var ap = virtualAPs[lat][lon];
                            visitor(ap);
                        }
                    }
                }
            }
        }

        function showVirtualAPSelector(e) {
            //TODO open popup and display id of each in e.target.ap.aps
            // when an id is clicked, show heatmap of AP
            $scope.dynamicPopover.content = e.target.ap.aps;
            $scope.dynamicPopover.marker = e.target;
            $('#ap-select').trigger('click');
            popupOpen = !popupOpen;
        }


        function showHeatMapListener(e) {
            setOpacityOfVisibleAps(0.4);
            selectedAp = e.target.apId;
            e.target.setOpacity(1);
            showHeatMap(selectedAp);
            $scope.closePopup();
        }


        /**
         * adds the given transmitters to the map
         * @param aps map of transmitters
         * @param icon icon to use
         */
        function addAPsToMap(aps, icon) {
            hideAps();

            // map of map with lat and lon as keys to detect virtual APs
            var apMap = {};

            // map of map with lat and lon as keys to store virtual aps
            virtualAPs = {};

            var apId, ap;

            for (apId in aps) {
                ap = aps[apId];
                var multiAp = false;
                if (!apMap[ap.pos[0]]) {
                    apMap[ap.pos[0]] = {};
                }
                if (apMap[ap.pos[0]][ap.pos[1]]) {
                    multiAp = true;

                    if (!virtualAPs[ap.pos[0]] || !virtualAPs[ap.pos[0]][ap.pos[1]]) {
                        // create array at virtualAps[lat][lng] and store first ap
                        virtualAPs[ap.pos[0]] = {};
                        virtualAPs[ap.pos[0]][ap.pos[1]] = {aps: []};
                        virtualAPs[ap.pos[0]][ap.pos[1]].aps.push(apMap[ap.pos[0]][ap.pos[1]]);
                    }
                    virtualAPs[ap.pos[0]][ap.pos[1]].aps.push(ap);
                } else {
                    apMap[ap.pos[0]][ap.pos[1]] = ap;
                }
            }

            // add virtual APs to map


            forEachVirtualAP(function (vap) {


                // show all APs in tooltip
                var title = '';
                vap.aps.forEach(function (ap) {
                    // if name and id are different, show both, else only the id
                    title += (ap.name === ap.id ? ap.id : ap.id + ' : ' + ap.name  ) + '\n';
                });


                var apMarker = L.marker(vap.aps[0].pos, {
                    icon: icon,
                    title: title
                });

                apMarker.on('click', showVirtualAPSelector);

                apMarker.addTo(map);
                apMarker.ap = vap;
                apMarkers.push(apMarker);

            });


            //for each non virtual AP, we create a marker and add it to the map
            for (apId in aps) {
                ap = aps[apId];

                if (isVirtualAP(ap)) {
                    continue;
                }

                var apMarker = L.marker(ap.pos, {
                    icon: icon,
                    title: ap.name === ap.id ? ap.id : ap.id + ' : ' + ap.name
                });
                apMarker.addTo(map);
                apMarker.apId = apId;

                apMarker.on('click', showHeatMapListener);

                apMarkers.push(apMarker);
            }
        }

        /**
         * removes all transmitter icons from the map
         * @param aps map of transmitters
         */
        function hideAps() {

            apMarkers.forEach(function (marker) {
                map.removeLayer(marker);
            });

            apMarkers.length = 0;
        }

        /**
         * Sets the opacity of all visible transitter icons
         * @param opacity the opacity
         */
        function setOpacityOfVisibleAps(opacity) {

            apMarkers.forEach(function (marker) {
                marker.setOpacity(opacity);
            });

        }

        function showConnectionsListener(e) {
            showConnections(e.target.fpId);
            $scope.closePopup();
        }

        /**
         * Displays the fingerprints in the map
         */
        function showFingerprints() {

            // each fingerprint is added as a marker to the map
            for (var fpId in fps) {
                var fp = fps[fpId];
                var fpMarker = L.marker(fp.pos, {
                    icon: icons.FP,
                    title: fp.id
                });
                fp.marker = fpMarker;
                fpMarker.fpId = fpId;
                fpMarker.on('click', showConnectionsListener);
                fpMarker.addTo(map);
            }
            addAPsToMap(visibleAps, $scope.transmitterType === 'wifi' ? icons.WIFI : icons.BTLE);

        }

        /**
         * Update the transmitters displayed in the map
         * @param toShow type of transmitters to show (wifi or btle)
         */
        function updateTransmitters(toShow) {
            hideLines();
            fpHeatMap.setFpTriangles([]);

            $scope.transmitterType = toShow;
            if (toShow === 'wifi') {
                visibleAps = wifiAps;
                addAPsToMap(wifiAps, icons.WIFI);
            } else {
                visibleAps = btleAps;
                addAPsToMap(btleAps, icons.BTLE);
            }
        }


        /**
         * hide all lines and fade fingerprint icons
         * @param focusFp to be focused fingerprints, all other FPs will be faded out. If null, all FPs are focused.
         */
        function hideLines(focusFp) {
            lines.forEach(function (line) {
                map.removeLayer(line);
            });

            lines.length = 0;

            dimFingerprintIcons(focusFp);

        }

        /**
         * Dims the fingerprints icons.
         * @param focusFp fingerprint id of fingerprint icon to be focused, or true if all icons shall be dimmed.
         */
        function dimFingerprintIcons(focusFp) {
            for (var fpId in fps) {
                if (fps.hasOwnProperty(fpId)) {
                    var otherFp = fps[fpId];

                    if (focusFp && focusFp !== otherFp) {
                        // fade other FPs
                        otherFp.marker.setOpacity(0.3);
                    } else {
                        otherFp.marker.setOpacity(1.0);
                    }
                }
            }
        }

        function getMeanColor(mean) {
            return ColorUtil.getColor(-100, -20, mean);
        }

        /**
         * Show connections from the fingerprint to the transmitters
         * @param fpId fingerprint id
         */
        function showConnections(fpId) {
            var fp = fps[fpId];

            hideLines(fp);

            var fromPos = fp.pos;

            for (var apId in fp.transmitters[$scope.transmitterType]) {
                if (fp.transmitters[$scope.transmitterType].hasOwnProperty(apId)) {
                    // get the AP to draw a line to
                    var ap = fp.transmitters[$scope.transmitterType][apId];

                    var toPos = visibleAps[apId].pos;
                    var color, label;
                    if (isVirtualAP(apId)) {

                        // is a virtual ap, calculate mean of means
                        label = '';
                        var meanCount = 0;
                        var meanSum = 0;
                        var vap = virtualAPs[toPos[0]][toPos[1]];
                        vap.aps.forEach(function (a) {
                            var thisMean = fp.transmitters[$scope.transmitterType][a.id].mean;
                            label += a.id + ' : ' + thisMean + ' <br>\n';
                            if (thisMean !== '') {
                                meanCount++;
                                meanSum += thisMean;
                            }
                        });

                        // if no mean values exist, line is white
                        if (meanCount > 0) {
                            color = getMeanColor(meanSum / meanCount);
                        } else {
                            color = 'rgb(255, 255, 255)';
                        }

                    } else {
                        label = ap.id + ' : ' + ap.mean;
                        if (ap.mean !== '') {
                            color = getMeanColor(ap.mean);
                        } else {
                            color = 'rgb(255, 255, 255)';
                        }
                    }


                    var line = L.polyline([fromPos, toPos], {
                        color: color,
                        opacity: 1,
                        weight: 5
                    }).bindLabel(label)
                        .addTo(map);
                    lines.push(line);

                }
            }
        }


        $scope.$on('fpDataLoaded', function (event, data) {
            onFingerPrintDataLoaded(data.fpDataAp, data.fpDataIBeacon);
        });


        $scope.$on('tabSelected', function (event, toShow) {
            updateTransmitters(toShow);
        });

        $scope.$on('MAPSELECTED', function () {
            initMap();
        });

        $scope.whenReady(function () {
            initMap();
        });

    });