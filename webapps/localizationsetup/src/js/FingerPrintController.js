angularApp.controller('FingerPrintController',
    function ($scope, $rootScope, $filter, $http, $timeout, config, FingerprintService) {

        $scope.fpError = false;

        /**
         * Triggers loading of the fingerprint data and broadcasts it
         */
        var loadFpData = function () {
            $scope.fpError = 'Loading fingerprint data...';
            FingerprintService.getFingerPrints(config().selectedMap, function (data) {
                $scope.fpError = false;
                $rootScope.$broadcast('fpDataLoaded', data);
            }, function () {
                $scope.fpError = 'Could not load fingerprint data';
            });
        };

        $scope.$on('MAPSELECTED', function () {
            loadFpData();
        });

        $scope.whenReady(function () {
            loadFpData();
        });

    });