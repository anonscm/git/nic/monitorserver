angularApp.controller('TransmitterController',
    function ($scope, $rootScope, $filter, $http, $timeout, config, FingerprintService) {

        var INFO = 'alert-info';
        var INFO_ADD = 'alert-info add-transmitter';
        var WARNING = 'alert-warning';
        var DANGER = 'alert-danger';
        var SUCCESS = 'alert-success';

        $scope.data = null;

        $scope.info = {
            saveState: INFO,
            message: ''
        };

        function info(state, message) {
            $scope.info.saveState = state;
            $scope.info.message = message;
        }

        /**
         * Triggers loading of the transmitter data and broadcasts it
         */
        var loadTmData = function () {
            info(INFO, 'Loading transmitter data...');
            FingerprintService.getTransmitters(config().selectedMap, function (data) {
                $scope.data = data;
                $scope.selected = null;
                info(INFO, 'Transmitter data loaded.');
                $rootScope.$broadcast('tmDataLoaded', data);
            }, function () {
                info(DANGER, 'Could not load transmitter data');
            });

        };


        $scope.save = function () {
            info(WARNING, 'Saving transmitter data...');
            FingerprintService.saveTransmitters(config().selectedMap, $scope.data, function (newData) {
                $scope.data = newData;
                info(SUCCESS, 'Transmitter data saved.');
                $rootScope.$broadcast('tmDataLoaded', newData);
            }, function () {
                info(DANGER, 'Could not save transmitter data');
            });
        };

        $scope.discard = function () {
            loadTmData();
        };

        $scope.import = function () {
            $http.get(serverEndpoint + '/transmitter/import/' + config().selectedMap).success(function (result) {
                info(INFO, 'Transmitter data imported: ' + result + ' . Reloading...');
                loadTmData();
            }).error(function () {
                info(DANGER, 'Could not import transmitter data.');
            });
        };

        $scope.$on('MAPSELECTED', function () {
            loadTmData();
        });

        $scope.$on('SAVESTATECHANGE', function (event, state) {
            if (state === 'dirty') {
                $scope.dataEdited();
            }
        });

        $scope.dataEdited = function () {
            info(WARNING, 'There are unsaved changes');
        };

        /**
         * Prepares to add a new transmitter to the map
         * @param transmitterType transmitter type, 'wifi' or 'btle'
         * @param transmitter new transmitter object
         */
        function prepareAddTransmitter(transmitterType, transmitter) {
            info(INFO_ADD, "Click on the map to place new transmitter");
            $scope.selected = null;
            $rootScope.$broadcast('ADD_TRANSMITTER', transmitterType, transmitter);
        }

        /**
         * Cancel the add new transmitter process
         */
        $scope.cancelAddTransmitter = function () {
            $rootScope.$broadcast('CANCEL_ADD_TRANSMITTER');
            info("", "");
        };

        /**
         * Adds a new BTLE transmitter.
         * @param btle the BTLE base transmitter obejct
         */
        $scope.addBtle = function (btle) {
            var id = btle.identifier.split('_');

            var transmitter = {
                lat: 0,
                lon: 0,
                height: 200,
                vendor: "",
                name: "new transmitter",
                uuid: id[0],
                majorId: id[1],
                minorId: id[2],
                powerLevel: "",
                sendInterval: "",
                transmitter: btle
            };

            prepareAddTransmitter('btle', transmitter);
        };

        /**
         * Adds a new Wifi AP
         */
        $scope.addWifi = function () {
            var transmitter = {
                map_id: $scope.data.mapId,
                lat: 0,
                lon: 0,
                height: 200,
                vendor: "",
                name: "new transmitter",
                networks: []
            };

            prepareAddTransmitter('wifi', transmitter);
        };

        $scope.$on('SELECTED_TRANSMITTER', function (event, transmitter) {
            $scope.selected = transmitter;
        });

        /**
         * Event handler when user has clicked on map to add a new transmitter
         */
        $scope.$on('NEW_TRANSMITTER', function (event, newTransmitter) {
            info(INFO, "Created new transmitter. Please enter basic transmitter information");
            $scope.data[newTransmitter.type].push(newTransmitter.transmitter);
            $scope.selected = newTransmitter.transmitter;
        });

        /**
         * Remove a network from an AP
         * @param network network to remove
         * @param selectedAp AP transmitter to remove network from
         */
        $scope.removeNetwork = function (network, selectedAp) {
            if (network == null || selectedAp == null) {
                return;
            }

            var index = selectedAp.networks.indexOf(network);
            selectedAp.networks.splice(index, 1);

            $scope.data.unknownWifi.push(network.transmitter);


            $scope.dataEdited();
        };

        /**
         * Add a network to an AP
         * @param unknownNetwork the network to be added.
         * @param selectedAp the AP to add the network to
         */
        $scope.addNetwork = function (unknownNetwork, selectedAp) {
            // create default network object
            var network = {
                name: "",
                macAddress: unknownNetwork.identifier,
                channelNumber: 0,
                band: 0,
                transmitter: unknownNetwork
            };

            // add network at top of list, that it appears right under the "add network" button
            selectedAp.networks.splice(0, 0, network);

            $scope.data.unknownWifi.splice($scope.data.unknownWifi.indexOf(unknownNetwork), 1);

            $scope.dataEdited();

        };

        /**
         * Remove a transmitter
         * @param transmitter transmitter to be removed
         */
        $scope.removeTransmitter = function (transmitter) {
            var transmitterList;

            if (transmitter.transmitter) {
                //btle
                transmitterList = $scope.data.btle;

                $scope.data.unknownBtle.push(transmitter.transmitter);

            } else {
                //wifi
                transmitterList = $scope.data.wifi;

                transmitter.networks.forEach(function (network) {
                    $scope.data.unknownWifi.push(network.transmitter);
                });
            }

            transmitterList.splice(transmitterList.indexOf(transmitter), 1);

            // broadcast new data, to update map
            $rootScope.$broadcast('tmDataLoaded', $scope.data);
            $scope.selected = null;
            $scope.dataEdited();

        }

        $scope.whenReady(function () {
            loadTmData();
        });

    });


