var path = require('path'),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    htmlmin = require('gulp-htmlmin'),
    connect = require('gulp-connect'),
    jshint = require('gulp-jshint'),
    minifyCSS = require('gulp-minify-css'),
    program = require('commander'),
    stylish = require('jshint-stylish'),
    inject = require('gulp-inject'),
    injectstr = require('gulp-inject-string'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    replace = require('gulp-replace'),
    plumber = require('gulp-plumber'),
    mainBowerFiles = require('main-bower-files'),
    gulpif = require('gulp-if'),
    karma = require('gulp-karma'),
    debug = false,
    minify = false,
    WATCH_MODE = 'watch',
    RUN_MODE = 'run';

var mode = RUN_MODE;

var buildDir = "localizationsetup";

// get all 3rd party libaries: from bower and those, for which we don't have a bower dependency
var vendorJsFiles = mainBowerFiles(['**/*.js'])
    .concat(['../common/src/lib/**/*.js']);

// all application javascript files
var appJsFiles = ['config.js', '../common/src/js/**/*.js', 'src/js/**/*.js'];

// all test files: bower dependencies including dev, other 3rd party lib, app js, test js
var testFiles = mainBowerFiles(['**/*.js'], {includeDev:true})
    .concat(['../common/src/lib/**/*.js'])
    .concat(appJsFiles)
    .concat(['../common/test/js/**/*.js','test/js/**/*.js']);

gulp.task('appjs', function() {
    return gulp.src(appJsFiles)
        .pipe(gulpif(debug, plumber()))
        .pipe(gulpif(debug, replace('var SELLFIO_DEBUG = false;', 'var SELLFIO_DEBUG = true;')))
        .pipe(concat('app.js'))
        .pipe(gulpif(!minify, gulp.dest(buildDir + '/js')))
        .pipe(gulpif(minify, uglify()))
        .pipe(gulpif(minify, rename('app.min.js')))
        .pipe(gulpif(minify, gulp.dest(buildDir + '/js')))
        .pipe(connect.reload())
        .on('error', gutil.log);
});

gulp.task('vendorjs', function() {
    return gulp.src(vendorJsFiles)
        .pipe(gulpif(debug, plumber()))
        .pipe(concat('vendor.js'))
        .pipe(gulpif(!minify, gulp.dest(buildDir + '/js')))
        .pipe(gulpif(minify, uglify()))
        .pipe(gulpif(minify, rename('vendor.min.js')))
        .pipe(gulpif(minify, gulp.dest(buildDir + '/js')))
        .pipe(connect.reload())
        .on('error', gutil.log);
});

gulp.task('apptemplate', function() {
    return gulp.src(['../common/src/template/**/*.html', 'src/template/**/*.html'])
        .pipe(gulpif(debug, plumber()))
        .pipe(gulpif(minify, htmlmin({ collapseWhitespace: true})))
        .pipe(gulp.dest(buildDir + '/template'))
        .pipe(connect.reload());
});

gulp.task('index', function() {
    return gulp.src('src/index.html')
        .pipe(gulpif(debug, plumber()))
        .pipe(gulpif(!minify, injectstr.after('<!-- inject:js -->',
                '\n<script src="js/vendor.js"></script>\n<script src="js/app.js"></script>')))
        .pipe(gulpif(!minify, injectstr.after('<!-- inject:css -->',
                '\n<link rel="stylesheet" href="css/vendor.css">\n<link rel="stylesheet" href="css/app.css"/>')))
        .pipe(gulpif(minify, injectstr.after('<!-- inject:js -->',
                '\n<script src="js/vendor.min.js"></script>\n<script src="js/app.min.js"></script>')))
        .pipe(gulpif(minify, injectstr.after('<!-- inject:css -->',
                '\n<link rel="stylesheet" href="css/vendor.min.css">\n<link rel="stylesheet" href="css/app.min.css"/>')))
        .pipe(gulpif(minify, htmlmin({ collapseWhitespace: true})))
        .pipe(gulp.dest(buildDir))
        .pipe(connect.reload());
});

gulp.task('appcss', function() {
    return gulp.src(['../common/src/css/**/*.css', 'src/css/**/*.css'])
        .pipe(gulpif(debug, plumber()))
        .pipe(concat('app.css'))
        .pipe(gulp.dest(buildDir + '/css'))
        .pipe(gulpif(minify, minifyCSS()))
        .pipe(gulpif(minify, rename('app.min.css')))
        .pipe(gulpif(minify, gulp.dest(buildDir + '/css')))
        .pipe(connect.reload())
        .on('error', gutil.log);
});

gulp.task('vendorcss', function() {
    return gulp.src(mainBowerFiles(['**/*.css']))
        .pipe(gulpif(debug, plumber()))
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest(buildDir + '/css'))
        .pipe(gulpif(minify, minifyCSS()))
        .pipe(gulpif(minify, rename('vendor.min.css')))
        .pipe(gulpif(minify, gulp.dest(buildDir + '/css')))
        .pipe(connect.reload())
        .on('error', gutil.log);
});

gulp.task('appimages', function() {
    return gulp.src(['../common/src/images/**.*', 'src/images/**.*'])
        .pipe(gulpif(debug, plumber()))
        .pipe(gulp.dest(buildDir + '/images'))
        .pipe(connect.reload());
});

gulp.task('vendorimages', function() {
    return gulp.src(mainBowerFiles(['**/*.png', '**/*.jpg', '**/*.gif', '**/*.bmp', '**/*.tif', '**/*.tiff']))
        .pipe(gulpif(debug, plumber()))
        .pipe(gulp.dest(buildDir + '/images'))
        .pipe(connect.reload());
});

gulp.task('appfonts', function() {
    return gulp.src(['../common/src/fonts/**.*', 'src/fonts/**.*'])
        .pipe(gulpif(debug, plumber()))
        .pipe(gulp.dest(buildDir + '/fonts'))
        .pipe(connect.reload());
});

gulp.task('vendorfonts', function() {
    return gulp.src(mainBowerFiles(['**/*.eot', '**/*.svg', '**/*.ttf', '**/*.woff', '**/*.woff2']))
        .pipe(gulpif(debug, plumber()))
        .pipe(gulp.dest(buildDir + '/fonts'))
        .pipe(connect.reload());
});

gulp.task('lint', function() {
    return gulp.src(['../common/src/js/**/*.js', 'src/js/**/*.js'])
        .pipe(gulpif(debug, plumber()))
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

gulp.task('connect', function() {
    if (mode === WATCH_MODE) {
        gulp.watch(['index.html'], function() {
            gulp.src(['index.html'])
                .pipe(connect.reload());
        });
    }

    connect.server({
        livereload: mode === WATCH_MODE,
        port: 8082
    });
});

gulp.task('debug', function() {
    debug = true;
});

gulp.task('minify', function() {
    minify = true;
});

gulp.task('watch-mode', function() {
    mode = WATCH_MODE;

    var jsWatcher = gulp.watch(['../common/src/js/**/*.js', 'src/js/**/*.js', 'config.js'], ['appjs']),
        testWatcher = gulp.watch(['../common/test/js/**/*.js', 'test/js/**/*.js', 'config.js'], ['test']),
        cssWatcher = gulp.watch(['../common/src/css/**/*.css', 'src/css/**/*.css'], ['appcss']),
        imageWatcher = gulp.watch(['../common/src/images/**.*', 'src/images/**.*'], ['appimages']),
        htmlWatcher = gulp.watch(['../common/src/template/**/*.html', 'src/template/**/*.html'], ['apptemplate']),
        indexWatcher = gulp.watch(['src/index.html'], ['index']);

    function changeNotification(event) {
        console.log('File', event.path, 'was', event.type, ', running tasks...');
    }

    jsWatcher.on('change', changeNotification);
    cssWatcher.on('change', changeNotification);
    imageWatcher.on('change', changeNotification);
    htmlWatcher.on('change', changeNotification);
    indexWatcher.on('change', changeNotification);
});



gulp.task('test', function() {
    return gulp.src(testFiles)
        .pipe(karma({
            configFile: 'karma.conf.js',
            action: 'run'
        }))
        .on('error', function(err) {
            if(!debug) {
                // Make sure failed tests cause gulp to exit non-zero
                throw err;
            }
        });
});


gulp.task('app', ['appcss', 'appjs', 'lint', 'appimages', 'appfonts', 'apptemplate', 'index']);
gulp.task('vendor', ['vendorcss', 'vendorjs', 'vendorimages', 'vendorfonts']);
gulp.task('all', ['vendor', 'app', 'test']);
gulp.task('default', ['all']);
gulp.task('server', ['connect', 'watch-mode', 'all']);
