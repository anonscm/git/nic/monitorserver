/**
 * OSIAM login
 */
angularApp.factory('osiam', function ($rootScope, $window, $location, $http, $cookies) {
    $rootScope.loggedIn = false;

    var requestedHash;
    var accessToken;


    function ensureLoggedIn() {
        requestedHash = $window.location.hash.slice(1);

        $http.get(serverEndpoint + 'osiam/loggedIn').
            success(function (data) {
                if (data && data.accessToken) {
                    accessToken = data.accessToken;
                    $rootScope.username = data.username;
                    $cookies.put('username', data.username);

                    $rootScope.loggedIn = true;
                    $rootScope.$broadcast('loggedIn');
                } else {
                    $rootScope.loggedIn = false;
                    $window.location = data.redirectUri;
                }
            }).error(function () {
                $rootScope.loggedIn = false;
                $location.path('/login');
            });
    }

    function logout() {
        $http.post(serverEndpoint + 'osiam/logout');
        $rootScope.$broadcast('loggedOut');
        $rootScope.loggedIn = false;
        $cookies.remove('username');

        $window.location.reload();
    }

    $rootScope.isAdmin = function () {
        return 'marissa' === $rootScope.username;
    };

    return {
        ensureLoggedIn: ensureLoggedIn,
        logout: logout
    };
});