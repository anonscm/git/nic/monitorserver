
angularApp.factory('websocket', function ($rootScope, $timeout) {
    var sock = null;
    var reconnecting = false;
    var connected = false;

    //flag if automatic reconnect is enabled. Will be disabled, if connection is explicitly closed.
    var doReconnect = false;

    function reconnect() {
        reconnecting = true;
        $rootScope.monitorError = 'Monitor-Application is offline. Reconnecting...';
        $timeout(connect, 2000);
        $rootScope.$apply();
    }

    function connect() {
        doReconnect = true;
        var wsUrl = serverEndpoint + '/monitor';
        var options = {
            transports: ['websocket', 'xhr-streaming', 'xhr-polling']
        };
        sock = new SockJS(wsUrl, null, options);
        sock.onopen = function () {
            connected = true;
            $rootScope.monitorError = false;
            $rootScope.$apply();
            console.log('websocket open');
            if (reconnecting) {
                $rootScope.$broadcast('reconnected');
                reconnecting = false;
            } else {
                $rootScope.$broadcast('connected');
            }

        };
        sock.onmessage = function (e) {
            //console.log('message', e.data);
            var monitorMessage = JSON.parse(e.data);
            $rootScope.$broadcast(monitorMessage.event, monitorMessage);
        };
        sock.onclose = function (e) {
            connected = false;
            console.log('close' + JSON.stringify(e));
            if(doReconnect) {
                reconnect();
            }
        };
        sock.onerror = function (e) {
            console.log('error ' + JSON.stringify(e));
        };
    }

    function disconnect() {
        doReconnect = false;
        if(sock) {
            sock.close();
            sock = null;
        }
    }

    function send(data) {
        sock.send(JSON.stringify(data));
    }

    return {
        connect: connect,
        disconnect: disconnect,
        send: send,
        isConnected: function () {
            return connected;
        }
    };
});