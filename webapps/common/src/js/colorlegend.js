angularApp.directive("colorlegend", function () {
    return {
        restrict: "E",
        templateUrl: "template/colorlegend.html",
        scope: {
            fields: "=fields"
        },
        controller: function ($scope) {
            $scope.getBackgroundColor = function () {

            };

            /**
             * Returns the width of the middle cells
             * @returns {*} string with the size and percent
             */
            $scope.getWidth = function () {
                if ($scope.fields && $scope.fields.length > 2) {
                    return "" + (100 / ($scope.fields.length - 1)) + "%";
                }
                return "100%";
            };

            /**
             * Returns the first field
             * @returns {*} the first field
             */
            $scope.first = function () {
                if ($scope.fields && $scope.fields[0]) {
                    return $scope.fields[0];
                }
                return "";
            };

            /**
             * Returns the last field.
             * @returns {*} the last field
             */
            $scope.last = function () {
                // check is fields a valid array with more than 1 element
                if ($scope.fields && $scope.fields.length && $scope.fields.length > 1) {
                    return $scope.fields[$scope.fields.length - 1];
                }
                return null;
            };

            /**
             * Returns the middle fields if fields has more than 2 entries.
             * @returns {*} the middle fields in an array or and empty array.
             */
            $scope.middle = function () {
                if ($scope.fields && $scope.fields.length > 2) {
                    return $scope.fields.slice(1, $scope.fields.length - 1);
                }
                return [];
            };
        }
    };
});
