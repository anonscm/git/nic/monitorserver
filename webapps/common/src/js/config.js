
/**
 * config factory
 * Reads configuration from server
 */
angularApp.factory('config', function ($rootScope, $http) {
    var config = {};
    $rootScope.ready = false;
    function getMapList() {
        $http.get(serverEndpoint + 'msr?path=/mapserver/map_list').success(function (result) {
            //store config
            config.mapList = result.mapList;

            $rootScope.ready = true;

            // broadcast that we're ready
            $rootScope.$broadcast('ready');
        });

    }

    var loadConfig = function() {
        $http.get(serverEndpoint + 'config').success(function (result) {
            //store config
            config = result;

            if (SELLFIO_DEBUG) {
                config.mapUrl = mapserverEndpoint.replace('/mapserver/', '');
            }

            getMapList();
        });
    }

    /**
     * as soon we are logged in, we load the configuration from the server
     */
    $rootScope.$on('loggedIn', function (event, data) {
        loadConfig();
    });

    if($rootScope.loggedIn) {
        loadConfig();
    }

    $rootScope.$on('loggedOut', function (event, data) {
        $rootScope.ready = false;
    });

    /**
     * if application is ready, directly execute function, else execute it, when we are actually ready
     * @param callback callback function
     */
    $rootScope.whenReady = function (callback) {
        if ($rootScope.ready) {
            callback();
        } else {
            $rootScope.$on('ready', function (event, data) {
                callback();
            });
        }
    };

    return function () {
        return config;
    };
});