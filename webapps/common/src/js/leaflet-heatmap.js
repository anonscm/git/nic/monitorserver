/** Dependencies: three.js (3d graphics library using WebGL) **/
LHeatMap = function LHeatMap(map) {
    this.map = map;
    this.renderer = null;
    this.camera = {};
    this.scene = {};
    this.width = 0;
    this.height = 0;
    this.fpTriangles = [];
    this.mesh = {};
    this.canvasOverlay = {};
    this.vertexShader = null;
    this.fragmentShader = null;
    
    this.transparency = 0.8;

    this.create();
};


LHeatMap.prototype.create = function () {
    var map = this.map;
    var mapSize = map.getSize();
    var heatMap = this;

    var canvasOverlay = L.canvasOverlay();
    this.canvasOverlay = canvasOverlay;

    canvasOverlay.addTo(map);

    heatMap.init(canvasOverlay._canvas);

    canvasOverlay.drawing(drawingOnCanvas);

    function drawingOnCanvas(canvasOverlay, params) {
        heatMap.draw();
    }


    heatMap.draw();
};

/** Creates a new Three.js WebGLRenderer using Orthographic Projection **/
LHeatMap.prototype.init = function (canvas) {
    var params = {
        canvas: canvas,
        precision: 'lowp',
        alpha: true
    };

    var renderer = new THREE.WebGLRenderer(params);
    renderer.setClearColor(0xFFFFFF, 0);
    renderer.autoClear = true;

    var scene = new THREE.Scene();

    this.renderer = renderer;
    this.scene = scene;

    //simple vertex shader which accepts vertex color attributes (for each vertex)
    //the build in shader from three.js does not support alpha components in vertex colors
    //so we needed to write our own simple shader
    this.vertexShader = " \
        attribute vec4 vertexColor;\
        varying vec4 vColor; \
        void main() { \
            vColor = vertexColor; \
            gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0); \
        }";

    this.fragmentShader = " \
        varying vec4 vColor; \
        void main() { \
            gl_FragColor = vColor; \
        }"
};

/** Creates a new mesh and renders the scene **/
LHeatMap.prototype.draw = function () {
    var heatMap = this;
    var renderer = this.renderer;
    var scene = this.scene;

    this.updateViewport(this.canvasOverlay._canvas);

    var camera = this.camera;

    var fpTriangles = this.fpTriangles;

    var map = this.map;

    var geometry = new THREE.Geometry();

    var attributes = {
      vertexColor: {
        type: 'v4',
        value: []
      }
    };

    var colorValues = attributes.vertexColor.value;

    var countTriangles = 0;

    var vIndex = 0;

    fpTriangles.forEach(function (triangle) {
        //HACK: do not draw any triangles with a transparent point in it
        if (triangle.a.color != "transparent" || triangle.b.color != "transparent" || triangle.c.color != "transparent") {

            //converting the latLng pairs to pixel coordinates (of the leaflet overlay canvas)
            var p1 = map.latLngToContainerPoint(L.latLng(triangle.a.x, triangle.a.y))._round();
            var p2 = map.latLngToContainerPoint(L.latLng(triangle.b.x, triangle.b.y))._round();
            var p3 = map.latLngToContainerPoint(L.latLng(triangle.c.x, triangle.c.y))._round();

            //add vertices in world space coordinates (which are equal to the leaflet pixel coordinates)
            geometry.vertices.push(new THREE.Vector3(p1.x, p1.y, 0.0));
            geometry.vertices.push(new THREE.Vector3(p2.x, p2.y, 0.0));
            geometry.vertices.push(new THREE.Vector3(p3.x, p3.y, 0.0));

            //connect the added vertices (currently no shared vertices - should not be needed)
            geometry.faces.push(new THREE.Face3(countTriangles * 3, countTriangles * 3 + 1,
                countTriangles * 3 + 2));

            colorValues[vIndex] = heatMap.colorToVec4(triangle.a.color);
            colorValues[vIndex+1] = heatMap.colorToVec4(triangle.b.color);
            colorValues[vIndex+2] = heatMap.colorToVec4(triangle.c.color);

            vIndex+=3;

            countTriangles++;
        }
    });

    scene.remove(this.mesh);

    this.material = new THREE.ShaderMaterial( {
        attributes: attributes,
        vertexShader: this.vertexShader,
        fragmentShader: this.fragmentShader
    } );

    this.mesh = new THREE.Mesh(geometry, this.material);
    this.mesh.position.set(0, 0, 0);
    scene.add(this.mesh);
    renderer.render(scene, camera);
};

LHeatMap.prototype.colorToVec4 = function (color) {
    var color = new THREE.Color(color);

    if(color.r == 0.0 && color.g == 0.0 && color.b == 0.0){
        return new THREE.Vector4(color.r, color.g, color.b, 0.0);
    } else {
        return new THREE.Vector4(color.r, color.g, color.b, this.transparency);
    }
};

/** updates the viewport, should be called whenever the canvas is resized or reinitialized **/
LHeatMap.prototype.updateViewport = function (canvas) {
    this.width = canvas.width;
    this.height = canvas.height;

    this.renderer.setSize(this.width, this.height);

    this.scene.remove(this.camera);

    /** orthographic camera using the leaflet map width and height as size, origin (0, 0) is top-left **/
    this.camera = new THREE.OrthographicCamera(0, this.width, 0, this.height, 1, 1000);
    this.camera.position.set(0, 0, 10);
    this.camera.lookAt(this.scene.position);
    this.scene.add(this.camera);
};

/** placeholder, currently using straight the fpTriangles from localization.js **/
LHeatMap.prototype.setFpTriangles = function (fpTriangles) {
    this.fpTriangles = fpTriangles;

    this.draw();
};
