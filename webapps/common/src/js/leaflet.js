angularApp.factory('leaflet', function ($rootScope, $http, config) {

    return function () {
        var markers = {};

        var heatConfig = {};

        var map;
        var heat;
        var heatEnabled = false;

        var markersEnabled = true;

        var addCurrentHeat = true;

        var icons = {
            ZOMBIE: L.icon({
                iconUrl: 'images/shopper_gray.png',
                iconSize: [28, 48],
                iconAnchor: [14, 46],
                popupAnchor: [0, 0]
            }),
            PAID: L.icon({
                iconUrl: 'images/shopper_green.png',
                iconSize: [28, 48],
                iconAnchor: [14, 46],
                popupAnchor: [0, 0]
            }),
            SHOPPING: L.icon({
                iconUrl: 'images/shopper_red.png',
                iconSize: [28, 48],
                iconAnchor: [14, 46],
                popupAnchor: [0, 0]
            }),
            CHECKOUT: L.icon({
                iconUrl: 'images/shopper_yellow.png',
                iconSize: [28, 48],
                iconAnchor: [14, 46],
                popupAnchor: [0, 0]
            })

        };

        function recordClick(e) {
            $rootScope.$broadcast('mapClick', e);
            $rootScope.$apply();
        }

        function init(cfg, mapElementName) {
            if (mapElementName === undefined) {
                mapElementName = 'map';
            }

            // clean up old map
            var mapElement = document.getElementById(mapElementName);
            var parent = mapElement.parentElement;
            parent.removeChild(mapElement);

            // create new map element
            var newMapElement = document.createElement('div');
            newMapElement.id = mapElementName;
            parent.appendChild(newMapElement);

            // create new leaflet map
            map = L.map(mapElementName, {
                attributionControl: false
            });

            heatEnabled = false;
            heat = L.heatLayer([], heatConfig);

            map.on('click', recordClick);

            L.tileLayer(cfg.tileUrl, {
                minZoom: cfg.minZoom,
                maxZoom: cfg.maxZoom,
                tms: true
            }).addTo(map);

            var center = [
                (cfg.boundingBox[0][0] + cfg.boundingBox[1][0]) / 2, (cfg.boundingBox[0][1] + cfg.boundingBox[1]
                    [1]) / 2
            ];
            map.setView(center);
            map.setMaxBounds(cfg.boundingBox);
            map.fitBounds(cfg.boundingBox);

            return map;
        }

        function enableHeat(enabled) {
            heatEnabled = enabled;
            if (map) {
                if (enabled) {
                    map.addLayer(heat);
                } else {
                    map.removeLayer(heat);
                }
            }
            heat.setLatLngs([]);
        }

        function updateConfig(cfg) {
            heatConfig = cfg;
            heat.setOptions(cfg);
        }

        function addHeat(latLngs) {
            if (heatEnabled && addCurrentHeat) {
                for (var i = 0; i < latLngs.length; i++) {
                    heat.addLatLng(latLngs[i]);
                }
            }
        }

        function showHeat(latLngs, addHeat) {
            addCurrentHeat = addHeat;
            heat.setLatLngs(latLngs);
        }

        function addOrUpdateMarker(customer, cart) {
            var marker = markers[customer.deviceId];

            if (!marker) {
                marker = L.marker(customer.location, {
                    icon: icons[customer.state]
                });
                marker.id = customer.deviceId;
                marker.on('click', function (e) {
                    cart.show(e.target.id);
                });

                if (markersEnabled) {
                    marker.addTo(map);
                }


                marker.update();
                markers[customer.deviceId] = marker;
            } else {
                marker.setLatLng(customer.location);
                marker.setIcon(icons[customer.state]);
                marker.update();
            }
        }

        function removeAllMarkers() {
            for(var marker in markers) {
                if(markers.hasOwnProperty(marker)) {
                    map.removeLayer(markers[marker]);
                    delete markers[marker];
                }
            }
        }

        function removeMarker(customer) {
            var marker = markers[customer.deviceId];

            if (marker) {
                map.removeLayer(marker);
                delete markers[customer.deviceId];
            }
        }

        function enableMarker(enabled) {
            if (markersEnabled === enabled) {
                return;
            }
            markersEnabled = enabled;
            for (var deviceId in markers) {
                if (enabled) {
                    markers[deviceId].addTo(map);
                    markers[deviceId].update();
                } else {
                    map.removeLayer(markers[deviceId]);
                }
            }
        }

        /**
         * Loads map related configuration and store them in the config object.
         * @param selectedMap the name of the map for which the configuration is loaded
         * @param success callback called, when configuration was loaded, parsed and stored
         */
        function loadMapConfig(selectedMap, success, error) {
            var cfg = config();
            cfg.selectedMap = selectedMap;
            cfg.tileUrl = cfg.mapUrl + '/maps/' + selectedMap + '/tiles/{z}/{x}/{y}.jpg';


            $http({
                url: serverEndpoint + 'msr?path=/maps/' + selectedMap + '/tiles/tilemapresource.xml',
                method: 'GET',
                transformResponse: [function (data) {
                    // Do whatever you want!
                    return data;
                }]
            }).success(function (result) {
                // parse XML
                var xml = $($.parseXML(result));

                // get boundingbox element
                var boundingBox = xml.find('BoundingBox');

                cfg.boundingBox = [
                    [parseFloat(boundingBox.attr('minx')), parseFloat(boundingBox.attr('miny'))],
                    [parseFloat(boundingBox.attr('maxx')), parseFloat(boundingBox.attr('maxy'))]
                ];

                var minZoom = 0xffff;
                var maxZoom = 0;

                // find smallest and largest order
                // ('order' is the OSM-name for zoomlevel, as specified in the tilemapresource.xml)
                $(xml).find('TileSet').each(function () {
                    var order = parseInt($(this).attr('order'));
                    minZoom = minZoom < order ? minZoom : order;
                    maxZoom = maxZoom > order ? maxZoom : order;
                });

                cfg.minZoom = minZoom;
                cfg.maxZoom = maxZoom;

                if (success) {
                    success();
                }
            }).error(function () {
                if (error) {
                    error();
                }
            });

        }


        return {
            init: init,
            enableHeat: enableHeat,
            addOrUpdateMarker: addOrUpdateMarker,
            removeAllMarkers: removeAllMarkers,
            removeMarker: removeMarker,
            addHeat: addHeat,
            updateConfig: updateConfig,
            showHeat: showHeat,
            enableMarker: enableMarker,
            loadMapConfig: loadMapConfig,
            getMap: function () {
                return map;
            }
        };
    };

});
