angularApp.controller('VersionController', function ($scope, $http) {
    $http.get(serverEndpoint + 'version').success(function (result) {
        $scope.version = result;
    });
});