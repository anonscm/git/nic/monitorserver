angularApp.controller('HeaderController', function ($rootScope, $scope, osiam, $cookies) {
    $scope.logout = function () {
        osiam.logout();
    };

    $rootScope.username = $cookies.get('username');

    if(typeof $rootScope.username === 'undefined' || $rootScope.username == ''){
        $rootScope.username = "No user";
    }
});