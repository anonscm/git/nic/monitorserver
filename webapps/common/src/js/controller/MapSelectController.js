angularApp.controller('MapSelectController', function ($rootScope, $scope, config) {
    $scope.mapList = null;
    $scope.selectedMap = null;
    $scope.mapSelected = function (map) {
        var cfg = config();
        if (cfg.selectedMap !== map) {
            cfg.selectedMap = map;
            $rootScope.$broadcast('MAPSELECTED', map);

        }
    };


    $rootScope.whenReady(function () {
        $scope.mapList = config().mapList;
        $scope.selectedMap = config().selectedMap;
    });

});