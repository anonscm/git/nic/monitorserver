var ColorUtil = {};


/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  l       The lightness
 * @return  String          RGB value
 */
ColorUtil.hslToRgb = function (h, s, l) {
    var r, g, b;

    if (s === 0) {
        r = g = b = l; // achromatic
    } else {
        var hue2rgb = function hue2rgb(p, q, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1 / 6) return p + (q - p) * 6 * t;
            if (t < 1 / 2) return q;
            if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
            return p;
        };

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1 / 3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1 / 3);
    }

    return "rgb(" + Math.round(r * 255) + "," + Math.round(g * 255) + "," + Math.round(b * 255) + ")";
};

/**
 * Gets color based on our gradient
 * @param min min reference value
 * @param max max reference value
 * @param value value, should be between min and max
 * @returns {String} RGB color code
 */
ColorUtil.getColor = function (min, max, value) {
    if (value < min) {
        value = min;
    } else if (value > max) {
        value = max;
    }


    var start = 240 / 360; // blue, h in hsl
    var end = 0; // red, h in hsl

    var p = 1 - (value - min) / (max - min); //inverse relative position of value between min and max
    var h = p * start + end;

    return ColorUtil.hslToRgb(h, 1.0, 0.5);
};

/**
 * Gets the color representing the transmitters mean value of a fingerprint
 * @param triangle object with positions as x, y in a, b and c
 * @param minValue minimal value
 * @param maxValue maximal value
 * @param getValue function to calculate the value for a point. the point object will be passed as parameter
 * @param specialValues object with special values
 */
ColorUtil.setColorOfTrianglePoint = function (triangle, minValue, maxValue, getValue, specialValues) {

    function addColor(point) {
        var value = getValue(point);
        var color = specialValues[value];

        if (!color) {
            color = ColorUtil.getColor(minValue, maxValue, value);
        }

        point.color = color;
    }

    addColor(triangle.a);
    addColor(triangle.b);
    addColor(triangle.c);

};

/**
 * Sets the color for each point in each triangle
 * @param triangles array of triangles
 * @param minValue minimal value
 * @param maxValue maximal value
 * @param getValue function to calculate the value for a point. the point object will be passed as parameter
 * @param specialValues object with special values
 */
ColorUtil.setColorOfTrianglePoints = function (triangles, minValue, maxValue, getValue, specialValues) {
    triangles.forEach(function (triangle) {
        ColorUtil.setColorOfTrianglePoint(triangle, minValue, maxValue, getValue, specialValues);
    });
};

