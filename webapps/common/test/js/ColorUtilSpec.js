describe('ColorUtil ::', function () {


    it('calculate the proper color', function () {
        expect(ColorUtil.getColor(0, 8, 0)).toEqual("rgb(0,0,255)");

        expect(ColorUtil.getColor(0, 8, 0.25)).toEqual("rgb(0,32,255)");
        expect(ColorUtil.getColor(0, 8, 0.5)).toEqual("rgb(0,64,255)");
        expect(ColorUtil.getColor(0, 8, 1)).toEqual("rgb(0,128,255)");
        expect(ColorUtil.getColor(0, 8, 1.5)).toEqual("rgb(0,191,255)");
        expect(ColorUtil.getColor(0, 8, 1.75)).toEqual("rgb(0,223,255)");

        expect(ColorUtil.getColor(0, 8, 2)).toEqual("rgb(0,255,255)");
        expect(ColorUtil.getColor(0, 8, 4)).toEqual("rgb(0,255,0)");
        expect(ColorUtil.getColor(0, 8, 6)).toEqual("rgb(255,255,0)");
        expect(ColorUtil.getColor(0, 8, 8)).toEqual("rgb(255,0,0)");
    });

    var triangles = [
        {
            a:{ foo:0},
            b:{ foo:2},
            c:{ foo:4}
        }
    ];

    it('sets color of all points in triangles', function () {
        ColorUtil.setColorOfTrianglePoints(triangles, 0, 8, function(point) {return point.foo;}, {});

        expect(triangles[0].a.color).toEqual("rgb(0,0,255)");
        expect(triangles[0].b.color).toEqual("rgb(0,255,255)");
        expect(triangles[0].c.color).toEqual("rgb(0,255,0)");
    });

    it('sets a color for a special value', function() {
        ColorUtil.setColorOfTrianglePoints(triangles, 0, 8, function() {return 2;}, {2:'rgb(0,0,0)'});

        expect(triangles[0].a.color).toEqual("rgb(0,0,0)");

    });

});