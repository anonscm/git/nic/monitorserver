localStorage.clear();

serverEndpoint = '';

describe('Controllers ::', function () {

    beforeEach(module('angularApp'));
    describe('VersionController', function () {
        var scope, ctrl, $httpBackend;
        var version = {version: "1"};

        beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;

            $httpBackend.whenGET('version').respond(version);
            $httpBackend.whenGET('osiam/loggedIn').respond(false);

            scope = $rootScope.$new();
            ctrl = $controller('VersionController', {$scope: scope});

            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.flush();
        }));

        it('should be defined', function () {
            expect(ctrl).toBeDefined();
        });

        afterEach(function () {
            expect(scope.version).toEqual(version);

        });
    });
});
