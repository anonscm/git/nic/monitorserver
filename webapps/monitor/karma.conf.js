module.exports = function (config) {
    config.set({

        // root path location that will be used to resolve all relative paths in files and exclude sections
        basePath: '.',

        // files to include, ordered by dependencies
        files: [
            'monitor/js/vendor.js',
            'monitor/js/app.js',
            'bower/angular-mocks/angular-mocks.js',
            '../common/test/js/**/*.js',
            'test/js/**/*.js'
        ],


        // karma has its own autoWatch feature but Grunt watch can also do this
        autoWatch: false,

        // testing framework, be sure to install the correct karma plugin
        frameworks: ['jasmine'],

        // browsers to test against, be sure to install the correct browser launcher plugins
        browsers: ['PhantomJS'],

        // map of preprocessors that is used mostly for pluginsmonitor
        preprocessors: {
            // test coverage
            'src/js/**/*.js': ['coverage']
        },

        reporters: ['progress', 'coverage', 'junit'],

        // list of karma plugins
        plugins: [
            'karma-coverage',
            'karma-jasmine',
            'karma-phantomjs-launcher',
            'karma-junit-reporter'
        ],

        coverageReporter: {
            // type of file to output, use text to output to console
            type: 'text',
            // directory where coverage results are saved
            dir: 'target/coverage/',
            // if type is text or text-summary, you can set the file name
            file: 'coverage.txt'
        },

        junitReporter: {
            outputDir: 'target/'
        }
    })
}