// fix for missing window.location.origin in IE
if (!window.location.origin) {
    window.location.origin = window.location.protocol + "//" + window.location.host;
}

var serverEndpoint = window.location.origin + '/monitor/';


var mapserverEndpoint = window.location.origin + '/mapserver/';


var SELLFIO_DEBUG = false;
if (SELLFIO_DEBUG) {
    serverEndpoint = 'http://invio-mapserver.lan.tarent.de:8080/monitor/';

    //In case in debug mode the mapserver is in a different machine, then change localhost with the mapserver's IP
    mapserverEndpoint = 'http://invio-mapserver.lan.tarent.de:8080/mapserver/';
}

var angularApp = angular.module('angularApp', ['ngRoute', 'ngCookies', 'ui.bootstrap', 'ui.bootstrap.datetimepicker']);
