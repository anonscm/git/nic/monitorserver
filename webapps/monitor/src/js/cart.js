
/**
 * Show shopping cart of customer in pop up window
 */
angularApp.factory('cart', function ($modal, $rootScope) {
    var ModalInstanceCtrl = function ($scope, $modalInstance, customer) {

        $scope.customer = customer;

        $scope.ok = function () {
            $modalInstance.close();
        };
    };

    return {
        show: function (id) {

            var modalInstance = $modal.open({
                templateUrl: 'template/cartPopup.html',
                controller: ModalInstanceCtrl,
                resolve: {
                    customer: function () {
                        return $rootScope.customers[id];
                    }
                }
            });
        }
    };
});