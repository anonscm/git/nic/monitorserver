/**
 * TODO:
 * * proper OSIAM integration (e.g. 3-legged oauth-flow
 */


/**
 * Configure routes
 */
angularApp.config(function ($routeProvider) {
    //TODO add map name to route param
    $routeProvider.when('/', {
        redirectTo: '/customer'
    }).when('/customer', {
        templateUrl: 'template/customer.html'
    }).when('/heatmap', {
        templateUrl: 'template/heatmap.html'
    }).when('/demo', {
        templateUrl: 'template/demo.html'
    }).otherwise({
        redirectTo: '/customer'
    });
    moment.locale('de');
});

/**
 * start the application
 */
angularApp.run(function ($rootScope, $location, osiam, config) {

    $rootScope.app = 'monitor';
    config();
    $rootScope.$on('loggedIn', function() {
        if($location.path() === '/' || $location.path() === '') {
            $location.path('/customer');
        }
    });

    // do login
    osiam.ensureLoggedIn();

});




