

/**
 * Records routes for dummy customers
 */
angularApp.controller('DemoRouteController', function ($rootScope, $scope, $http, leaflet, config) {
    $scope.routes = [];
    $scope.currentRoute = [];
    $scope.status = 3;
    $scope.data = {
        product: '',
        name: ''
    };

    $scope.msg = '';

    // global flag for demo recording, needed to disable heatmap
    $rootScope.demoEnabled = false;

    var lf = leaflet();

    var pointInRoute = 0;

    $scope.$on('mapClick', function (event, data) {
        if (!$scope.demoEnabled) {
            return;
        }

        lf.addHeat([data.latlng]);

        var product = $scope.data.product;
        if (product === '') {
            product = -1;
        } else {
            product = parseInt(product);
        }

        var routePoint = {
            routeId: $scope.routes.length,
            pointInRoute: pointInRoute,
            map: config().selectedMap,
            name: $scope.data.name === '' ? '' + $scope.routes.length : $scope.data.name,
            deviceId: $scope.data.name === '' ? '' + $scope.routes.length : $scope.data.name,
            lat: data.latlng.lat,
            lng: data.latlng.lng,
            status: parseInt($scope.status),
            product: product
        };

        $scope.currentRoute.push(routePoint);
        $scope.data.product = '';

        pointInRoute++;
        $scope.$apply();
    });


    $scope.saveStatus = function (status) {
        $scope.status = status;
    };

    $scope.saveRoute = function () {
        $scope.routes.push($scope.currentRoute);
        $scope.currentRoute = [];
        $scope.data.name = '';

        leaflet.showHeat([], true);
    };

    $scope.load = function () {
        $http.get(serverEndpoint + '/demo/' + config().selectedMap).success(function (result) {
            $scope.routes = result;
        });
    };

    $scope.saveRoutes = function () {
        $http.post(serverEndpoint + '/demo', $scope.routes).success(function () {
            $scope.msg = 'Saved!';
        }).error(function () {
            $scope.msg = 'Failed to save data on server. Copy data manually from JS console';
            console.log('Route Data: ' + JSON.stringify($scope.routes));
        });
    };

    $scope.toggleDemo = function () {
        $rootScope.demoEnabled = !$rootScope.demoEnabled;
        leaflet.enableHeat($rootScope.demoEnabled);
        leaflet.enableMarker(!$rootScope.demoEnabled);
    };

    function initMap() {
        var cfg = config();

        lf.loadMapConfig(cfg.selectedMap, function () {

            lf.init(cfg);
            lf.enableHeat(true);

        });
    }


    initMap();
});
