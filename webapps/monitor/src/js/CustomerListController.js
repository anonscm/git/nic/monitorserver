
/**
 * Show list of customers
 */
angularApp.controller('CustomerListController', function ($scope, $rootScope, config, cart) {
    $scope.removeAll = function () {
        for (var customer in $rootScope.customers) {
            if($rootScope.customers.hasOwnProperty(customer)) {
                delete $rootScope.customers[customer];
            }
        }
    };

    $scope.$on('UPDATE', function (event, data) {
        if(data.customer.map === config().selectedMap) {
            $rootScope.customers[data.customer.deviceId] = data.customer;
            $rootScope.$apply();
        }
    });

    $scope.$on('REMOVEALL', $scope.removeAll);

    $scope.$on('REMOVE', function (event, data) {
        delete $rootScope.customers[data.customer.deviceId];
        $rootScope.$apply();
    });


    $scope.whenReady(function () {
        $rootScope.customers = {};
    });

    $scope.$on('MAPSELECTED', $scope.removeAll);

    $scope.showCart = function (id) {
        cart.show(id);
    };

});