/**
 * Show map and markers
 */
angularApp.controller('MapController', function ($scope, $rootScope, $http, leaflet, config, websocket, cart) {

    var checkout;

    var lf = leaflet();

    function loadDefaultMap() {
        var cfg = config();
        var defaultMap = cfg.selectedMap;
        cfg.selectedMap = null;

        initMap(defaultMap);
    }

    function subscribeToMap() {
        if (websocket.isConnected()) {
            websocket.send({
                'map': config().selectedMap,
                'heat': false
            });
        }
    }

    function initMap(selectedMap) {
        var cfg = config();

        lf.loadMapConfig(selectedMap, function () {
            lf.removeAllMarkers();
            lf.init(cfg);


            subscribeToMap();

            checkout = null;
            $http({
                url: serverEndpoint + '/msr?path=/maps/' + selectedMap + '/data/' + selectedMap + '.osm',
                method: 'GET',
                transformResponse: [function (data) {
                    // do not modify data.
                    // required, else angular will try to parse the data as json
                    return data;
                }]
            }).success(
                function (result) {

                    var rect = {
                        from: {
                            lat: 180,
                            lng: 180
                        },
                        to: {
                            lat: 0,
                            lng: 0
                        }
                    };

                    var xml = $($.parseXML(result));
                    var foundPos = 0;

                    xml.find('[v=\'self_checkout_area\']').parent().children('nd').each(function () {

                        var pos = xml.find('[id=' + $(this).attr('ref') + ']');

                        var lat = parseFloat(pos.attr('lat'));
                        var lng = parseFloat(pos.attr('lon'));

                        rect.from.lat = Math.min(rect.from.lat, lat);
                        rect.from.lng = Math.min(rect.from.lng, lng);

                        rect.to.lat = Math.max(rect.to.lat, lat);
                        rect.to.lng = Math.max(rect.to.lng, lng);

                        foundPos++;
                    });

                    if (foundPos >= 4) {
                        checkout = rect;
                    }

                });
        });
    }

    function isInCheckout(location) {
        if (!location || !location[0] || !location[1]) {
            return false;
        }
        if (checkout == null) {
            return false;
        }

        var lat = location[0];
        var lng = location[1];

        return checkout.from.lat <= lat && checkout.to.lat >= lat &&
            checkout.from.lng <= lng && checkout.to.lng >= lng;
    }


    $scope.$on('UPDATE', function (event, data) {
        if(data.customer.map === config().selectedMap) {
            if (data.customer.state === 'SHOPPING' && isInCheckout(data.customer.location)) {
                data.customer.state = 'CHECKOUT';

                //re-send event with updated data
                $rootScope.$broadcast('UPDATE', data);
                return;
            }

            lf.addOrUpdateMarker(data.customer, cart);
        }
    });

    $scope.$on('REMOVE', function (event, data) {
        lf.removeMarker(data.customer);
    });

    $scope.$on('REMOVEALL', function (event, data) {
       lf.removeAllMarkers();
    });


    $scope.$on('MAPSELECTED', function (event, map) {
        initMap(map);
    });

    $scope.whenReady(function () {
        initMap(config().selectedMap);
        websocket.connect();
    });

    $scope.$on('connected', function (event, data) {
        subscribeToMap()
    });

    $scope.$on('reconnected', function (event, data) {
        subscribeToMap()
    });

    $scope.$on('$destroy', function iVeBeenDismissed() {
        websocket.disconnect();
    });


});