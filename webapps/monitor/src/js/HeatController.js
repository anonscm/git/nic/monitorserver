angularApp.controller('HeatController', function ($rootScope, $scope, $http, $filter, leaflet, config, websocket) {


    $scope.dateOptions = {
        showWeeks: true,
        startingDay: 1
    };

    $scope.timeOptions = {
        readonlyInput: true,
        showMeridian: false
    };

    /** flags which datetime picker is open */
    $scope.open = {
        from: false,
        to: false
    };

    /** opens a datetime picker */
    $scope.openCalendar = function (e, date) {
        e.preventDefault();
        e.stopPropagation();

        // close other calendar
        if (date === 'from') {
            $scope.open.to = false;
        } else {
            $scope.open.from = false;
        }

        $scope.open[date] = true;
    };

    $scope.heatConfig = {
        radius: 25,
        blur: 15,
        numOfCellsinX: 1
    };

    $scope.openRequests = 0;

    $scope.data = {
        from: new Date(),
        to: new Date()
    };

    // default to the last 15 minutes
    $scope.data.from.setMinutes($scope.data.from.getMinutes() - 15);

    $scope.liveHeat = false;


    var heatMap = {};
    var lf = leaflet();

    $scope.toggleLiveHeat = function (liveHeatEnabled) {
        lf.showHeat([], liveHeatEnabled);

        $scope.liveHeat = liveHeatEnabled;

        if (liveHeatEnabled) {
            hideUniqueCustomersHeatmap();
        }
    };

    function initMap() {
        var cfg = config();

        lf.loadMapConfig(cfg.selectedMap, function () {

            var map = lf.init(cfg, 'map');
            lf.enableHeat(true);
            lf.enableMarker(false);
            heatMap = new LHeatMap(map);

            if (websocket.isConnected()) {
                websocket.send({
                    'map': cfg.selectedMap,
                    'heat': true
                });
            }

        });

    }

    $scope.applyConfig = function (radius, blur) {
        $scope.heatConfig.radius = radius;
        $scope.heatConfig.blur = blur;

        lf.updateConfig({
            radius: $scope.heatConfig.radius,
            blur: $scope.heatConfig.blur
        });
    };


    /**
     * Hides the unique customers heatmap by setting and empty array as data
     */
    function hideUniqueCustomersHeatmap() {
        heatMap.setFpTriangles([]);
        $scope.colorLegendFields = null;
    }

    /**
     * Loads data to show the unique customers heatmap
     * @param unique flag if only unique visits shall be displayed
     */
    $scope.showCustomers = function (unique) {
        var cfg = config();

        var start = moment($scope.data.from).format('x');
        var end = moment($scope.data.to).format('x');

        console.log('Getting heat data from ' + $scope.data.from + ' ' + start +
            ' to ' + $scope.data.to + ' ' + end);
        $scope.openRequests++;
        $http({
            method: 'GET',
            url: serverEndpoint + '/heat/map',
            params: {
                map: cfg.selectedMap,
                start: start,
                end: end,
                unique: unique,
                downSample: $scope.heatConfig.numOfCellsinX
            }
        }).success(function (result) {
            if (result !== '' && result.length && result.length > 0) {
                $rootScope.monitorError = null;
                showUniqueHeatMap(result);
            } else {
                $rootScope.monitorError = 'No Data. Bad Parameters?';
            }


            $scope.openRequests--;
        }).error(function () {
            $scope.openRequests--;
        });
    };



    /**
     * Displays the unique customers heatmap.
     * @param result the result from the REST API
     */
    function showUniqueHeatMap(result) {
        lf.showHeat([], false);

        var heatPoints = [];
        var id = 0;
        var minCount = Number.MAX_VALUE;
        var maxCount = 0;

        // result is a 2D array; create new array for delaunay triangles
        result.forEach(function (a) {
            a.forEach(function (cell) {

                if (cell.count > 0) {
                    // zero visitors means transparent, so minCount is at least 1
                    minCount = Math.min(minCount, cell.count);
                }

                maxCount = Math.max(maxCount, cell.count);
                heatPoints.push({
                    // yes, x is latitude, y is longitude... ¯\_(ツ)_/¯
                    x: cell.pos[0],
                    y: cell.pos[1],
                    count: cell.count,
                    id: id++
                });
            });
        });

        if (minCount < maxCount) {
            var step = (maxCount - minCount) / 4;
            $scope.colorLegendFields = [minCount, minCount + step, minCount + 2 * step, minCount + 3 * step, maxCount];
        } else if (minCount === maxCount) {
            // if there is only one different value, the legend also has only 1 color
            $scope.colorLegendFields = [maxCount];

            // set minCount to something else, so the color calculation works properly
            minCount = 0;
        } else {
            $scope.colorLegendFields = null;
        }

        if ($scope.colorLegendFields) {
            for (var i = 0; i < $scope.colorLegendFields.length; i++) {
                //filter values with 2 decimal points
                $scope.colorLegendFields[i] = $filter('number')($scope.colorLegendFields[i], 2);
            }
        }

        var triangles = triangulate(heatPoints);

        ColorUtil.setColorOfTrianglePoints(triangles, minCount, maxCount, function (point) {
            return point.count;
        }, {0: 'transparent'});


        heatMap.setFpTriangles(triangles);

    }

    $scope.$on('MAPSELECTED', function (event, data) {
        initMap();

    });

    $scope.whenReady(function () {
        websocket.connect();
        initMap();
    });

    /**
     * Subscribe to heat data as soon as we are connected
     */
    $scope.$on('connected', function (event, data) {
        websocket.send({
            'map': config().selectedMap,
            'heat': true
        });
    });

    /**
     * Add heat points to map if live heat is enabled
     */
    $scope.$on('HEAT', function (event, data) {
        if ($scope.liveHeat) {
            lf.addHeat(data.positions);
        }
    });


    /**
     * disconnect from websocket
     */
    $scope.$on('$destroy', function iVeBeenDismissed() {
        websocket.disconnect();
    });

});