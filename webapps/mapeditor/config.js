// fix for missing window.location.origin in IE
if (!window.location.origin) {
    window.location.origin = window.location.protocol + "//" + window.location.host;
}

var serverEndpoint = window.location.origin + window.location.pathname + '/';
var SELLFIO_DEBUG = false;
if (SELLFIO_DEBUG) {
    serverEndpoint = 'http://localhost:8080/mapeditor/';
}

//serverEndpoint = 'http://localhost:8080/mapeditor/';
//serverEndpoint = 'http://invio-mapserver.lan.tarent.de:8080/mapeditor/';


var angularApp = angular.module('angularApp', ['ngRoute', 'ngCookies', 'ui.bootstrap', 'ui.bootstrap.datetimepicker']);