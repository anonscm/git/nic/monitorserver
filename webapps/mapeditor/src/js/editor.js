/**
 * Configure routes
 */
angularApp.config(function ($routeProvider) {
    $routeProvider.when('/mapeditor', {
        templateUrl: 'template/editor.html',
        reloadOnSearch: false
    }).otherwise({
        redirectTo: '/mapeditor'
    });

    moment.locale('de');
});


/**
 * start the application
 */
angularApp.run(function ($rootScope, osiam, $location) {

    $rootScope.app = 'mapeditor';

    $rootScope.$on('ready', function (event, data) {
       $location.path('/mapeditor');
    });

    // do login
    osiam.ensureLoggedIn();
});


