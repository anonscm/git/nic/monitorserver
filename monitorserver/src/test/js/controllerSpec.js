localStorage.clear();
describe('Controllers ::', function () {

    beforeEach(module('angularApp'));
    describe('VersionController', function () {
        var scope, ctrl, $httpBackend;
        var version = {version: "1"};

        beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;

            $httpBackend.whenGET('version').respond(version);
            $httpBackend.whenGET('osiam/loggedIn').respond(false);

            scope = $rootScope.$new();
            ctrl = $controller('VersionController', {$scope: scope});

            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.flush();
        }));

        it('should be defined', function () {
            expect(ctrl).toBeDefined();
        });

        afterEach(function () {
            expect(scope.version).toEqual(version);
        });
    });

    describe('OsiamFactory', function () {
        var scope, $httpBackend, window;
        beforeEach(inject(function (_$httpBackend_, $window, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;
            window = $window;
            $httpBackend.whenGET('osiam/loggedIn').respond(false);

            scope = $rootScope.$new();

            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.flush();
        }));

        it('can get an instance of my factory', inject(function (osiam) {
            expect(osiam).toBeDefined();
        }));

        it('goes to login page when not logged in', inject(function (osiam) {
            osiam.ensureLoggedIn();

            expect(window.location.hash).toEqual('#/login');
            expect(scope.loggedIn).toBeFalsy();
        }));

        /*
        // this test does not work properly
        describe('login success', function () {

            var success = true;
            var loggedin = false;


            beforeEach(function () {
                $httpBackend.whenPOST('osiam/login').respond("true");

                scope.$on("loggedIn", function(event, data){
                    console.log("on loggedIn");
                    loggedin = true;
                });
            });

            it('shall pass', inject(function (osiam) {
                osiam.login("user", "password", function (msg) {
                    console.log("Errorhandler: "+msg);
                    success = false
                });
            }));

            afterEach(function(){
                expect(success).toBeTruthy();
                expect(scope.loggedIn).toBeTruthy();
                expect(loggedin).toBeTruthy();

            });
        });
        */

    });

});
