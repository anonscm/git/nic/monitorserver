package de.tarent.invio.spring.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tarent.invio.spring.model.MonitorMessage;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mley on 14.11.14.
 */
public class MonitorHandlerTest {

    MonitorHandler monitorHandler;
    UpdateService updateServiceMock;
    @Before
    public void setup() {
        monitorHandler = new MonitorHandler();
        updateServiceMock = mock(UpdateService.class);
        monitorHandler.setUpdateService(updateServiceMock);

        List<MonitorMessage> initial = new ArrayList<>();
        when(updateServiceMock.getAll("map1")).thenReturn(initial);
        when(updateServiceMock.getAll("map2")).thenReturn(initial);
    }



    @Test
    public void testNoHeat() throws IOException {
        WebSocketSession session = mock(WebSocketSession.class);

        monitorHandler.afterConnectionEstablished(session);

        // not subscribed to heat messages
        monitorHandler.handleTextMessage(session, new TextMessage("{\"map\":\"map1\",\"heat\":false}"));

        MonitorMessage m = new MonitorMessage();
        m.setEvent("HEAT");

        monitorHandler.broadcast("map1", m);

        // so no heat message shall be received
        verify(session, times(1)).sendMessage(any(TextMessage.class));

    }

    @Test
    public void testHeat() throws IOException {
        WebSocketSession session = mock(WebSocketSession.class);

        monitorHandler.afterConnectionEstablished(session);

        // subscribed to heat message
        monitorHandler.handleTextMessage(session, new TextMessage("{\"map\":\"map1\",\"heat\":true}"));

        MonitorMessage m = new MonitorMessage();
        m.setEvent("HEAT");

        monitorHandler.broadcast("map1", m);

        // so 1 heat message expected
        verify(session, times(2)).sendMessage(any(TextMessage.class));

    }


    @Test
    public void testRegisterAndInitialDataSend() throws Exception {
        WebSocketSession session = mock(WebSocketSession.class);
        List<MonitorMessage> initial = new ArrayList<>();
        MonitorMessage m = new MonitorMessage();
        m.setEvent("BLA");
        initial.add(m);
        when(updateServiceMock.getAll("map1")).thenReturn(initial);
        when(updateServiceMock.getAll("map2")).thenReturn(initial);

        monitorHandler.afterConnectionEstablished(session);
        monitorHandler.handleTextMessage(session, new TextMessage("{\"map\":\"map1\"}"));
        TextMessage tm = new TextMessage(new ObjectMapper().writeValueAsString(m));
        verify(session).sendMessage(tm);
    }


    @Test
    public void testRegisterAndNoSubscription() throws Exception {
        WebSocketSession session = mock(WebSocketSession.class);

        // connect
        monitorHandler.afterConnectionEstablished(session);

        // send message
        MonitorMessage m = new MonitorMessage();
        m.setEvent("BLA");
        monitorHandler.broadcast("map", m);

        // only remove all message shall be sent
        TextMessage tm = new TextMessage(new ObjectMapper().writeValueAsString(MonitorMessage.REMOVE_ALL));
        verify(session).sendMessage(tm);
    }

    @Test
    public void testBroadcast() throws Exception {
        WebSocketSession session1 = mock(WebSocketSession.class);
        WebSocketSession session2 = mock(WebSocketSession.class);
        WebSocketSession session3 = mock(WebSocketSession.class);

        when(session1.getId()).thenReturn("1");
        when(session2.getId()).thenReturn("2");
        when(session3.getId()).thenReturn("3");

        monitorHandler.afterConnectionEstablished(session1);
        monitorHandler.afterConnectionEstablished(session2);
        monitorHandler.afterConnectionEstablished(session3);


        monitorHandler.handleTextMessage(session1, new TextMessage("{\"map\":\"map1\"}"));
        monitorHandler.handleTextMessage(session2, new TextMessage("{\"map\":\"map1\"}"));
        monitorHandler.handleTextMessage(session3, new TextMessage("{\"map\":\"map2\"}"));

        MonitorMessage m = new MonitorMessage();
        m.setEvent("BLUB");

        monitorHandler.broadcast("map1", m);
        TextMessage tm = new TextMessage(new ObjectMapper().writeValueAsString(m));
        verify(session1).sendMessage(tm);
        verify(session2).sendMessage(tm);
        verify(session3, times(1)).sendMessage(any(TextMessage.class));

        monitorHandler.afterConnectionClosed(session1, CloseStatus.NORMAL);
        monitorHandler.afterConnectionClosed(session2, CloseStatus.NORMAL);
        monitorHandler.afterConnectionClosed(session3, CloseStatus.NORMAL);

    }


}
