package de.tarent.invio.spring.rest;

import de.tarent.invio.spring.AbstractTest;
import de.tarent.invio.spring.websocket.UpdateService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by mley on 03.03.15.
 */
public class LocationControllerTest extends AbstractTest {

    @Autowired
    LocationController lc;

    @Test
    public void testLocationController() throws IOException {
        UpdateService usMock = mock(UpdateService.class);
        lc.setUpdateService(usMock);

        lc.updatePosition("map", "device", 0.0, 0.0, "SHOPPING");

        verify(usMock, times(0)).updatePosition(anyString(), anyString(), anyLong(), any(double[].class), anyString());

        lc.updatePosition("map", "device", 0.0, 1.0, "SHOPPING");
        lc.updatePosition("map", "device", 1.0, 1.0, "SHOPPING");
        lc.updatePosition("map", "device", 1.0, 0.0, "SHOPPING");

        verify(usMock, times(3)).updatePosition(anyString(), anyString(), anyLong(), any(double[].class), anyString());

    }


}
