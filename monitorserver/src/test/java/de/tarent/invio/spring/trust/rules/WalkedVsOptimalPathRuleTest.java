package de.tarent.invio.spring.trust.rules;

import de.tarent.invio.spring.AbstractTest;
import de.tarent.sellfio.web.model.Customer;
import de.tarent.invio.spring.util.PathPlanner;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * Created by mley on 20.03.15.
 */
@Transactional(value = "txName", readOnly = false)
@Ignore
public class WalkedVsOptimalPathRuleTest extends AbstractTest {

    @Autowired
    WalkedVsOptimalPathRule rule;

    @Autowired
    SessionFactory sessionFactory;

    Customer c;

    @Before
    public void before() {
        PathPlanner pp = new PathPlanner() {
            public double[][] planPath(String mapName, double[] start, double[] end) {
                // line of sight
                return new double[][] {start, end};
            }

        };

        ReflectionTestUtils.setField(rule, "pathPlanner", pp);

        c = new Customer();
        c.setDeviceId("a");
        c.setMap("map1");
        c.setLat(1.0);
        c.setLng(1.0);

        rule.removeCustomer("a");
    }

    private void walk( double[][] route) {

        for (double[] pos : route) {
            rule.updateCustomerPosition("a", pos);
        }

    }

    @Test
    public void testNoPreviousCart() {
        assertEquals(-1, rule.getTrust(c), 1E-6);
    }


    @Test
    public void testSamePosition() {
        rule.getTrust(c);
        walk(new double[][]{{1.0, 1.0}});
        assertEquals(-1, rule.getTrust(c), 1E-6);
    }

    @Test
    public void testStraightWalk() {
        rule.getTrust(c);

        // walked 3, optimal 3
        walk( new double[][]{{2.0, 1.0}, {3.0, 1.0}});

        c.setLat(4.0);
        assertEquals(1.0, rule.getTrust(c), 1E-6);
    }

    @Test
    public void testDetour() {
        rule.getTrust(c);

        // walked 4, optimal 2
        walk(new double[][]{{4.0, 1.0}});

        c.setLat(3.0);
        assertEquals(0.5, rule.getTrust(c), 1E-6);
    }

    @Test
    public void testAccumulation() {
        rule.getTrust(c);

        // walked: 3, optimal 3
        walk( new double[][]{{2.0, 1.0}, {3.0, 1.0}});

        c.setLat(4.0);
        rule.getTrust(c);

        // walked 5, optimal 1
        walk(new double[][]{{1.0, 1.0} });

        c.setLat(3.0);
        // sum: walked: 8, optimal 4
        double trust = rule.getTrust(c);

        assertEquals(0.5, trust, 1E-6);
    }

}
