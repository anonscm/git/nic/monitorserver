package de.tarent.invio.spring.util;

import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.spring.AbstractTest;
import de.tarent.invio.tracker.Tracker;
import de.tarent.sellfio.web.util.HaversineDistance;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by mley on 09.04.15.
 */
@Ignore
public class PathPlannerTest extends AbstractTest {

    @Autowired
    PathPlanner pathPlanner;

    private Map<String, Tracker> trackers;

    private Tracker tracker;

    @Before
    public void setup() throws IOException, ParserConfigurationException, SAXException {
        trackers = (Map<String, Tracker>) ReflectionTestUtils.getField(pathPlanner, "trackers");

        tracker = new Tracker();
        tracker.setDebugLogEnabled(true);

        PathPlanner.setBoundingBox(tracker, ClassLoader.getSystemResourceAsStream("tilemapresource.xml"));
        PathPlanner.setOccupancyGridMap(tracker, ClassLoader.getSystemResourceAsStream("ogm.png"));


        trackers.put("map", tracker);

    }

    @Test
    public void testReadOGM() throws IOException {
        Tracker t = new Tracker() {
            public void setOccupancyGridMap(float[] pixelValues, int width, int height) {
                assertEquals(9, pixelValues.length);
                assertEquals(3, width);
                assertEquals(3, height);

                assertEquals(1 / 3.0f, pixelValues[0], 0.0001); // red
                assertEquals(1 / 3.0f, pixelValues[1], 0.0001); // green
                assertEquals(1 / 3.0f, pixelValues[2], 0.0001); // blue

                assertEquals(1.0, pixelValues[4], 0.0001); // white
                assertEquals(0xcc / 255.0, pixelValues[5], 0.0001); // #ccc
                assertEquals(0x88 / 255.0, pixelValues[6], 0.0001); // #888
                assertEquals(0x44 / 255.0, pixelValues[7], 0.0001); // #444
                assertEquals(0.0, pixelValues[8], 0.0001); // black
            }
        };

        PathPlanner.setOccupancyGridMap(t, ClassLoader.getSystemResourceAsStream("ogm_3x3.png"));
    }

    /**
     * if no map metadata can be found, no tracker object shall be created. Result is the fallback to haversine distance.
     */
    @Test
    public void noValidOGMOrResources() {
        double[] start = new double[]{0.0, 0.0};
        double[] end = new double[]{1.0, 1.0};
        double result = pathPlanner.calcOptimalDistance("foobar", start, end);
        assertEquals(HaversineDistance.calcDistance(start, end), result, 0.1);
    }


    @Test
    public void testPlanPathTracker() throws IOException {

        InvioGeoPoint start = new InvioGeoPoint(0.999, 0.001);
        InvioGeoPoint end = new InvioGeoPoint(0.001, 0.001);

        final InvioGeoPoint[] invioGeoPoints = tracker.planPath(start, end);

        // tracker shall give a valid response
        assertTrue(invioGeoPoints.length > 0);

    }

    @Test
    public void testPathPlanner() {
        double[] start = new double[]{0.999, 0.001};
        double[] end = new double[]{0.001, 0.001};

        double dist = pathPlanner.calcOptimalDistance("map", start, end);

        // planned route is 18 squared, one square is about 0.1°
        double pixelSize = HaversineDistance.calcRouteDistance(new double[][]{{0.0, 0.0}, {0.1, 0.1}});

        assertEquals("difference between calculated optimal distance" +
                " and the number of gridcells of route shall be smaller " +
                "than sqrt(2) gridcells"
                , 18 * pixelSize, dist, Math.sqrt(2) * pixelSize);
    }

    @Test
    public void testGetVersion() {
        String version = tracker.version();
        assertNotNull(version);
    }

    @Test
    public void testSetOccupancyGridMap() throws IOException, SAXException, ParserConfigurationException {
        Tracker tracker = new Tracker() {
            public synchronized void setOccupancyGridMap(float[] pixelValues, int width, int height) {

                assertTrue((width * height) == pixelValues.length);
                for (float f : pixelValues) {
                    assertTrue(f == 0.0f || f == 1.0f);
                }
                assertTrue(pixelValues[0] == 1.0f);
                assertTrue(pixelValues[width] == 0.0f);
            }


        };

        PathPlanner.setOccupancyGridMap(tracker, ClassLoader.getSystemResourceAsStream("ogm.png"));


    }


}
