package de.tarent.invio.spring.trust;

import de.tarent.sellfio.web.model.Customer;
import de.tarent.invio.spring.trust.rules.WalkedVsOptimalPathRule;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mley on 20.03.15.
 */
public class TrustEngineTest  {

    TrustEngine trustEngine = new TrustEngine();

    List<Rule> rules = new ArrayList<>();

    Customer customer;

    @Before
    public void setup() {
        ReflectionTestUtils.setField(trustEngine, "rules", rules);
        customer = new Customer();
        customer.setDeviceId("abc");
    }

    @Test
    public void testNoRules() {
        trustEngine.calculateTrust(customer);

        assertEquals(0, customer.getConfidence(), 1E-10);
        assertEquals(0, customer.getTrust(), 1E-10);

    }

    @Test
    public void testValidRules() {
        rules.add(mockRule(0.5));
        rules.add(mockRule(1.0));
        rules.add(mockRule(0.0));

        trustEngine.calculateTrust(customer);

        assertEquals(1, customer.getConfidence(), 1E-10);
        assertEquals(0.5, customer.getTrust(), 1E-10);

    }

    @Test
    public void testSomeInvalidRules() {
        rules.add(mockRule(0.5));
        rules.add(mockRule(-1));
        rules.add(mockRule(0.0));

        trustEngine.calculateTrust(customer);

        assertEquals(0.66666, customer.getConfidence(), 1E-4);
        assertEquals(0.25, customer.getTrust(), 1E-10);

    }

    @Test
    public void testSomeMultipleCalculations() {
        WalkedVsOptimalPathRule rule = mock(WalkedVsOptimalPathRule.class);
        rules.add(rule);

        when(rule.getTrust(any(Customer.class))).thenReturn(0.5);
        trustEngine.calculateTrust(customer);

        when(rule.getTrust(any(Customer.class))).thenReturn(-1.0);
        trustEngine.calculateTrust(customer);


        assertEquals(0.5, customer.getConfidence(), 1E-10);

    }

    @Test
    public void testInvalidRules() {
        rules.add(mockRule(-1));
        rules.add(mockRule(2));
        rules.add(mockRule(-1));

        trustEngine.calculateTrust(customer);

        assertEquals(0, customer.getConfidence(), 1E-4);
        assertEquals(0, customer.getTrust(), 1E-10);

    }

    @Test
    public void testRemove() {
        trustEngine.removeCustomer("a");

        WalkedVsOptimalPathRule rule = mock(WalkedVsOptimalPathRule.class);
        rules.add(rule);

        trustEngine.removeCustomer("a");

        verify(rule, times(1)).removeCustomer(eq("a"));
    }

    private Rule mockRule(double trust) {
        WalkedVsOptimalPathRule rule = mock(WalkedVsOptimalPathRule.class);
        when(rule.getTrust(any(Customer.class))).thenReturn(trust);
        return rule;
    }

}
