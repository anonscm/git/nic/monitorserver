package de.tarent.invio.spring;

import org.junit.Test;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mley on 04.03.15.
 */
public class WebInitializerTest extends AbstractTest {

    WebInitializer wi = new WebInitializer();

    @Test
    public void test() throws ServletException {
        ServletContext mock = mock(ServletContext.class);

        ServletRegistration.Dynamic springMock = mock(ServletRegistration.Dynamic.class);
        when(mock.addServlet(eq("monitor"), any(DispatcherServlet.class))).thenReturn(springMock);

        FilterRegistration.Dynamic filterMock = mock(FilterRegistration.Dynamic.class);
        when(mock.addFilter(eq("characterEncoding"), any(CharacterEncodingFilter.class))).thenReturn(filterMock);

        wi.onStartup(mock);

        verify(springMock, times(1)).setAsyncSupported(eq(true));
        verify(springMock, times(1)).setLoadOnStartup(eq(1));
        verify(springMock, times(1)).addMapping(eq("/"));


    }
}
