package de.tarent.invio.spring.heatmap;

import de.tarent.sellfio.web.model.AggregatedPoint;
import de.tarent.sellfio.web.model.Map;
import de.tarent.sellfio.web.model.Point;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by mley on 08.09.15.
 */
public class AggregatedHeatMapTest {

    @Test
    public void test() {
        Map m = new Map();
        m.setId(1);
        m.setName("karte");
        m.setMinLat(0.0);
        m.setMinLon(0.0);
        m.setMaxLat(0.1);
        m.setMaxLon(0.1);
        m.setOriginLon(0.0);
        m.setOriginLat(0.0);

        // scale is set to get an area of 20x20 cells
        m.setScale(1112);

        AggregatedHeatMap ahm = new AggregatedHeatMap(m, 0);


        // first visitor
        ahm.aggregatePoint(new Point(1, 1, 1, new double[]{0.0, 0.01}));
        ahm.aggregatePoint(new Point(2, 1, 1, new double[]{0.01, 0.01}));
        ahm.aggregatePoint(new Point(3, 1, 1, new double[]{0.02, 0.01}));
        ahm.aggregatePoint(new Point(4, 1, 1, new double[]{0.02, 0.01}));

        // second visitor
        ahm.aggregatePoint(new Point(2, 1, 2, new double[]{0.01, 0.01}));
        ahm.aggregatePoint(new Point(2, 1, 2, new double[]{0.02, 0.01}));
        ahm.aggregatePoint(new Point(3, 1, 2, new double[]{0.02, 0.01}));

        // invalid coordinates
        ahm.aggregatePoint(new Point(4, 1, 2, new double[]{0.101, 0.01}));
        ahm.aggregatePoint(new Point(4, 1, 2, new double[]{0.01, 0.101}));
        ahm.aggregatePoint(new Point(4, 1, 2, new double[]{0.101, 0.101}));

        final AggregatedPoint[][] ap = ahm.recreateHeatMap(5);

        // 0.0, 0.01
        assertEquals(1, ap[1][0].getVisits());

        // 0.01, 0.01
        assertEquals(2, ap[1][1].getVisits());
        assertEquals(2, ap[1][1].getNumberOfUniqueVisits());

        // 0.02, 0.01
        assertEquals(4, ap[1][3].getVisits());
        assertEquals(2, ap[1][3].getNumberOfUniqueVisits());
    }


}
