package de.tarent.invio.spring.heatmap;

import de.tarent.sellfio.web.model.InvioGeoPoint;
import de.tarent.sellfio.web.model.AggregatedPoint;
import de.tarent.sellfio.web.model.HeatPoint;
import org.junit.Test;

import java.util.HashMap;

import static junit.framework.Assert.assertEquals;

/**
 * Created by mley on 15.09.15.
 */

public class UniqueVisitorsHeatMapTest2 {

    @Test
    public void testHeatMapGridSize() {
        UniqueVisitorsHeatMap uvhm = new UniqueVisitorsHeatMap();
        de.tarent.sellfio.web.model.Map map = new de.tarent.sellfio.web.model.Map();
        map.setScale(2000.0);
        map.setMinLat(0.0);
        map.setMinLon(0.0);
        map.setMaxLat(1.0);
        map.setMaxLon(1.0);

        AggregatedHeatMap heatMap = new AggregatedHeatMap(map, 0);
        HeatPoint[][] heatPoints = uvhm.calculateAggregatedHeatPoints(new HashMap<InvioGeoPoint, AggregatedPoint>(), heatMap, 1, false);
        assertEquals(112, heatPoints.length);
        assertEquals(112, heatPoints[0].length);


        heatPoints = uvhm.calculateAggregatedHeatPoints(new HashMap<InvioGeoPoint, AggregatedPoint>(), heatMap, 2, false);
        assertEquals(56, heatPoints.length);
        assertEquals(56, heatPoints[0].length);

        heatPoints = uvhm.calculateAggregatedHeatPoints(new HashMap<InvioGeoPoint, AggregatedPoint>(), heatMap, 3, false);
        assertEquals(38, heatPoints.length);
        assertEquals(38, heatPoints[0].length);

        heatPoints = uvhm.calculateAggregatedHeatPoints(new HashMap<InvioGeoPoint, AggregatedPoint>(), heatMap, 10, false);
        assertEquals(12, heatPoints.length);
        assertEquals(12, heatPoints[0].length);
    }
}
