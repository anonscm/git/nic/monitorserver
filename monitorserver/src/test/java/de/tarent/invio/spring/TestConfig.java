package de.tarent.invio.spring;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.IOException;

/**
 * Created by mley on 26.02.15.
 */
@Configuration
@PropertySources({
        @PropertySource(value = "classpath:version.properties"),
        @PropertySource(value = "classpath:config.properties"),
})
@EnableTransactionManagement
@ComponentScan(value = {"de.tarent.invio.spring.model", "de.tarent.invio.spring.rest",
        "de.tarent.invio.spring.util", "de.tarent.invio.spring.websocket", "de.tarent.invio.spring.service",
        "de.tarent.invio.spring.trust", "de.tarent.invio.spring.heatmap", "de.tarent.sellfio.web.rest",
        "de.tarent.sellfio.web.security", "de.tarent.sellfio.web.util", "de.tarent.sellfio.web.dao",
        "de.tarent.sellfio.web.model", "de.tarent.sellfio.web.util"})
public class TestConfig {

    /**
     * Enable Property source placeholders.
     *
     * @return PropertySourcesPlaceholderConfigurer
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }


    /**
     * Get the datasource
     *
     * @return DataSource object
     */
    @Bean(name = "dataSource")
    public DataSource dataSource() throws PropertyVetoException, ClassNotFoundException {
        Class.forName("org.h2.Driver");

        ComboPooledDataSource ds = new ComboPooledDataSource();
        ds.setDriverClass("org.h2.Driver");
        ds.setJdbcUrl("jdbc:h2:mem:inmem-testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
        ds.setUser("h2");
        ds.setPassword("h2");

        ds.setAcquireIncrement(5);
        ds.setMinPoolSize(5);
        ds.setMaxPoolSize(50);
        ds.setMaxIdleTime(1800);
        ds.setIdleConnectionTestPeriod(1800);
        ds.setMaxStatements(50);

        return ds;
    }

    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) {

        final LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
        sessionBuilder.scanPackages("de.tarent.sellfio.web.model");

        //needed to create schema in H2
        sessionBuilder.getProperties().setProperty("hibernate.hbm2ddl.auto", "create-drop");
        sessionBuilder.getProperties().setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");

        SessionFactory sessionFactory = sessionBuilder.buildSessionFactory();

        return sessionFactory;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template;

    }

    @Bean(name = "txName")
    public HibernateTransactionManager txName(DataSource dataSource, SessionFactory sessionFactory) throws IOException {
        final HibernateTransactionManager txName = new HibernateTransactionManager();
        txName.setSessionFactory(sessionFactory);
        txName.setDataSource(dataSource);
        return txName;
    }

}
