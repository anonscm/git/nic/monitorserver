package de.tarent.invio.spring.rest;

import de.tarent.invio.spring.AbstractTest;
import de.tarent.sellfio.web.model.Customer;
import de.tarent.invio.spring.websocket.UpdateService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by mley on 19.03.15.
 */
public class CustomerControllerTest extends AbstractTest {

    @Autowired
    CustomerController customerController;

    @Test
    public void testCartController() throws IOException {
        UpdateService mock = mock(UpdateService.class);
        ReflectionTestUtils.setField(customerController, "updateService", mock);

        Customer c = new Customer();
        c.setTrust(1);

        customerController.handleCustomerUpload(c);

        verify(mock, times(1)).updateCustomer(any(Customer.class));
        assertTrue(c.getTrust() == 0);
        assertTrue(c.getTimestamp() != 0);

    }
}
