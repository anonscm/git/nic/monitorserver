package de.tarent.invio.spring.service;


import de.tarent.invio.spring.AbstractTest;
import de.tarent.sellfio.web.dao.DeviceDAO;
import de.tarent.sellfio.web.dao.DeviceService;
import de.tarent.sellfio.web.model.Device;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.concurrent.ConcurrentMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class DeviceServiceTest extends AbstractTest {
    
    @Autowired
    DeviceService deviceService;

    @Autowired
    DeviceDAO deviceDAO;

    DeviceDAO deviceDAOMock = mock(DeviceDAO.class);
    ConcurrentMap<Integer, String> idToName;
    ConcurrentMap<String, Integer> nameToId;

    @Before
    public void setup() {
        idToName = (ConcurrentMap<Integer, String>)ReflectionTestUtils.getField(deviceService, "idToName");
        nameToId = (ConcurrentMap<String, Integer>)ReflectionTestUtils.getField(deviceService, "nameToId");

        idToName.clear();
        nameToId.clear();
    }


    @After
    public void cleanUp() {
        ReflectionTestUtils.setField(deviceService, "deviceDAO", deviceDAO);
    }

    @Test
    public void testGetIdCacheOnly() {
        ReflectionTestUtils.setField(deviceService, "deviceDAO", deviceDAOMock);

        nameToId.put("device-1", 1);
        nameToId.put("device-1", 2);
        nameToId.put("device-2", 3);

        assertNull(deviceService.getId("device-0"));
        assertEquals(2, deviceService.getId("device-1").intValue());
        assertEquals(3, deviceService.getId("device-2").intValue());

        verify(deviceDAOMock).getByName("device-0");
        verifyNoMoreInteractions(deviceDAOMock);
    }


    @Test
    public void testGetNameCacheOnly() {
        ReflectionTestUtils.setField(deviceService, "deviceDAO", deviceDAOMock);

        idToName.put(1, "device-0");
        idToName.put(1, "device-1");
        idToName.put(2, "device-2");

        assertNull(deviceService.getName(0));
        assertEquals("device-1", deviceService.getName(1));
        assertEquals("device-2", deviceService.getName(2));

        verify(deviceDAOMock).getById(0);
        verifyNoMoreInteractions(deviceDAOMock);
    }


    @Test
    public void testMapNameToIdCacheOnly() {
        ReflectionTestUtils.setField(deviceService, "deviceDAO", deviceDAOMock);

        nameToId.put("map-1", 1);

        assertEquals(1, deviceService.mapNameToId("map-1").intValue());

        verifyNoMoreInteractions(deviceDAOMock);
    }


    @Test
    public void testMapNameToId() {
        ReflectionTestUtils.setField(deviceService, "deviceDAO", deviceDAO);

        Integer id1a = deviceService.mapNameToId("device-1");
        Integer id2 = deviceService.mapNameToId("device-2");

        assertNotEquals(id1a, id2);

        // Clear cache, to see if it is refilled from the database:
        nameToId.clear();

        Integer id1b = deviceService.mapNameToId("device-1");

        System.out.println("id1a = " + id1a + ", id1b = " + id1b + ", id2 = " + id2);
        // Did it reload the ID?
        assertEquals(id1a, id1b);
        // ...and put it in both caches again?
        assertEquals(id1a, nameToId.get("device-1"));
        assertEquals("device-1", idToName.get(id1a));
    }


    @Test
    public void testGetDeviceFromDB() {

        Device d = new Device("myTestDevice");

        deviceDAO.store(d);

        final Integer id = deviceService.mapNameToId(d.getName());

        assertNotNull(id);
        assertEquals(d.getId(), (int)id);
    }

}
