package de.tarent.invio.spring.heatmap;

import de.tarent.invio.spring.AbstractTest;
import de.tarent.sellfio.web.dao.AggregatedPointDAO;
import de.tarent.sellfio.web.dao.DeviceService;
import de.tarent.sellfio.web.dao.MapDAO;
import de.tarent.sellfio.web.dao.MapService;
import de.tarent.sellfio.web.dao.PointDAO;
import de.tarent.sellfio.web.model.AggregatedPoint;
import de.tarent.sellfio.web.model.Map;
import de.tarent.sellfio.web.model.Point;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by mley on 15.09.15.
 */
public class HeatMapAggregatorTest extends AbstractTest {

    private static final String MAP = "map-aggregatortest";
    private static final String DEVICE1 = "device1";
    private static final String DEVICE2 = "device2";
    private int mapId;
    private int deviceId1;
    private int deviceId2;

    private HeatMapAggregator hma;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private MapService mapService;

    @Autowired
    private MapDAO mapDao;

    private AggregatedPointDAO aggregatedPointDao;
    private PointDAO pointDao;

    private Map map;
    private Map brokenMap;

    @Before
    public void setup() {

        aggregatedPointDao = mock(AggregatedPointDAO.class);
        pointDao = mock(PointDAO.class);
        hma = new HeatMapAggregator(mapDao, pointDao, aggregatedPointDao, deviceService, mapService);

        brokenMap = new Map();
        brokenMap.setScale(-1);
        brokenMap.setMinLat(-1);
        brokenMap.setMinLon(-1);
        brokenMap.setMaxLat(-1);
        brokenMap.setMaxLon(-1);
        brokenMap.setOriginLat(-1);
        brokenMap.setOriginLon(-1);
        brokenMap.setName("borked");
        mapDao.store(brokenMap);

        map = new Map();
        map.setScale(2000.0);
        map.setMinLat(0.0);
        map.setMinLon(0.0);
        map.setMaxLat(2.0);
        map.setMaxLon(2.0);
        map.setName(MAP);
        mapDao.store(map);

        mapId = mapService.mapNameToId(MAP);

        deviceId1 = deviceService.mapNameToId(DEVICE1);
        deviceId2 = deviceService.mapNameToId(DEVICE2);
    }

    @Test
    public void test() throws InterruptedException {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                // verify heatmap to be saved
                AggregatedPoint[][] heatmap = ((List<AggregatedPoint[][]>) invocation.getArguments()[0]).get(0);
                AggregatedPoint p = heatmap[111][111];
                assertNotNull(p);
                assertEquals(2, p.getNumberOfUniqueVisits());
                assertEquals(4, p.getVisits());

                return null;
            }
        }).when(aggregatedPointDao).saveHeatMaps(anyList());

        hma.aggregateHeat(new Point(System.currentTimeMillis(), mapId, deviceId1, new double[]{1.0, 1.0}), MAP, DEVICE1);
        hma.aggregateHeat(new Point(System.currentTimeMillis(), mapId, deviceId1, new double[]{1.0, 1.0}), MAP, DEVICE1);
        hma.aggregateHeat(new Point(System.currentTimeMillis(), mapId, deviceId2, new double[]{1.0, 1.0}), MAP, DEVICE2);
        hma.aggregateHeat(new Point(System.currentTimeMillis(), mapId, deviceId2, new double[]{1.0, 1.0}), MAP, DEVICE2);

        // points shall only be saved when storePointsAndHeatMap is called, not at every time a point is added
        verify(pointDao, times(0)).store(any(Point.class));

        try {
            hma.storePointsAndHeatMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
        verify(pointDao, times(4)).store(any(Point.class));
    }

    @Test
    public void testNoMapData() {


        hma.aggregateHeat(new Point(System.currentTimeMillis(), brokenMap.getId(), deviceId1, new double[]{1.0, 1.0}), brokenMap.getName(), DEVICE1);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                // there should be no heatmaps to be saved.
                List<AggregatedPoint[][]> heatmaps = (List<AggregatedPoint[][]>) invocation.getArguments()[0];
                assertEquals(0, heatmaps.size());
                return null;
            }
        }).when(aggregatedPointDao).saveHeatMaps(anyList());

        hma.storePointsAndHeatMap();

        verify(pointDao, times(1)).store(any(Point.class));

    }


}
