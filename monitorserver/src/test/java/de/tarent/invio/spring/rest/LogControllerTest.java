package de.tarent.invio.spring.rest;


import de.tarent.invio.spring.AbstractTest;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.junit.Assert.assertEquals;

public class LogControllerTest extends AbstractTest {

    @Value("${maps.root}")
    private String mapsRootPath;

    @Autowired
    LogController logController;

    HttpEntity<byte[]> httpEntityZipOk;

    String fileA;


    @Before
    public void setup() throws IOException {

        new File(mapsRootPath).mkdirs();

        // Build a valid zip archive with two files. The first file has a timestamp in the name so that we can
        // find and validate it later.

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);

        fileA = "file-A-" + System.currentTimeMillis();
        ZipEntry ze1 = new ZipEntry(fileA);
        zos.putNextEntry(ze1);
        zos.write('A');
        zos.closeEntry();

        ZipEntry ze2 = new ZipEntry("file-B");
        zos.putNextEntry(ze2);
        zos.write('B');
        zos.closeEntry();

        zos.close();
        byte[] body = baos.toByteArray();
        httpEntityZipOk = new HttpEntity<>(body, new HttpHeaders());
    }

    @After
    public void cleanUp() {
        try {
            FileUtils.deleteDirectory(new File(mapsRootPath));
        } catch(IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testZipUploadOk() throws IOException, InterruptedException {
        logController.storeLogs("testMap", httpEntityZipOk);
        String contents = fileGetContents(fileA);
        assertEquals("A", contents);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testEmptyZipUpload() throws IOException, InterruptedException {
        byte[] body = null;
        HttpEntity<byte[]> entity = new HttpEntity<>(body, new HttpHeaders());
        logController.storeLogs("testMap", entity);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testZipUploadWithIllegalMapNameSpace() throws IOException, InterruptedException {
        logController.storeLogs("test Map", httpEntityZipOk);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZipUploadWithIllegalMapNameEmpty() throws IOException, InterruptedException {
        logController.storeLogs("", httpEntityZipOk);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZipUploadWithIllegalMapNameDot() throws IOException, InterruptedException {
        logController.storeLogs(".testMap", httpEntityZipOk);
    }

    public void testZipUploadWithIllegalMapNameSlash() throws IOException, InterruptedException {
        logController.storeLogs("test/Map", httpEntityZipOk);
    }


    // Load the contents of a file that was extracted from our uploaded zip archive.
    // We let the shell find it because the directory name changes with time and the shell does better globbing.
    private String fileGetContents(String filename) throws IOException, InterruptedException {
        String script = "cat /tmp/maps/testMap/logs/*/" + filename;
        PrintWriter out = new PrintWriter("/tmp/catFile.sh");
        out.println(script);
        out.close();

        final ProcessBuilder pb = new ProcessBuilder("bash", "--", "/tmp/catFile.sh");
        final Process process = pb.start();
        BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = in.readLine();
        process.waitFor();
        in.close();

        return line;
    }

}
