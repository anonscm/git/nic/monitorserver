package de.tarent.invio.spring.service;


import de.tarent.invio.spring.AbstractTest;
import de.tarent.sellfio.web.dao.MapDAO;
import de.tarent.sellfio.web.dao.MapService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verify;

public class MapServiceTest extends AbstractTest {

    @Autowired
    MapService mapService;

    @Autowired
    MapDAO mapDAO;

    MapDAO mapDAOMock = mock(MapDAO.class);


    @Before
    public void setup() {
        mapService.getNameToId().clear();
        mapService.getIdToName().clear();
    }


    @Test
    public void testGetIdCacheOnly() {
        ReflectionTestUtils.setField(mapService, "mapDAO", mapDAOMock);

        mapService.getNameToId().put("map-1", 1);
        mapService.getNameToId().put("map-1", 2);
        mapService.getNameToId().put("map-2", 3);

        assertNull(mapService.getId("map-0"));
        assertEquals(2, mapService.getId("map-1").intValue());
        assertEquals(3, mapService.getId("map-2").intValue());

        verify(mapDAOMock).getByName("map-0");
        verifyNoMoreInteractions(mapDAOMock);
    }


    @Test
    public void testGetNameCacheOnly() {
        ReflectionTestUtils.setField(mapService, "mapDAO", mapDAOMock);

        mapService.getIdToName().put(1, "map-0");
        mapService.getIdToName().put(1, "map-1");
        mapService.getIdToName().put(2, "map-2");

        assertNull(mapService.getName(0));
        assertEquals("map-1", mapService.getName(1));
        assertEquals("map-2", mapService.getName(2));

        verify(mapDAOMock).getById(0);
        verifyNoMoreInteractions(mapDAOMock);
    }


    @Test
    public void testMapNameToIdCacheOnly() {
        ReflectionTestUtils.setField(mapService, "mapDAO", mapDAOMock);

        mapService.getNameToId().put("map-1", 1);

        assertEquals(1, mapService.mapNameToId("map-1").intValue());

        verifyNoMoreInteractions(mapDAOMock);
    }


    @Test
    public void testMapNameToId() {
        ReflectionTestUtils.setField(mapService, "mapDAO", mapDAO);

        Integer id1a = mapService.mapNameToId("map-1");
        Integer id2 = mapService.mapNameToId("map-2");

        assertNotEquals(id1a, id2);

        // Clear cache, to see if it is refilled from the database:
        mapService.getNameToId().clear();

        Integer id1b = mapService.mapNameToId("map-1");

        // Did it reload the ID?
        assertEquals(id1a, id1b);
        // ...and put it in both caches again?
        assertEquals(id1a, mapService.getNameToId().get("map-1"));
        assertEquals("map-1", mapService.getIdToName().get(id1a));
    }

}
