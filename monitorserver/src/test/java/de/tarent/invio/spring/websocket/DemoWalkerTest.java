package de.tarent.invio.spring.websocket;

import de.tarent.invio.spring.AbstractTest;
import de.tarent.sellfio.web.dao.DemoRouteDao;
import de.tarent.sellfio.web.model.RoutePoint;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by mley on 03.03.15.
 */
public class DemoWalkerTest extends AbstractTest {

    @Autowired
    DemoRouteDao drd;

    @Autowired
    DemoWalker dw;

    @Before
    public void before() throws InterruptedException {

        // testing checks in DemoRouteDao
        drd.saveRoutes(null);
        drd.saveRoutes(new RoutePoint[0][0]);
        drd.saveRoutes(new RoutePoint[][]{{}});

        RoutePoint[][] routes = new RoutePoint[][]{{
                new RoutePoint(0, 1, 1, "map", "name", "dev", 1.0, 1.0, 3, 0),
                new RoutePoint(0, 1, 2, "map", "name", "dev", 1.1, 1.0, 3, 1),
                new RoutePoint(0, 1, 3, "map", "name", "dev", 1.2, 1.0, 2, 0),
                new RoutePoint(0, 1, 4, "map", "name", "dev", 1.3, 1.0, 1, 0),
        }, {
                new RoutePoint(0, 2, 1, "map", "name2", "dev2", 1.0, 1.0, 3, 0),
                new RoutePoint(0, 2, 2, "map", "name2", "dev2", 1.0, 1.0, 3, 1),
                new RoutePoint(0, 2, 3, "map", "name2", "dev2", 1.0, 1.0, 3, 0),
                new RoutePoint(0, 2, 4, "map", "name2", "dev2", 1.0, 1.0, 3, 1),
                new RoutePoint(0, 2, 5, "map", "name2", "dev2", 1.0, 1.0, 3, 0),
                new RoutePoint(0, 2, 6, "map", "name2", "dev2", 1.1, 1.0, 2, 0),
                new RoutePoint(0, 2, 7, "map", "name2", "dev2", 1.2, 1.0, 1, 0),
                new RoutePoint(0, 2, 8, "map", "name2", "dev2", 1.3, 1.0, 1, 0),
        }};
        drd.saveRoutes(routes);

        UpdateService usMock = mock(UpdateService.class);

        dw.setSleepInterval(10);

        // mock productserver
        RestTemplate rtMock = mock(RestTemplate.class);
        Map<String, Object> product = new HashMap<>();
        product.put("description", "product");
        product.put("price", 100);
        when(rtMock.getForObject(anyString(), eq(Map.class))).thenReturn(product);
        dw.setRestTemplate(rtMock);


        // mock random
        Random r = mock(Random.class);
        when(r.nextDouble()).thenReturn(0.1);

        dw.stop();

        Thread.sleep(1000);

        dw.start(usMock);
    }

    @After
    public void after() {
        dw.stop();
    }

    @Test
    public void testDemoWalker() throws InterruptedException {
        // not a real test though, just let it run a few seconds
        Thread.sleep(6000);


    }


}
