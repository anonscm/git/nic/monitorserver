package de.tarent.invio.spring.rest;

import de.tarent.invio.spring.AbstractTest;
import de.tarent.sellfio.web.rest.VersionController;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static junit.framework.Assert.assertTrue;

/**
 * Created by mley on 04.03.15.
 */
public class VersionControllerTest extends AbstractTest {

    @Autowired
    VersionController vc;

    @Test
    public void testVersion() {

        final Map<String, String> version = vc.getVersion();
        assertTrue(version.containsKey("version"));
        assertTrue(version.containsKey("date"));
    }
}
