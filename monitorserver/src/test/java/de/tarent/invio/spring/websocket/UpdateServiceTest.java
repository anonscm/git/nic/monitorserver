package de.tarent.invio.spring.websocket;

import de.tarent.sellfio.web.dao.CustomerDAO;
import de.tarent.invio.spring.heatmap.HeatMapAggregator;
import de.tarent.sellfio.web.model.CartItem;
import de.tarent.sellfio.web.model.Customer;
import de.tarent.invio.spring.model.MonitorMessage;
import de.tarent.sellfio.web.model.State;
import de.tarent.invio.spring.trust.TrustEngine;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by mley on 14.11.14.
 */
public class UpdateServiceTest {

    UpdateService updateService;

    MonitorHandler handlerMock;

    TrustEngine trustEngine;

    Customer c1, c2;

    @Before
    public void setup() {
        updateService = new UpdateService();
        handlerMock = mock(MonitorHandler.class);

        updateService.setCustomerDAO(mock(CustomerDAO.class));
        trustEngine = mock(TrustEngine.class);
        updateService.setTrustEngine(trustEngine);

        updateService.setMonitorHandler(handlerMock);
        updateService.setHeatMapAggregator(mock(HeatMapAggregator.class));

        updateService.setZombieCollector(mock(ZombieCollector.class));

        c1 = new Customer();
        c1.setMap("map");
        c1.setDeviceId("abc");
        c1.setState(State.SHOPPING);
        c1.getCartItems().add(new CartItem());
        c1.setTimestamp(1);
        c1.setLocation(new double[]{0, 0});

        c2 =new Customer();
        c2.setMap("map");
        c2.setDeviceId("abc");
        c2.setState(State.SHOPPING);
        c2.getCartItems().add(new CartItem());
        c2.setTimestamp(2);
        c2.setLocation(new double[]{0, 0});

    }

    @Test
    public void testUpdateService() throws IOException {
        long millis = System.currentTimeMillis();
        // update + heat
        updateService.updatePosition("map", "abc", millis, new double[]{1.0, 1.0}, "SHOPPING");

        // only heat, because timestamp is the same
        updateService.updatePosition("map", "abc", millis, new double[]{1.0, 1.0}, "SHOPPING");

        // update + heat
        updateService.updatePosition("map", "abc", millis + 3, new double[]{1.1, 1.0}, "SHOPPING");

        verify(handlerMock, times(6)).broadcast(eq("map"), any(MonitorMessage.class));
    }

    @Test
    public void testCleanup() throws IOException, InterruptedException {
        updateService.setWalker(mock(DemoWalker.class));

        ZombieCollector zc = new ZombieCollector();

        zc.setZombieCleanInterval(10);
        zc.setZombieTimeout(100);
        zc.setRemoveTimeout(300);
        updateService.setZombieCollector(zc);

        updateService.postConstruct();

        c1.setTimestamp(System.currentTimeMillis());
        updateService.updateCustomer(c1);

        Thread.sleep(200);

        assertEquals(1, updateService.getAll("map").size());
        assertEquals(State.ZOMBIE, c1.getState());

        Thread.sleep(200);

        assertTrue(updateService.getAll("map").isEmpty());

        updateService.preDestroy();
    }


    @Test
    public void testUpdateSameCart() throws IOException {
        // updateCustomer called twice with same data, no 2nd update shall be sent to Monitor application
        testUpdate(c1, c2, 1);
    }

    @Test
    public void testUpdateSameCart2() throws IOException {
        // updateCustomer called twice with same data, no 2nd update shall be sent to Monitor application
        testUpdate(c1, c1, 1);
    }

    @Test
    public void testUpdateDifferentLocation() throws IOException {
        c2.setTimestamp(2);
        c2.setLocation(new double[]{3,4});
        // two updates of cart and one for heatmap
        testUpdate(c1, c2, 3);
    }

    @Test
    public void testUpdateAmount() throws IOException {
        c2.getCartItems().get(0).setAmount(2);
        c2.setTimestamp(2);
        // updateCustomer called with different data, update shall be sent to Monitor application
        testUpdate(c1, c2, 2);
    }

    @Test
    public void testUpdateState() throws IOException {
        c2.setTimestamp(2);
        c2.setState(State.CHECKOUT);
        // updateCustomer called with different data, update shall be sent to Monitor application
        testUpdate(c1, c2, 2);
        verify(trustEngine, times(1)).calculateTrust(eq(c2));
    }

    @Test
    public void testUpdateState2() throws IOException {
        updateService.updateCustomer(c1);
        updateService.updatePosition(c1.getMap(), c1.getDeviceId(), 2l, new double[]{1.1, 2.2}, State.CHECKOUT);
        verify(trustEngine, times(2)).calculateTrust(eq(c1));
    }

    @Test
    public void testUpdateMoreItems() throws IOException {
        c2.setTimestamp(2);
        c2.getCartItems().add(new CartItem());
        // updateCustomer called with different data, update shall be sent to Monitor application
        testUpdate(c1, c2, 2);
    }

    private void testUpdate(Customer c1, Customer c2, int numOfMessages) throws IOException {
        updateService.updateCustomer(c1);
        updateService.updateCustomer(c2);
        verify(handlerMock, times(numOfMessages)).broadcast(eq("map"), any(MonitorMessage.class));
    }

    @Test
    public void testRemove() throws IOException {
        updateService.updateCustomer(c1);
        updateService.removeCustomer(c1.getMap(), c1.getDeviceId());

        verify(handlerMock, times(1)).broadcast(eq("map"), eq( new MonitorMessage("REMOVE", c1)));
    }

    @Test
    public void testRemoveUnknown() throws IOException {
        updateService.updateCustomer(c1);
        updateService.removeCustomer(c1.getMap(), "unknownDeviceId");
    }

    @Test
    public void testRemoveUnknown2() throws IOException {
        updateService.removeCustomer("unknownMap", "unknownDeviceId");
    }
}
