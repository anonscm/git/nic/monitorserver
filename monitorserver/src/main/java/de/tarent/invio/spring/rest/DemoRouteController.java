package de.tarent.invio.spring.rest;

import de.tarent.sellfio.web.dao.DemoRouteDao;
import de.tarent.sellfio.web.model.RoutePoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.List;

/**
 * Created by mley on 17.02.15.
 */
@RestController
@RequestMapping("/demo")
public class DemoRouteController {

    @Autowired
    DemoRouteDao dao;

    /**
     * Save demo routes.
     * The map, to which these routes belong, is specified in the map of the first RoutePoint.
     *
     * @param routes routes
     */
    @RolesAllowed("")
    @RequestMapping(method = RequestMethod.POST)
    public void saveRoute(@RequestBody RoutePoint[][] routes) {
        dao.saveRoutes(routes);
    }

    /**
     * Get routes for map.
     *
     * @param map map name
     * @return list of routes
     */
    @RolesAllowed("")
    @RequestMapping(value = "/{map}", method = RequestMethod.GET)
    public List<List<RoutePoint>> getRoute(@PathVariable("map") String map) {
        return dao.getRoutes(map);
    }
}
