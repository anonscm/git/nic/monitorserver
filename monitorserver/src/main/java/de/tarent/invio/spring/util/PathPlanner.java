package de.tarent.invio.spring.util;

import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.tracker.Tracker;
import de.tarent.sellfio.web.util.HaversineDistance;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mley on 09.04.15.
 */
@Component
public class PathPlanner {

    private static final Logger LOG = Logger.getLogger(PathPlanner.class);

    @Value("${map.baseurl}")
    private String mapBaseUrl;

    @Value("${map.usetracker}")
    private boolean useTracker = false;

    private final Map<String, Tracker> trackers = new HashMap<>();

    /**
     * Plan the shortest path from start to end point using the occupancy grid map
     *
     * @param mapName name of the map
     * @param start   start point
     * @param end     end point
     * @return array of points with the planned path
     */
    public double[][] planPath(String mapName, double[] start, double[] end) {


        final Tracker tracker = getTracker(mapName);

        if (tracker == null) {
            LOG.error("Tracker could not be created. Falling back to line of sight distance");
            return new double[][]{start, end};
        }

        final InvioGeoPoint fromPoint = new InvioGeoPoint(start[0], start[1]);
        final InvioGeoPoint toPoint = new InvioGeoPoint(end[0], end[1]);

        final InvioGeoPoint[] route = tracker.planPath(fromPoint, toPoint);

        final double[][] routePoints = new double[route.length + 2][2];
        routePoints[0] = start;
        for (int i = 0; i < route.length; i++) {
            routePoints[i + 1][0] = route[i].getLatitudeE6() / 1E6;
            routePoints[i + 1][1] = route[i].getLongitudeE6() / 1E6;
        }
        routePoints[routePoints.length - 1] = end;

        return routePoints;
    }

    /**
     * Calculate the length of the optimal route between start and endpoint
     *
     * @param mapName name of map
     * @param start   start point
     * @param end     end point
     * @return distance of optimal path in meters
     */
    public double calcOptimalDistance(String mapName, double[] start, double[] end) {
        final double[][] routePoints = planPath(mapName, start, end);
        return HaversineDistance.calcRouteDistance(routePoints);
    }

    /**
     * Checks if we already have tracker for this map, if not a new tracker will be created.
     *
     * @param mapName map name
     * @return Tracker object or null if tracker could not be created.
     */
    private synchronized Tracker getTracker(final String mapName) {
        if (!useTracker) {
            return null;
        }

        Tracker tracker = null;

        if (trackers.containsKey(mapName)) {
            tracker = trackers.get(mapName);
        } else {
            try {
                tracker = createTracker(mapName);
            } catch (NoClassDefFoundError | UnsatisfiedLinkError e) {
                LOG.error("Error loading native tracker library", e);
                return null;
            } catch (IOException | ParserConfigurationException | SAXException e) {
                LOG.error("Error configuring tracker", e);
                return null;
            }
            trackers.put(mapName, tracker);
        }

        return tracker;

    }

    private Tracker createTracker(final String mapName) throws IOException, ParserConfigurationException, SAXException {


        final URL ogm = new URL(mapBaseUrl + "/maps/" + mapName + "/data/ogm_" + mapName + ".png");
        final InputStream ogmIs = ogm.openStream();
        final URL tileMapResource = new URL(mapBaseUrl + "/maps/" + mapName + "/tiles/tilemapresource.xml");
        final InputStream tileMapResourceIs = tileMapResource.openStream();

        Tracker tracker = null;

        if (ogmIs.available() > 0 && tileMapResourceIs.available() > 0) {
            tracker = new Tracker();
            setOccupancyGridMap(tracker, ogmIs);
            setBoundingBox(tracker, tileMapResourceIs);
        }


        return tracker;
    }

    /**
     * Loads the OGM from the stream, converts it and gives it to the tracker.
     *
     * @param tracker   the tracker
     * @param ogmStream stream of the occupancy grid map.
     * @throws IOException if loading the stream fails.
     */
    public static void setOccupancyGridMap(final Tracker tracker, final InputStream ogmStream) throws IOException {
        final BufferedImage img = ImageIO.read(ogmStream);

        final float[] greyscale = new float[img.getHeight() * img.getWidth()];
        int[] pixels = new int[greyscale.length];

        pixels = img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());

        for (int i = 0; i < pixels.length; i++) {

            final int red = (pixels[i] >> 16) & 0xFF;
            final int green = (pixels[i] >> 8) & 0xFF;
            final int blue = pixels[i] & 0xFF;

            greyscale[i] = (red + green + blue) / 765.0f;
        }

        tracker.setOccupancyGridMap(greyscale, img.getWidth(), img.getHeight());
    }


    /**
     * Load the tilemapresources from the stream and sets its bounding box to the tracker.
     *
     * @param tracker tracker object.
     * @param stream  stream of tilemapresource.xml
     * @throws ParserConfigurationException if parsing the XML fails
     * @throws IOException                  if loading the stream fails.
     * @throws SAXException                 if XML fails ;-).
     */
    public static void setBoundingBox(final Tracker tracker, final InputStream stream)
            throws ParserConfigurationException, IOException, SAXException {
        final DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        final Document doc = builder.parse(stream);
        final Element element = doc.getDocumentElement();
        final Node bb = element.getElementsByTagName("BoundingBox").item(0);


        tracker.setBoundingBox(new InvioGeoPoint(getAttrAsDouble(bb, "minx"), getAttrAsDouble(bb, "miny")),
                new InvioGeoPoint(getAttrAsDouble(bb, "maxx"), getAttrAsDouble(bb, "maxy")));
    }


    private static double getAttrAsDouble(final Node node, final String attr) {
        return Double.parseDouble(node.getAttributes().getNamedItem(attr).getNodeValue());
    }

}
