package de.tarent.invio.spring.model;

import lombok.Data;

/**
 * Websocket subscription.
 */
@Data
public class Subscription {

    /** map subscribed to */
    private String map;

    /** flag if live heat info shall be sent to the socket */
    private boolean heat;
}
