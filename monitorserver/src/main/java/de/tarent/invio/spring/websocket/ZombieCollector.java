package de.tarent.invio.spring.websocket;

import de.tarent.sellfio.web.model.Customer;
import de.tarent.sellfio.web.model.State;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mley on 24.02.15.
 */
@Component
public class ZombieCollector implements Runnable {

    private static final Logger LOG = Logger.getLogger(ZombieCollector.class);

    @Value("${monitor.collectZombies}")
    private volatile boolean collectZombies = true;

    @Setter
    @Value("${monitor.zombieCleanInterval}")
    private long zombieCleanInterval = 3000;

    @Setter
    @Value("${monitor.zombieTimeout}")
    private long zombieTimeout = 10000;

    @Setter
    @Value("${monitor.removeTimeout}")
    private long removeTimeout = 20000;

    private Thread collectorThread;

    private UpdateService updateService;

    private final Map<String, String> oldStates = Collections.synchronizedMap(new HashMap<String, String>());


    /**
     * Start the zombie collector thread
     *
     * @param updateService UpdateService object
     */
    public void start(UpdateService updateService) {
        this.updateService = updateService;

        collectorThread = new Thread(this);
        collectorThread.start();
    }

    /**
     * Stop the zombie collector thread. Returns after thread has stopped.
     */
    public void stop() {
        collectZombies = false;
        LOG.info("Waiting for ZombieCollector thread to stop...");
        synchronized (collectorThread) {
            collectorThread.interrupt();
        }
        try {
            collectorThread.join(zombieCleanInterval);
        } catch (InterruptedException e) { //NOSONAR can be ignored
        }
        if (collectorThread.isAlive()) {
            LOG.error("Failed to stop ZombieCollector thread!");
        }
    }

    /**
     * Remove a deviceId of the oldStates map
     *
     * @param deviceId device id
     */
    public void free(String deviceId) {
        oldStates.remove(deviceId);
    }

    @Override
    public void run() {
        try {
            while (collectZombies) {
                cleanZombies();
                try {
                    Thread.sleep(zombieCleanInterval);
                } catch (InterruptedException e) { //NOSONAR can be ignored
                    LOG.error("ZombieCollector: interrupted");
                }
            }
            LOG.info("ZombieCollector: Thread stopped");
        } catch (RuntimeException e) { // NOSONAR gotta catch em all for logging.
            LOG.info("ZombieCollector: Runtime exception!", e);
        }
    }

    private void cleanZombies() {
        final long now = System.currentTimeMillis();

        final List<Customer> cartsToRemove = new ArrayList<>();
        for (Map<String, Customer> carts : updateService.getCurrentCustomers().values()) {
            for (Customer c : carts.values()) {
                final long lastUpdateBefore = now - c.getTimestamp();
                try {
                    if (lastUpdateBefore > removeTimeout) {
                        // customer has sent no updates for too long and will be removed.
                        cartsToRemove.add(c);
                        LOG.info("ZombieCollector: Removing customer " + c.getDeviceId() + " from list");
                    } else if (lastUpdateBefore > zombieTimeout && ! State.ZOMBIE.equals(c.getState())) {
                        // customer has sent no updates for some time. Will be displayed as zombie
                        oldStates.put(c.getDeviceId(), c.getState());
                        c.setState(State.ZOMBIE);
                        updateService.sendUpdate(c);
                        LOG.info("ZombieCollector: Customer " + c.getDeviceId() + " is now a zombie ");
                    }

                } catch (IOException e) {
                    LOG.error("ZombieCollector: Error cleaning zombie ", e);
                }
            }
        }
        removeCustomers(cartsToRemove);
    }

    private void removeCustomers(List<Customer> cartsToRemove) {
        for (Customer c : cartsToRemove) {
            try {
                updateService.remove(c);
            } catch (IOException e) {
                LOG.error("error removing zombie customer", e);
            }
        }
    }

}
