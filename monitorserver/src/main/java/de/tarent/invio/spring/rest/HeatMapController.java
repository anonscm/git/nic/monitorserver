package de.tarent.invio.spring.rest;

import de.tarent.sellfio.web.model.InvioGeoPoint;
import de.tarent.sellfio.web.dao.AggregatedPointDAO;
import de.tarent.sellfio.web.dao.MapDAO;
import de.tarent.sellfio.web.dao.MapService;
import de.tarent.invio.spring.heatmap.AggregatedHeatMap;
import de.tarent.invio.spring.heatmap.UniqueVisitorsHeatMap;
import de.tarent.sellfio.web.model.AggregatedPoint;
import de.tarent.sellfio.web.model.HeatPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;
import java.util.Map;

/**
 * Created by mley on 22.11.14.
 */
@RestController
@RequestMapping("/heat")
public class HeatMapController {

    @Autowired
    private AggregatedPointDAO aggregatedPointDAO;

    @Autowired
    private MapService mapService;

    @Autowired
    private MapDAO mapDAO;

     /**
     * Calculates a heat map for unique customers for a given time frame.
     *
     * @param mapName    name of map
     * @param start      start timestamp (millis from 1.1.1970 0:00 UTC)
     * @param end        end timestamp (millis from 1.1.1970 0:00 UTC)
     * @param downSample factor by which original heatmap is sampled down
     * @param unique     flag if heatmap shall contain only unique visits or all visits
     * @return 2D array of HeatPoint objects containing the position and the count distinct customers
     * @throws IOException when deserializing boundsJson fails
     */
    @RolesAllowed("")
    @RequestMapping(value = "/map", method = RequestMethod.GET)
    public HeatPoint[][] getHeatMap(@RequestParam("map") String mapName, @RequestParam("start") long start,
                                    @RequestParam("end") long end,
                                    @RequestParam("downSample") int downSample, @RequestParam("unique") boolean unique)
            throws IOException {

        if (!paramsValid(start, end)) {
            return new HeatPoint[0][0];
        }

        final int mapId = mapService.mapNameToId(mapName);
        final de.tarent.sellfio.web.model.Map map = mapDAO.getById(mapId);
        final AggregatedHeatMap heatMap = new AggregatedHeatMap(map, 0);
        final Map<InvioGeoPoint, AggregatedPoint> points = aggregatedPointDAO.get(mapId, start, end);
        final UniqueVisitorsHeatMap uvhm = new UniqueVisitorsHeatMap();

        return uvhm.calculateAggregatedHeatPoints(points, heatMap, downSample, unique);

    }

    private boolean paramsValid(long start, long end) {
        if (start < 0 || end < 0) {
            return false;
        }

        if (start == 0 && end == 0) {
            return false;
        }

        if (start > end) {
            return false;
        }

        return true;
    }
}
