package de.tarent.invio.spring.rest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * The LogController is used to upload zip archives with (sensor-, position-, etc.) log files from the apps.
 *
 * Needs the "unzip" utility.
 */
@RestController
@RequestMapping("/logs")
public class LogController {

    private static final Logger LOG = Logger.getLogger(LogController.class);

    // The name of the directory where the zip files are stored (under the individual map directories):
    private static final String ZIP_LOG_DIR = "logs";

    private static final String UNZIP = "/usr/bin/unzip";


    @Value("${maps.root}")
    private String mapsRootPath;


    /**
     * Store a zip archive with sensor data from the admin app. If the map directory doesn't exist it will be created.
     *
     * Example request to this method:
     *   curl -v -X POST --data-binary @foo.zip --header "Content-Type: application/octet-stream" \
     *   http://localhost:8080/monitor/logs/mapname
     *
     * @param mapName the name of the map to which these logs belong
     * @param requestEntity the HttpEntity which contains the uploaded file.
     * @throws IOException when the uploaded file could not be saved
     * @throws InterruptedException when the unzipping was terminated somehow
     */
    @RequestMapping(path = "/{mapName}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void storeLogs(@PathVariable String mapName, HttpEntity<byte[]> requestEntity)
            throws IOException, InterruptedException {

        validateMapName(mapName);

        final byte[] body = requestEntity.getBody();
        if (body == null) {
            throw new IllegalArgumentException("Zip file appears to be empty.");
        }

        // Create destination directory:
        final String dateString = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
        final File dir = new File(mapsRootPath + "/" + mapName + "/" + ZIP_LOG_DIR + "/", dateString);
        if (dir.mkdirs()) {
            LOG.info("Created: " + dir.getAbsolutePath());
        } else {
            LOG.error("Could not create directory " + dir.getAbsolutePath());
        }

        // Save zip:
        final Path zipFile = Paths.get(dir.getAbsolutePath(), "logs.zip");
        LOG.info("storeLogs: body-size: " + body.length + ", destination file: " + zipFile);
        Files.write(zipFile, body);

        // Unzip:
        final ProcessBuilder pb = new ProcessBuilder(UNZIP, "-j", "-d", dir.getAbsolutePath(), zipFile.toString());
        pb.directory(new File(dir.getAbsolutePath()));
        final Process process = pb.start();
        final int returnCode = process.waitFor();
        if (returnCode != 0) {
            LOG.error("Unzipping of " + zipFile + " had return code " + returnCode);
        }
    }


    /**
     * We blame the user for IllegalArgumentException (=> 400) because they come from the validation of the request.
     * A 404 would not really be appropriate because we will create maps on the fly, as long as the name is valid.
     *
     * @param e thrown exception
     * @return Response
     */
    @ExceptionHandler({IllegalArgumentException.class})
    @ResponseBody
    public ResponseEntity<String> handleValidationException(Exception e) {
        LOG.warn("Invalid request URI => 400: " + e.getMessage());
        return new ResponseEntity<>(e.getMessage() + "\n", HttpStatus.BAD_REQUEST);
    }


    /**
     * InterruptedException indicates that something went wrong while unzipping.
     *
     * @param e thrown exception
     * @return Response
     */
    @ExceptionHandler({InterruptedException.class})
    @ResponseBody
    public ResponseEntity<String> handleInterruptedException(Exception e) {
        LOG.error("InterruptedException => 500: " + e.getMessage());
        return new ResponseEntity<>(e.getMessage() + "\n", HttpStatus.INTERNAL_SERVER_ERROR);
    }


    // Validate a client-supplied mapName and throw IllegalArgumentException if we don't like it.
    //
    // @ToldYouSo("Someday there will be problems because of strange characters that we intentionally did not filter.")
    private void validateMapName(String mapName) {
        if ((mapName == null) || mapName.isEmpty()) {
            throw new IllegalArgumentException("mapName may not be empty.");
        }

        if (mapName.indexOf('/') != -1) {
            throw new IllegalArgumentException("mapName may not contain slashes.");
        }

        if (mapName.indexOf('.') == 0) {
            throw new IllegalArgumentException("mapName may not start with a dot.");
        }

        if (mapName.indexOf(' ') != -1) {
            throw new IllegalArgumentException("mapName may not contain spaces.");
        }
    }

}
