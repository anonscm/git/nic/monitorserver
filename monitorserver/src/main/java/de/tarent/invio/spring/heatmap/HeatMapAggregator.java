package de.tarent.invio.spring.heatmap;

import de.tarent.sellfio.web.dao.AggregatedPointDAO;
import de.tarent.sellfio.web.dao.DeviceService;
import de.tarent.sellfio.web.dao.MapDAO;
import de.tarent.sellfio.web.dao.MapService;
import de.tarent.sellfio.web.dao.PointDAO;
import de.tarent.sellfio.web.model.AggregatedPoint;
import de.tarent.sellfio.web.model.Map;
import de.tarent.sellfio.web.model.Point;
import de.tarent.sellfio.web.util.MapServerUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * HeatMapAggregator. Stores heatpoints in memory and flushes them at times. Also aggregates heatpoints and saves
 * aggregated information in the DB.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
@EnableAsync
@EnableScheduling
@Transactional
public class HeatMapAggregator {

    private static final AggregatedHeatMap INVALID_MAP = new AggregatedHeatMap(new Map(), 0);

    private static final Logger LOG = Logger.getLogger(HeatMapAggregator.class);

    @Autowired
    private MapDAO mapDAO;

    @Autowired
    private PointDAO pointDAO;

    @Autowired
    private AggregatedPointDAO aggregatedPointDAO;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private MapService mapService;

    @Autowired
    private MapServerUtils mapServerUtils;

    /**
     * Map that store the heatmaps. The id of the map is the key.
     */
    private ConcurrentMap<Integer, AggregatedHeatMap> heatMaps = new ConcurrentHashMap<>();

    /**
     * List that stores the Points until they are saved to the DB.
     */
    private ConcurrentLinkedDeque<Point> pointList = new ConcurrentLinkedDeque<>();

    private AtomicLong timeStamp = new AtomicLong(System.currentTimeMillis());

    /**
     * Default constructor required for spring
     */
    public HeatMapAggregator() {
    }

    /**
     * Custom constructor, since no autowired instance is used by the migration.
     *
     * @param md  MapDAO
     * @param pd  PointDOA
     * @param apd AggregatedPointDAO
     * @param ds  DeviceService
     * @param ms  MapService
     */
    public HeatMapAggregator(MapDAO md, PointDAO pd, AggregatedPointDAO apd, DeviceService ds, MapService ms) {
        this.mapDAO = md;
        this.pointDAO = pd;
        this.aggregatedPointDAO = apd;
        this.deviceService = ds;
        this.mapService = ms;
    }

    /**
     * Save points and aggregated heatmaps to DB. Scheduled to be called every minute.
     */
    @Scheduled(cron = "0 * * * * *")
    public void storePointsAndHeatMap() {
        storePointsAndHeatMap(System.currentTimeMillis(), true);
    }

    /**
     * Save points and aggregated heatmaps to DB. Scheduled to be called every minute.
     *
     * @param newTimestamp new timestamp. Required for migration. This will be the start timestamp of the aggregated
     *                     heatpoint for the 60s timespan it covers.
     * @param storePoints  flag if the original, precise Point objects shall be stored as well.
     */
    public void storePointsAndHeatMap(final long newTimestamp, final boolean storePoints) {
        timeStamp.set(newTimestamp);
        final List<AggregatedPoint[][]> heatMapList = new ArrayList<>(heatMaps.size());

        for (AggregatedHeatMap ahm : heatMaps.values()) {
            if (ahm != INVALID_MAP) { //NOSONAR we explicitly want to compare object identity
                heatMapList.add(ahm.recreateHeatMap(newTimestamp));
            }
        }
        aggregatedPointDAO.saveHeatMaps(heatMapList);

        while (storePoints && !pointList.isEmpty()) {
            // since this class is also @Transactional, no new Transactions are created when calling DAO method
            // multiple times.
            pointDAO.store(pointList.removeFirst());
        }

    }

    /**
     * Aggregates heat points.
     *
     * @param point      point object
     * @param mapName    name of the map
     * @param deviceName device name
     */
    public void aggregateHeat(final Point point, final String mapName, final String deviceName) {

        final Integer device = deviceService.mapNameToId(deviceName);
        final Integer mapId = mapService.mapNameToId(mapName);

        point.setDevice(device);
        point.setMap(mapId);

        aggregateHeat(point);
    }


    /**
     * Aggregates heat points where the point already has the correct map- and device-ID.
     *
     * @param point point object
     */
    public void aggregateHeat(final Point point) {

        final AggregatedHeatMap heatMap = getOrCreateHeatMap(point.getMap());
        if (heatMap != INVALID_MAP) { //NOSONAR we explicitly want to compare object identity
            heatMap.aggregatePoint(point);
        }

        pointList.add(point);
    }


    /**
     * Get the aggregated heatmap object for given map id. If no heatmap for a map exists in heatMaps, the map meta
     * data is queried from the DB. If the meta-data is missing it tries to download it from the mapserver and store
     * it in the DB.
     *
     * @param mapId the map id.
     * @return the AggregatedHeatMap objects. If none exists in heatMaps, it's created. If there is missing meta data
     * files (OSM file, tilemapresource), then INVALID_MAP ist returned.
     */
    private synchronized AggregatedHeatMap getOrCreateHeatMap(Integer mapId) {
        if (!heatMaps.containsKey(mapId)) {
            final Map map = getByIdAndStoreMetaDataIfNotPresent(mapId);

            // if map is null, the metadata is missing and we can't process this map
            if (map == null) {
                heatMaps.put(mapId, INVALID_MAP);
                return INVALID_MAP;
            }

            heatMaps.put(mapId, new AggregatedHeatMap(map, timeStamp.get()));
        }

        return heatMaps.get(mapId);
    }

    /**
     * Gets a Map from the DB by its id. It is checked if the map meta data is complete. If it's not complete it is
     * downloaded from the mapserver and and persisted in the DB. If that fails, null is returned.
     * @param id the map id
     * @return Map object with meta data or null if getting meta data fails.
     */
    public Map getByIdAndStoreMetaDataIfNotPresent(final Integer id) {
        final Map map = mapDAO.getById(id);

        if (map.getScale() == -1 && map.getMinLat() == -1 && map.getMinLon() == -1
                && map.getMaxLat() == -1 && map.getMaxLon() == -1) {
            final boolean success = fillMetaData(map);
            if(success) {
                mapDAO.update(map);
            } else {
                return null;
            }
        }

        return map;
    }

    /**
     * Load map metadata from mapserver and write them into the map object.
     * TODO: why not save it to the DB as well?
     *
     * @param map the map object.
     * @return true if map meta data was loaded successfully, else false.
     */
    public boolean fillMetaData(Map map) {

        try {
            map.setScale(mapServerUtils.getScale(map.getName()));

            final double[] originAndBoundingBox = mapServerUtils.getOriginAndBoundingBox(map.getName());
            map.setOriginLat(originAndBoundingBox[0]);
            map.setOriginLon(originAndBoundingBox[1]);
            map.setMinLat(originAndBoundingBox[2]);
            map.setMinLon(originAndBoundingBox[3]);
            map.setMaxLat(originAndBoundingBox[4]);
            map.setMaxLon(originAndBoundingBox[5]);
            return true;
        } catch (Exception e) { //NOSONAR gotta catch em all
            // we catch all exceptions here, since any thrown exception on a @Transactional method will cause the
            // transaction to rollback, even if the exception is caught.
            LOG.error("Error loading or parsing map data of map "+map.getName());
            return false;
        }

    }

}
