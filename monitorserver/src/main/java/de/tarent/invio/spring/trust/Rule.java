package de.tarent.invio.spring.trust;

import de.tarent.sellfio.web.model.Customer;

/**
 * An interface for our implemented trust rules.
 */
public interface Rule {

    /**
     * Gets the trust value calculated by this rule
     *
     * @param customer current shopping cart
     * @return trust value, between 0 and 1.
     */
    double getTrust(final Customer customer);

    /**
     * Method to clean customer data from rule
     * @param deviceId device id.
     */
    void removeCustomer(String deviceId);

    /**
     * Upate the customers position
     * @param deviceId device id
     * @param pos positions
     */
    void updateCustomerPosition(String deviceId, double[] pos);
}
