package de.tarent.invio.spring.rest;

import de.tarent.invio.spring.websocket.UpdateService;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by mley on 13.11.14.
 */
@RestController
@RequestMapping("/location")
public class LocationController {

    private static final Logger LOG = Logger.getLogger(LocationController.class);

    @Autowired
    @Setter // for testing
    private UpdateService updateService;

    /**
     * Update customers position
     *
     * @param map      map name
     * @param deviceId device id
     * @param lat      latitude
     * @param lng      longitude
     * @param status   status of customer
     * @throws IOException if updating position fails
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void updatePosition(@RequestParam String map, @RequestParam String deviceId, @RequestParam double lat,
                               @RequestParam double lng, @RequestParam String status) throws IOException {
        //check for invalid positions
        if (lat == 0.0f && lng == 0.0f) {
            return;
        }

        LOG.debug("Updating position " + deviceId + " " + lat + " " + lng);
        updateService.updatePosition(map, deviceId, System.currentTimeMillis(), new double[]{lat, lng}, status);
    }

}
