package de.tarent.invio.spring.rest;


import de.tarent.sellfio.web.model.Customer;
import de.tarent.invio.spring.websocket.UpdateService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Cart Controller.
 */
@RestController
@RequestMapping("/cart")
public class CustomerController {

    private static final Logger LOG = Logger.getLogger(CustomerController.class);

    @Autowired
    UpdateService updateService;

    /**
     * Update the shopping cart.
     *
     * @param customer the {@link Customer}
     * @throws java.io.IOException if processing update failed.
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void handleCustomerUpload(@RequestBody Customer customer) throws IOException {
        LOG.info("Received cart: " + customer);
        customer.setTimestamp(System.currentTimeMillis());

        // trust value is never accepted from REST API
        customer.setTrust(0);

        updateService.updateCustomer(customer);
    }
}
