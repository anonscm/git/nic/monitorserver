package de.tarent.invio.spring.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tarent.sellfio.web.dao.DemoRouteDao;
import de.tarent.sellfio.web.model.Customer;
import de.tarent.sellfio.web.model.CartItem;
import de.tarent.sellfio.web.model.DemoWalkerMap;
import de.tarent.sellfio.web.model.RoutePoint;
import de.tarent.sellfio.web.model.State;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by mley on 17.02.15.
 */
@Component
public class DemoWalker implements Runnable {

    private static final Logger LOG = Logger.getLogger(UpdateService.class);

    @Autowired
    private DemoRouteDao drd;

    @Value("${monitor.demo.map}")
    private String[] maps;

    @Value("${monitor.product.resource}")
    private String productResource;

    @Value("${monitor.demo.min}")
    private int[] minCustomers;

    @Value("${monitor.demo.max}")
    private int[] maxCustomers;

    private volatile boolean running;

    private UpdateService updateService;

    private Thread thread;


    private final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Map with config-data
     * key: mapName
     */
    private Map<String, DemoWalkerMap> demoMaps = new HashMap<>();


    private Map<Integer, CartItem> productCache = new HashMap<>();

    @Setter // for testing
    private long sleepInterval = 1000;

    @Setter // for testing
    private RestTemplate restTemplate = new RestTemplate();

    @Setter // for testing
    private Random random = new Random();

    private long iteration;


    @Override
    public void run() {
        iteration = 0;

        for (int i = 0; i < maps.length; i++) {
            createDemoMap(i);
        }

        while (running) {
            try {
                for (DemoWalkerMap map : demoMaps.values()) {
                    updateDemoWalker(map);
                }
            } catch (Exception e) { //NOSONAR gotta catch'em all
                LOG.error("FAILZ", e);
            }
            try {
                Thread.sleep(sleepInterval);
            } catch (InterruptedException e) { //NOSONAR can be ignored

            }
            iteration++;
        }
    }

    /**
     * Start the demo walker thread
     *
     * @param updateService UpdateService
     */
    public synchronized void start(UpdateService updateService) {

        if(running || (thread != null && thread.isAlive())) {
            throw new IllegalStateException("Already started");
        }

        running = true;
        this.updateService = updateService;

        thread = new Thread(this);
        thread.start();
    }

    private void createDemoMap(int i) {
        final String mapName = maps[i];

        final DemoWalkerMap map = new DemoWalkerMap();
        map.setName(mapName);
        map.setMin(minCustomers[i]);
        map.setMax(maxCustomers[i]);

        demoMaps.put(mapName, map);
    }

    /**
     * Stop the demo walker thread
     */
    public synchronized void stop() {
        running = false;
        if(thread != null) {
            try {
                thread.join();
            } catch (InterruptedException e) {//NOSONAR can be ignored

            }
        }
    }

    private void updateDemoWalker(DemoWalkerMap map) {
        if (iteration % 5 == 0 && // every 5 "steps" we add customers
                (map.getWalking().size() < map.getMin() // check if at least minCustomers are in
                        // or randomly add one if max is not reached yet
                        || (map.getWalking().size() < map.getMax() && random.nextDouble() < 0.2))
                ) {
            addCustomer(map);
        }
        walkRoutes(map);

    }

    private void walkRoutes(DemoWalkerMap map) {


        // for each route we're walking
        for (final Iterator<List<RoutePoint>> i = map.getWalking().iterator(); i.hasNext(); ) {
            final List<RoutePoint> route = i.next();


            // we send the next point
            final RoutePoint p = route.remove(0);
            sendRoutePoint(map, p);

            // route walked to end
            if (route.isEmpty()) {
                i.remove();
                map.getCustomers().remove(p.getDeviceId());
                try {
                    updateService.removeCustomer(map.getName(), p.getDeviceId());
                } catch (IOException e) {
                    LOG.error("FAILZ", e);
                }
            }
        }
    }

    private Customer getCustomer(DemoWalkerMap map, RoutePoint rp) {
        Customer c;
        if (!map.getCustomers().containsKey(rp.getDeviceId())) {
            c = new Customer();
            c.setDeviceId(rp.getDeviceId());
            c.setMap(rp.getMap());
            c.setName(rp.getName());
            map.getCustomers().put(rp.getDeviceId(), c);
        } else {
            c = map.getCustomers().get(rp.getDeviceId());
        }

        return c;
    }

    private void sendRoutePoint(DemoWalkerMap map, RoutePoint p) {


        final Customer customer = getCustomer(map, p);
        final String oldState = customer.getState();

        customer.setState(State.fromKey(p.getStatus()));
        customer.setLocation(new double[]{p.getLat(), p.getLng()});
        customer.setTimestamp(System.currentTimeMillis());

        try {


            // if new status is paid, we clear the shopping cart
            if (p.getStatus() == 1 && !State.PAID.equals(oldState) ) {
                customer.getCartItems().clear();
                try {
                    updateCart(customer);
                } catch (IOException e) {
                    LOG.error("FAIL", e);
                }
            } else if (p.getProduct() > 0) {
                customer.addCartItem(getProduct(p.getProduct()));

                updateCart(customer);
            }

            updateService.updatePosition(map.getName(), p.getDeviceId(), System.currentTimeMillis(),
                    new double[]{p.getLat(), p.getLng()}, State.fromKey(p.getStatus()));
        } catch (IOException e) {
            LOG.error("FAIL", e);
        }
    }


    private void updateCart(Customer customer) throws IOException {
        final Customer newCustomer = objectMapper.readValue(objectMapper.writeValueAsString(customer), Customer.class);
        updateService.updateCustomer(newCustomer);
    }


    private CartItem getProduct(Integer productId) {
        CartItem ci;
        if (!productCache.containsKey(productId)) {
            final Map<String, Object> product = restTemplate.getForObject(productResource + "/" + productId, Map.class);
            ci = new CartItem((String) product.get("description"), 1, (Integer) product.get("price"));
            productCache.put(productId, ci);
        } else {
            ci = productCache.get(productId);
        }

        return ci;
    }

    private void addCustomer(DemoWalkerMap map) {
        if (map.getStandingBy().isEmpty()) {
            map.getStandingBy().addAll(drd.getRoutes(map.getName()));
        }
        if (map.getStandingBy().isEmpty()) {
            return;
        }

        final List<RoutePoint> route = map.getStandingBy().remove(random.nextInt(map.getStandingBy().size()));


        // check if user already exists in walking customers and change name if required
        String deviceId = route.get(0).getDeviceId();
        while (containsDevId(map.getWalking(), deviceId)) {
            deviceId += random.nextInt(10); //NOSONAR can be ignored. Not performance relevant
        }
        for (RoutePoint rp : route) {
            rp.setDeviceId(deviceId);
            rp.setName(deviceId);
        }

        map.getWalking().add(route);
    }


    private boolean containsDevId(List<List<RoutePoint>> routes, String deviceId) {
        for (List<RoutePoint> route : routes) {
            if (route.get(0).getDeviceId().equals(deviceId)) {
                return true;
            }
        }

        return false;
    }
}
