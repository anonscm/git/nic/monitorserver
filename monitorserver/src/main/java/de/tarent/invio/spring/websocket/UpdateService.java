package de.tarent.invio.spring.websocket;

import de.tarent.sellfio.web.dao.CustomerDAO;
import de.tarent.invio.spring.heatmap.HeatMapAggregator;
import de.tarent.sellfio.web.model.Customer;
import de.tarent.invio.spring.model.MonitorMessage;
import de.tarent.sellfio.web.model.Point;
import de.tarent.sellfio.web.model.State;
import de.tarent.invio.spring.trust.TrustEngine;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mley on 13.11.14.
 */
@Service
public class UpdateService {

    @Setter
    @Autowired
    private MonitorHandler monitorHandler;

    @Setter
    @Autowired
    private HeatMapAggregator heatMapAggregator;

    @Setter
    @Autowired
    private DemoWalker walker;

    @Setter
    @Autowired
    private ZombieCollector zombieCollector;

    @Setter
    @Autowired
    private CustomerDAO customerDAO;

    @Setter
    @Autowired
    private TrustEngine trustEngine;

    @Getter
    private final Map<String, Map<String, Customer>> currentCustomers = Collections.synchronizedMap(new HashMap<String,
            Map<String, Customer>>());


    /**
     * Post-construct method. Starts clean-up thread.
     */
    @PostConstruct
    public void postConstruct() {
        monitorHandler.setUpdateService(this);

        zombieCollector.start(this);
        walker.start(this);
    }

    /**
     * Pre-Destroy method. Stop clean-up thread.
     */
    @PreDestroy
    public void preDestroy() {
        zombieCollector.stop();
        walker.stop();
    }


    /**
     * Remove customer from map
     *
     * @param map      map name
     * @param deviceId device id
     * @throws java.io.IOException when sending message fails
     */
    public void removeCustomer(String map, String deviceId) throws IOException {
        final Map<String, Customer> carts = currentCustomers.get(map);
        if (carts == null) {
            return;
        }

        final Customer customer = carts.get(deviceId);

        if (customer != null) {
            remove(customer);
        }
    }

    /**
     * Update the position of a customer
     *
     * @param map       map name
     * @param deviceId  device id
     * @param timestamp timestamp
     * @param location  location as double array [lat, lng]
     * @param state     customer state
     * @throws IOException when the update could not be processed.
     */
    public void updatePosition(String map, String deviceId, long timestamp, double[] location, String state)
            throws IOException {
        final Customer c = getOrCreateCustomer(map, deviceId);

        c.setTimestamp(timestamp);
        c.setLocation(location);

        if (!getCustomersOfMap(map).containsKey(deviceId)) {
            // first "event": customer enters shop
            trustEngine.calculateTrust(c);
        }

        if (State.ZOMBIE.equals(c.getState())) {
            zombieCollector.free(c.getDeviceId());
        }

        if (State.CHECKOUT.equals(state) && !State.CHECKOUT.equals(c.getState())) {
            // state changed to CHECKOUT, also calculate trust.
            trustEngine.calculateTrust(c);
            saveCart(c);
        }

        c.setState(state);


        if (!getCustomersOfMap(map).containsKey(deviceId)) {
            // save initial empty cart as entry point of customer
            saveCart(c);
        }

        trustEngine.updateCustomerPosition(deviceId, location);

        sendUpdate(c);

        storePosition(timestamp, map, deviceId, location);
    }


    private boolean positionEquals(double[] l1, double[] l2) {
        return !(l1 == null || l2 == null) && l1[0] == l2[0] && l1[1] == l2[1];
    }

    /**
     * Gets an existing customer from the customers map or creates a new customer object, which is not stored in the
     * customers map (yet).
     * @param map map name.
     * @param deviceId device id.
     * @return Customer object.
     */
    private Customer getOrCreateCustomer(String map, String deviceId) {
        Customer c;
        if (!getCustomersOfMap(map).containsKey(deviceId)) {
            c = new Customer();
            c.setState(State.SHOPPING);
            c.setDeviceId(deviceId);
            c.setMap(map);
            c.setName(deviceId);
        } else {
            c = getCustomersOfMap(map).get(deviceId);
        }

        return c;
    }

    /**
     * Update the shopping cart of customer.
     *
     * @param customer Customer object
     * @throws IOException when the update could not be processed.
     */
    public void updateCustomer(Customer customer) throws IOException { //NOSONAR Method Length: splitting it up would make it harder
        // to understand.
        final Customer oldCustomer = getCustomer(customer.getMap(), customer.getDeviceId());


        if (oldCustomer == null || oldCustomer.getTimestamp() < customer.getTimestamp()) {

            // copy values from old cart
            if (oldCustomer != null) {
                if (customer.getLat() == 0.0 && customer.getLng() == 0.0) {
                    customer.setLocation(oldCustomer.getLocation());
                }

                if (oldCustomer.getState() != null && customer.getState() == null) {
                    customer.setState(oldCustomer.getState());
                }

                customer.setTrust(oldCustomer.getTrust());
            }

            if (customer.getState() == null) {
                if (customer.getCartItems() != null && customer.getCartItems().size() > 0) {
                    customer.setState(State.SHOPPING);
                } else {
                    customer.setState(State.ZOMBIE);
                }
            }


            if (!customerEquals(oldCustomer, customer)) {
                trustEngine.calculateTrust(customer);
                saveCart(customer);
                sendUpdate(customer);
            }
        }

        if (customer.getLocation()[0] != 0 && customer.getLocation()[1] != 0) {
            storePosition(customer.getTimestamp(), customer.getMap(), customer.getDeviceId(), customer.getLocation());
        }

    }

    private void saveCart(Customer customer) {
        customerDAO.storeCart(customer);
        getCustomersOfMap(customer.getMap()).put(customer.getDeviceId(), customer);
    }

    private Map<String, Customer> getCustomersOfMap(String map) {
        if (!currentCustomers.containsKey(map)) {
            currentCustomers.put(map, new HashMap<String, Customer>());
        }

        return currentCustomers.get(map);
    }

    private Customer getCustomer(String map, String deviceId) {
        if (currentCustomers.containsKey(map)) {
            return currentCustomers.get(map).get(deviceId);
        }

        return null;
    }

    private boolean customerEquals(Customer oldCustomer, Customer customer) {
        if (oldCustomer == null || customer == null) {
            return false;
        }

        if(customer.getCartItems() == null && oldCustomer.getCartItems() == null) {
            return false;
        }

        if (customer.getCartItems() != null &&  customer.getCartItems().size() != oldCustomer.getCartItems().size()) {
            return false;
        }

        if (!oldCustomer.getState().equals(customer.getState())) {
            return false;
        }

        if (!positionEquals(customer.getLocation(), oldCustomer.getLocation())) {
            return false;
        }

        for (int i = 0; i < customer.getCartItems().size(); i++) {
            if (!customer.getCartItems().get(i).equals(oldCustomer.getCartItems().get(i))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Stores and broadcasts positions for the heatmap
     *
     * @param timestamp timestamp
     * @param location  location as double array [lat, lng]
     * @throws IOException
     */
    private void storePosition(long timestamp, String map, String deviceId, double[] location) throws IOException {
        if (location[0] != 0.0 || location[1] != 0.0) {
            // only store and broadcast valid locations
            // TODO: why -1 instead of null?
            // because: f1790e5	03.08.15 17:49	Robert Linden
            //          SEL-639: extracted map and device from point into their own tables.
            heatMapAggregator.aggregateHeat(new Point(timestamp, -1, -1, location), map, deviceId);
            sendHeat(map, location);
        }
    }

    /**
     * Returns MonitorMessage for all current customers to initialize the Monitor app
     *
     * @param map map name
     * @return List of MonitorMessage objects
     */
    public List<MonitorMessage> getAll(String map) {
        final List<MonitorMessage> messages = new ArrayList<>();
        for (Customer c : getCustomersOfMap(map).values()) {
            messages.add(new MonitorMessage("UPDATE", c));
        }

        return messages;
    }

    /**
     * Removes cart and customer from map
     *
     * @param c Customer
     * @throws IOException when sending message fails
     */
    void remove(Customer c) throws IOException {
        zombieCollector.free(c.getDeviceId());
        trustEngine.removeCustomer(c.getDeviceId());
        getCustomersOfMap(c.getMap()).remove(c.getDeviceId());
        sendRemove(c);
    }

    private void sendRemove(Customer c) throws IOException {
        monitorHandler.broadcast(c.getMap(), new MonitorMessage("REMOVE", c));
    }

    private void sendHeat(String map, double[] position) throws IOException {
        monitorHandler.broadcast(map, new MonitorMessage("HEAT", new double[][]{position}));
    }

    /**
     * Sends update.
     *
     * @param c cart
     * @throws IOException when sending message fails.
     */
    void sendUpdate(Customer c) throws IOException {
        monitorHandler.broadcast(c.getMap(), new MonitorMessage("UPDATE", c));
    }


}
