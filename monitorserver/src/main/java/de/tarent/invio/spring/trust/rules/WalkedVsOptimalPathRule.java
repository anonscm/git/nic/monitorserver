package de.tarent.invio.spring.trust.rules;

import de.tarent.invio.spring.trust.Rule;
import de.tarent.invio.spring.util.PathPlanner;
import de.tarent.sellfio.web.model.Customer;
import de.tarent.sellfio.web.util.HaversineDistance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Calculates the trust based on the deviation of walked distance and optimal distance between changes in the
 * shopping cart.
 * Created by mley on 19.03.15.
 */
@Component
public class WalkedVsOptimalPathRule implements Rule {

    @Autowired
    private PathPlanner pathPlanner;

    private final Map<String, HistoricDistance> historicDistanceMap = new HashMap<>();

    @Override
    public void removeCustomer(String deviceId) {
        historicDistanceMap.remove(deviceId);
    }

    @Override
    public void updateCustomerPosition(String deviceId, double[] pos) {
        final HistoricDistance hd = historicDistanceMap.get(deviceId);

        if (hd == null) {
            return;
        }
        synchronized (hd) {
            hd.addUpWalkedDistance(pos);
        }
    }

    @Override
    public double getTrust(final Customer customer) {
        HistoricDistance hd = historicDistanceMap.get(customer.getDeviceId());

        if (hd == null) {
            hd = new HistoricDistance();
            hd.lastPosition = customer.getLocation();
            hd.lastCartChangePosition = customer.getLocation();
            historicDistanceMap.put(customer.getDeviceId(), hd);
            return -1;
        }

        synchronized (hd) {
            hd.addUpWalkedDistance(customer.getLocation());
            hd.addUpOptimalDistance(customer.getMap(), customer.getLocation());
        }

        // calculate deviation over all distances
        final double deviation = hd.getDeviation();

        // deviation shall be larger than 1 (walked route shall not be shorter than optimal route)
        if (deviation >= 1 && isValidValue(deviation)) {

            // trust is inverse to deviation in path.
            return 1.0 / deviation;
        }


        return -1;
    }


    private boolean isValidValue(final double... value) {
        for (final double d : value) {
            if (Double.isInfinite(d) || Double.isNaN(d)) {
                return false;
            }
        }

        return true;
    }


    /**
     * Value object to cummulate distances.
     */
    private class HistoricDistance {

        /**
         * sum of actual walked distances
         */
        double aDist;

        /**
         * sum of optimal distances
         */
        double oDist;

        /**
         * position of last shopping cart change
         */
        double[] lastCartChangePosition;

        /**
         * last tracked position of customer
         */
        double[] lastPosition;

        /**
         * Deviation is the factor, which the customer walked more than the optimal path.
         *
         * @return deviation value. May be Infinity or NaN.
         */
        public double getDeviation() {
            return aDist / oDist;
        }

        /**
         * Add way walked from last known position to the sum of walked distance.
         *
         * @param pos current customer position.
         */
        public void addUpWalkedDistance(double[] pos) {
            aDist += HaversineDistance.calcDistance(lastPosition, pos);
            lastPosition = Arrays.copyOf(pos, 2);
        }

        /**
         * Add way of optimal distance to the sum of optimal distances.
         * @param map name of map
         * @param pos position.
         */
        public void addUpOptimalDistance(String map, double[] pos) {
            oDist += pathPlanner.calcOptimalDistance(map, lastCartChangePosition, pos);
            lastCartChangePosition = Arrays.copyOf(pos, 2);
        }
    }
}
