package de.tarent.invio.spring.model;

import de.tarent.sellfio.web.model.Customer;
import lombok.Data;

/**
 * Created by mley on 12.09.14.
 */
@Data
public class MonitorMessage {

    public static final MonitorMessage REMOVE_ALL = new MonitorMessage("REMOVEALL");

    private String event;
    private Customer customer;
    private double[][] positions;

    /**
     * Creates a new MonitorMessage
     * @param e event
     * @param c cart
     */
    public MonitorMessage(String e, Customer c) {
        this.event = e;
        this.customer = c;
    }

    public MonitorMessage(String e, double[][] p) {
        this.event = e;
        this.positions = p;
    }

    public MonitorMessage(String e) {
        this.event = e;
    }
    /**
     * Default constructor
     */
    public MonitorMessage() {

    }
}
