package de.tarent.invio.spring.heatmap;

import de.tarent.sellfio.web.model.AggregatedPoint;
import de.tarent.sellfio.web.model.Map;
import de.tarent.sellfio.web.model.Point;
import lombok.Getter;
import org.apache.log4j.Logger;

/**
 * This class collects and aggregates the heatmap information for one map.
 * <p/>
 * For the migration of the data in the point table to the aggregated data, see git branch heatpoint_migration.q
 */

public class AggregatedHeatMap {

    private static final Logger LOG = Logger.getLogger(AggregatedHeatMap.class);

    /**
     * Size of the heat cell in meters.
     */
    private static final double CELL_SIZE = 0.5;

    /**
     * Size of a heatcell in lat/lon degrees.
     */
    private final double latStep;
    private final double lonStep;

    /**
     * Number cells in x/y direction.
     */
    @Getter
    private final int xCells;
    @Getter
    private final int yCells;

    @Getter
    private final Map map;

    /**
     * Heatmap data, volatile, because new arrays are created whenever the heatmaps are saved and recreateHeatMap() is
     * called, which can be another thread than the one that calls aggregatePoint()
     */
    private volatile AggregatedPoint[][] heatMap;

    /**
     * Timestamp of last heatmap creation.
     */
    private volatile long tstamp;

    /**
     * Creates a new heatmap.
     *
     * @param map              Map object.
     * @param initialTimestamp the start timestamp
     */
    public AggregatedHeatMap(Map map, long initialTimestamp) {

        this.map = map;

        // calculate how many cells our map has
        final double xCellsD = map.getWidth() / CELL_SIZE;
        final double yCellsD = map.getHeight() / CELL_SIZE;

        // calculate the lat/lon distance of a cell
        latStep = (map.getMaxLat() - map.getMinLat()) / yCellsD;
        lonStep = (map.getMaxLon() - map.getMinLon()) / xCellsD;

        // round up to have an integer number of cells
        xCells = (int) Math.ceil(xCellsD);
        yCells = (int) Math.ceil(yCellsD);

        // create initial heatmap array
        recreateHeatMap(initialTimestamp);
    }

    /**
     * Recreates the heatmap array and returns the old array to be stored. This method is synchronized, because the
     * heatmap array is accessed.
     *
     * @param newTimestamp the new timestamp
     * @return AggregatedPoint array
     */
    public final synchronized AggregatedPoint[][] recreateHeatMap(final long newTimestamp) {
        tstamp = newTimestamp;
        final AggregatedPoint[][] oldHeatMap = heatMap;
        heatMap = new AggregatedPoint[xCells][yCells];
        return oldHeatMap;
    }

    /**
     * Aggregate the data of a heat point.
     *
     * @param p the heat point.
     */
    public void aggregatePoint(Point p) {
        final double lat = p.getLat() - map.getMinLat();
        final double lon = p.getLng() - map.getMinLon();

        final int y = (int) (lat / latStep);
        final int x = (int) (lon / lonStep);

        if (coordsValid(x, y)) {

            getAggregatedPoint(x, y).visit(p.getDevice());
        } else {
            LOG.debug("Invalid position. Not inside map bounds.: " + p.getLat() + " " + p.getLng());
        }
    }

    private boolean coordsValid(int x, int y) {
        return x >= 0 && y >= 0 && x < heatMap.length && y < heatMap[x].length;
    }

    /**
     * Gets an AggregatedPoint for a position. If no objects exists in the array, it will be created.
     *
     * @param x x position
     * @param y y position
     * @return AggregatedPoint object
     */
    private synchronized AggregatedPoint getAggregatedPoint(int x, int y) {
        if (heatMap[x][y] == null) {

            // calculate position in center of cell
            final double lat = ((y + 0.5) * latStep) + map.getMinLat();
            final double lon = ((x + 0.5) * lonStep) + map.getMinLon();

            heatMap[x][y] = new AggregatedPoint(map.getId(), lat, lon, tstamp);
        }
        return heatMap[x][y];
    }
}
