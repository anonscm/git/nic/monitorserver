package de.tarent.invio.spring.trust;

import de.tarent.sellfio.web.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The trust engine determines a customer's trust based on the implemented rules and their values.
 */
@Component
public class TrustEngine {

    @Autowired
    private List<Rule> rules;

    private Map<String, HistoricConfidenceData> historicConfidenceDataMap = new HashMap<>();

    /**
     * Calculates the trust index.
     *
     * @param customer current shopping cart.
     */
    public void calculateTrust(final Customer customer) {
        // start with full trust
        double trust = 0;

        // count number of rules which calculated valid trust values
        int validValues = 0;

        for (final Rule rule : rules) {
            final double t = rule.getTrust(customer);

            //TODO get weight for each rule (store in DB, make it configurable via Frontend)

            // only accept valid values
            if (t >= 0.0 && t <= 1.0) {
                validValues++;
                trust += t;
            }
        }

        if(validValues > 0 ) {
            trust /= validValues;
        }

        final HistoricConfidenceData historicConfidence = getHistoricConfidence(customer.getDeviceId());
        historicConfidence.addValue(rules.size(), validValues);

        customer.setTrust(trust);
        customer.setConfidence(historicConfidence.getConfidence());

    }

    /**
     * Updates the customer position in all rules.
     * @param deviceId device id.
     * @param pos position.
     */
    public void updateCustomerPosition(String deviceId, double[] pos) {
        for(Rule r : rules) {
            r.updateCustomerPosition(deviceId, pos);
        }
    }

    private HistoricConfidenceData getHistoricConfidence(String deviceId) {
        HistoricConfidenceData data;
        if(historicConfidenceDataMap.containsKey(deviceId)) {
            data = historicConfidenceDataMap.get(deviceId);
        } else {
            data = new HistoricConfidenceData();
            historicConfidenceDataMap.put(deviceId, data);
        }

        return data;
    }

    /**
     * Data object to store confidence data.
     */
    private static class HistoricConfidenceData {
        int numOfCalculations = 0;
        int numOfValidCalculations = 0;

        /**
         * Confidence is percentage of valid calculated rules.
         * @return the confidence that our calculations make sense.
         */
        public double getConfidence() {
            double confidence = 0;
            if(numOfCalculations > 0) {
                confidence = numOfValidCalculations / (double)numOfCalculations;
            }
            return confidence;
        }

        /**
         * Add confidence data.
         * @param numOfRules number of rules.
         * @param validRules number of rules which returned useful results.
         */
        public void addValue(int numOfRules, int validRules) {
            numOfCalculations += numOfRules;
            numOfValidCalculations += validRules;
        }
    }

    /**
     * Called to remove customer data from rule engine.
     * @param deviceId device id.
     */
    public void removeCustomer(String deviceId) {

        historicConfidenceDataMap.remove(deviceId);

        for(final Rule rule : rules) {
            rule.removeCustomer(deviceId);
        }
    }
}
