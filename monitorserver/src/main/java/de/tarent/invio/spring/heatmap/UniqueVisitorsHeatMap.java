package de.tarent.invio.spring.heatmap;

import de.tarent.sellfio.web.model.InvioGeoPoint;
import de.tarent.sellfio.web.model.AggregatedPoint;
import de.tarent.sellfio.web.model.HeatPoint;

import java.util.Map;

/**
 * Created by mley on 21.07.15.
 */
public class UniqueVisitorsHeatMap {

    public static final int POS_A = 0;
    public static final int POS_B = 1;
    public static final int LAT = 0;
    public static final int LNG = 1;

    /**
     * Bounds of map in form of [[lat,lng],[lat,lng].
     */
    private double[][] bounds;

    /**
     * Size of one cell in X direction, where "size" is interpreted according to the longitude-scale,
     * i.e. purely numeric.
     */
    private double xRasterSize;

    /**
     * Size of one cell in Y direction, where "size" is interpreted according to the longitude-scale,
     * i.e. purely numeric.
     */
    private double yRasterSize;

    /**
     * Number of cells in X direction.
     */
    private int rasterWidth;

    /**
     * Number of cells in Y direction.
     */
    private int rasterHeight;

    /**
     * Calculates the unique visitors in the raster and returns the raster as 2D array.
     *
     * @param heatmap    the aggregated heatmap data
     * @param map        AggregatedHeatMap object
     * @param downSample factor by which the heatmap resolution is sampled down.
     * @param unique     flag if heatmap shall contain unique visits or all visits
     * @return HeatPoint array
     */
    public HeatPoint[][] calculateAggregatedHeatPoints(Map<InvioGeoPoint, AggregatedPoint> heatmap,
                                                       AggregatedHeatMap map, int downSample,
                                                       boolean unique) {


        final HeatPoint[][] raster = createRaster(map, downSample, unique);

        for (AggregatedPoint p : heatmap.values()) {
            if (isInBounds(p.getLat(), p.getLon())) {
                final HeatPoint hp = findHeatPoint(raster, p.getLat(), p.getLon());
                if (unique) {
                    for (Integer deviceId : p.getDeviceIds()) {
                        hp.count(deviceId);
                    }
                } else {
                    hp.count(p.getVisits());
                }
            }
        }

        return raster;
    }

    private HeatPoint[][] createRaster(AggregatedHeatMap heatMap, int downSample, boolean unique) {
        bounds = heatMap.getMap().getBounds();

        // use existing grid size to calculate new, downsampled heatmap grid size
        rasterWidth = (int) Math.ceil(heatMap.getXCells() / (double) downSample);
        rasterHeight = (int) Math.ceil(heatMap.getYCells() / (double) downSample);

        // Calculate height and width, as absolute lat/lng-differences:
        // FIXME: this might not work if map spans across null-meridian, equator or poles
        final double width = bounds[POS_B][LNG] - bounds[POS_A][LNG];
        final double height = bounds[POS_B][LAT] - bounds[POS_A][LAT];

        xRasterSize = width / rasterWidth;
        yRasterSize = height / rasterHeight;


        // Create 2D array with all heatpoints:
        final HeatPoint[][] raster = new HeatPoint[rasterWidth][rasterHeight];

        for (int x = 0; x < rasterWidth; x++) {
            for (int y = 0; y < rasterHeight; y++) {

                raster[x][y] = new HeatPoint();
                if (!unique) {
                    raster[x][y].setCountedIds(null);
                }

                // Position of heat point is in the middle of the raster cell, therefore  + 0.5:
                final double lng = bounds[POS_A][LNG] + (x + 0.5) * xRasterSize;
                final double lat = bounds[POS_A][LAT] + (y + 0.5) * yRasterSize;

                raster[x][y].setPos(new double[]{lat, lng});
            }
        }

        return raster;
    }


    /**
     * Test if the point is within the request bounds.
     *
     * @param lat Latitude
     * @param lng Longitude
     * @return true if Point p is inside the bounds.
     */
    private boolean isInBounds(double lat, double lng) {
        return bounds[POS_A][LAT] <= lat && bounds[POS_B][LAT] >= lat &&
                bounds[POS_A][LNG] <= lng && bounds[POS_B][LNG] >= lng;
    }

    /**
     * Gets the HeatPoint for the position in Point
     *
     * @param lat Latitude
     * @param lng Longitude
     * @return matching HeatPoint object
     */
    private HeatPoint findHeatPoint(HeatPoint[][] raster, double lat, double lng) {
        // get distance from origin
        final double dx = lng - bounds[POS_A][LNG];
        final double dy = lat - bounds[POS_A][LAT];

        // divide by raster size
        int x = (int) (dx / xRasterSize);
        int y = (int) (dy / yRasterSize);


        // required for corner cases (p is exactly on bounds)
        x = Math.min(x, rasterWidth - 1);
        y = Math.min(y, rasterHeight - 1);

        return raster[x][y];
    }


}
