package de.tarent.invio.spring.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tarent.invio.spring.model.MonitorMessage;
import de.tarent.invio.spring.model.Subscription;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.web.socket.sockjs.SockJsTransportFailureException;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Web socket handler for users of the Monitor Web App.
 */

public class MonitorHandler extends TextWebSocketHandler {

    private static final Logger LOG = Logger.getLogger(MonitorHandler.class);

    private ObjectMapper objectMapper = new ObjectMapper();

    private Collection<WebSocketSession> sessions = new ConcurrentLinkedQueue<>();

    private Map<WebSocketSession, Subscription> subscribedMaps =
            Collections.synchronizedMap(new HashMap<WebSocketSession, Subscription>());

    @Setter
    private UpdateService updateService;

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) {
        LOG.debug("Received " + session.getId() + " " + message.getPayload());
        //TODO  check and validate accesstoken in first message

        try {
            final Subscription s = objectMapper.readValue(message.asBytes(), Subscription.class);
            LOG.info("subscribing to map: " + s.getMap() + " " + s.isHeat());
            subscribedMaps.put(session, s);


            for (MonitorMessage m : updateService.getAll(s.getMap())) {
                sendMessage(session, m);
            }

        } catch (IOException e) {
            LOG.error("error deserializing message", e);
        }


    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws IOException {
        LOG.info("Connect: " + session.getId());
        sessions.add(session);

        // clear all when (re)connecting
        sendMessage(session, MonitorMessage.REMOVE_ALL);

    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        LOG.info("Closed: " + session.getId() + status);
        sessions.remove(session);
        subscribedMaps.remove(session);
    }

    /**
     * Broadcast message to all monitor websockets
     *
     * @param map map name
     * @param m   MonitorMessage object to send
     * @throws IOException in case the message could not be serialized or sent
     */
    public void broadcast(String map, MonitorMessage m) throws IOException {
        final String message = objectMapper.writeValueAsString(m);
        LOG.debug("Broadcasting message " + message);
        final TextMessage tm = new TextMessage(message);
        for (WebSocketSession session : sessions) {

            final Subscription s = subscribedMaps.get(session);

            if (s == null) {
                continue;
            }

            // check if it's a HEAT message and subscriber has them subscribed
            if (map.equals(s.getMap()) &&
                    (!("HEAT".equals(m.getEvent()) && !s.isHeat()))) {
                sendMessage(session, tm);
            }
        }
    }

    private void sendMessage(WebSocketSession session, TextMessage tm) {
        try {
            synchronized (session) {
                session.sendMessage(tm);
            }
        } catch (IllegalStateException e) {
            LOG.warn("Error writing into socket: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            LOG.warn("Error writing into socket:" + e.getMessage());
        } catch (SockJsTransportFailureException e) {
            LOG.warn("Error writing into socket:" + e.getMessage());
        } catch (IOException e) {
            LOG.warn("Error writing into socket.", e);
        }
    }

    private void sendMessage(WebSocketSession session, MonitorMessage m) throws IOException {
        final String message = objectMapper.writeValueAsString(m);
        final TextMessage tm = new TextMessage(message);
        sendMessage(session, tm);
    }


}
