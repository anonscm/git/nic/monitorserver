package de.tarent.invio.spring.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * Created by mley on 12.11.14.
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    private static final MonitorHandler MONITOR_HANDLER = new MonitorHandler();

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(monitorHandler(), "/monitor").setAllowedOrigins("*").withSockJS();
    }

    /**
     * Gets the MonitorHandler instance.
     * @return MonitorHandler object.
     */
    @Bean
    public MonitorHandler monitorHandler() {
        return MONITOR_HANDLER;
    }

}
