-- This script does:
--   - create a new table for devices
--   - make the point table use map and devices, instead of keeping their long names itself
--   - safely migrate all data (might be slow if there are many points!)
--
-- Motivation: make the table smaller and thus faster by not storing redundant strings
--
-- After the conversion maybe a "VACUUM FULL point" is required to really get rid of obsolete bits and bytes.
-- But for big tables it will be very slow, so this script doesn't do it.


-- The table "device" just stores deviceIDs, for the moment.
-- (Maybe later it could/should also be referenced from table "customer"?)
CREATE TABLE device (
  id serial NOT NULL,
  name VARCHAR(255),
  PRIMARY KEY (id)
);


-- Copy all deviceIds from all points into the new device table:
INSERT INTO device (name)
  SELECT DISTINCT(p.deviceid)
  FROM point p
  WHERE p.deviceid NOT IN (
    SELECT name FROM device
  );


-- Create index to quickly find device ID by name:
CREATE INDEX device_name_index ON device (name);


-- Add new column device to table point.
-- (It has no FK-constraint because we might want to allow anonymous points and because it's faster.)
ALTER TABLE point
  ADD COLUMN device INTEGER;

-- Fill the new device column with the IDs of the devices:
UPDATE point p SET device = (SELECT id FROM device d WHERE d.name = p.deviceid);


-- Ensure that there are no null values in the map column:
UPDATE point SET map = 'null' WHERE map IS NULL;

-- Copy all map names from all points into the existing map table:
INSERT INTO map (name)
  SELECT DISTINCT(p.map)
  FROM point p
  WHERE p.map NOT IN (
    SELECT name FROM map
  );


-- Create index to quickly find map ID by name:
CREATE INDEX map_name_index ON map (name);


-- Add new column map_tmp th table point:
ALTER TABLE point
  ADD COLUMN map_tmp INTEGER;

-- Fill the new map_tmp column with the IDs of the maps:
UPDATE point p SET map_tmp = (SELECT id FROM map m WHERE m.name = p.map);


-- Remove the old index on obsolete column map:
DROP INDEX point_index;

-- Remove the two columns map + deviceid, which are now obsolete:
ALTER TABLE point
  DROP COLUMN deviceid,
  DROP COLUMN map;

-- Rename map_tmp to its final name map:
ALTER TABLE point
  RENAME COLUMN map_tmp TO map;


-- Add FK and NOT NULL constraints to the (new) map column, because a point with unspecified map makes no sense:
ALTER TABLE point
  ALTER COLUMN map SET NOT NULL,
  ADD CONSTRAINT FK_point_map FOREIGN KEY (map) REFERENCES map ON DELETE CASCADE;


-- Add again the index for quicker searching:
-- (This index does impose a certain write overhead, but is necessary for the heatmap - we should keep an eye on it.)
CREATE INDEX point_index ON point (map, timestamp);
