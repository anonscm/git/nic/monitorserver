DROP TYPE IF EXISTS transmitter_enum;

alter TABLE wifinetworks drop CONSTRAINT fk_wifinetworks_wifiap;
alter TABLE wifinetworks add CONSTRAINT fk_wifinetworks_wifiap FOREIGN KEY (wifiap) REFERENCES wifiap (id) on DELETE CASCADE;

alter TABLE wifinetworks DROP CONSTRAINT fk_wifinetworks_transmitter;
alter TABLE wifinetworks add  CONSTRAINT fk_wifinetworks_networknode FOREIGN KEY (networknode) REFERENCES networknode (id) on delete CASCADE;

alter TABLE networknode add  CONSTRAINT fk_networknode_transmitter FOREIGN KEY (transmitter_id) REFERENCES transmitter (id) on delete CASCADE;
