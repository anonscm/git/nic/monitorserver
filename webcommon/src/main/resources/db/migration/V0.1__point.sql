create table Point (
  id  bigserial not null,
  lat float8 not null,
  lng float8 not null,
  timestamp int8 not null,
  primary key (id)
);
