create table RoutePoint (
  id  bigserial not null,
  routeId int8 not null,
  pointInRoute int8 not null,
  map varchar(255) not null,
  name varchar(255),
  deviceId varchar(255) not null,
  lat float8 not null,
  lng float8 not null,
  status int8 not null,
  product int8 not null,
  primary key (id)
);