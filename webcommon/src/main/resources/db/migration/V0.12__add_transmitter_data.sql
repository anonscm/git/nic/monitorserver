-- n to 1 relationship to transmitter. One wifi transmitter can have multiple wifi networks/virtual APs
CREATE TABLE wifiap
(
  id     SERIAL NOT NULL,
  map_id INTEGER NOT NULL,
  lat    FLOAT8      DEFAULT 0,
  lon    FLOAT8      DEFAULT 0,
  height FLOAT8      DEFAULT 0,
  vendor VARCHAR(64) DEFAULT NULL,
  name   VARCHAR(64) DEFAULT NULL,
  CONSTRAINT wifiap_pkey PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

CREATE TABLE networknode (
  id             SERIAL NOT NULL,
  band           FLOAT8  NOT NULL,
  channelNumber  INTEGER NOT NULL,
  macAddress     VARCHAR(64),
  name           VARCHAR(64),
  transmitter_id BIGINT  NOT NULL,
  CONSTRAINT networknode_pkey PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);


CREATE TABLE wifinetworks
(
  wifiap      SERIAL NOT NULL,
  networknode SERIAL NOT NULL,
  CONSTRAINT fk_wifinetworks_wifiap FOREIGN KEY (wifiap) REFERENCES wifiap (id),
  CONSTRAINT fk_wifinetworks_transmitter FOREIGN KEY (networknode) REFERENCES networknode (id)
) WITH (
OIDS = FALSE
);

-- 1 to 1 relationship to transmitter
CREATE TABLE btletransmitter
(
  id             SERIAL      NOT NULL,
  lat            FLOAT8 DEFAULT 0,
  lon            FLOAT8 DEFAULT 0,
  height         FLOAT8 DEFAULT 0,
  vendor         VARCHAR(64),
  name           VARCHAR(64),
  uuid           VARCHAR(36) NOT NULL,
  majorId        SMALLINT    NOT NULL,
  minorId        SMALLINT    NOT NULL,
  powerLevel     VARCHAR(16),
  sendInterval   INT,
  transmitter_id SERIAL      NOT NULL,
  CONSTRAINT btletransmitter_pkey PRIMARY KEY (id),
  CONSTRAINT fk_btletransmitter_transmitter FOREIGN KEY (transmitter_id)
  REFERENCES transmitter (id) ON DELETE CASCADE
)
WITH (
OIDS =FALSE
);

alter table fingerprint RENAME COLUMN fingerprint_type TO fingerprint_type_old;
alter table fingerprint ADD COLUMN fingerprint_type VARCHAR(4);
update fingerprint set fingerprint_type = 'btle' where cast(fingerprint_type_old as text) = 'btle';
update fingerprint set fingerprint_type = 'wifi' where cast(fingerprint_type_old as text) = 'wifi';
alter table fingerprint DROP COLUMN fingerprint_type_old;

alter table transmitter RENAME COLUMN transmitter_type TO transmitter_type_old;
alter table transmitter ADD COLUMN transmitter_type VARCHAR(4);
update transmitter set transmitter_type = 'btle' where cast(transmitter_type_old as text) = 'btle';
update transmitter set transmitter_type = 'wifi' where cast(transmitter_type_old as text) = 'wifi';
alter table transmitter DROP COLUMN transmitter_type_old;

alter table fingerprint add CONSTRAINT chk_fingerprint_type check (fingerprint_type in ('btle', 'wifi'));
alter table transmitter add CONSTRAINT chk_transmitter_type check (transmitter_type in ('btle', 'wifi'));