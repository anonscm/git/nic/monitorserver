-- delete duplicate fingerprints
delete from fingerprint where id not in (select max(id) from fingerprint group by map_id, name, fingerprint_type );

-- add constraint to prevent duplicate fingerprints
alter table fingerprint add UNIQUE (map_id, name, fingerprint_type);