-- create cart table
CREATE TABLE Cart (
  id        BIGSERIAL    NOT NULL,
  name      VARCHAR(255),
  deviceId  VARCHAR(255) NOT NULL,
  map       VARCHAR(255),
  timestamp INT8         NOT NULL,
  state     VARCHAR(255) NOT NULL,
  lat       FLOAT8       NOT NULL,
  lng       FLOAT8       NOT NULL,
  PRIMARY KEY (id)
);

-- create cartitem table for List of CartItems in Cart
CREATE TABLE CartItem (
  id        BIGSERIAL NOT NULL,
  cartId    BIGSERIAL NOT NULL,
  name      VARCHAR(255),
  articleId INT8,
  ean       VARCHAR(20),
  unitPrice INT8,
  amount    INT8,
  PRIMARY KEY (id)
);

CREATE TABLE Cart_cartitem (
  Cart_id      BIGSERIAL NOT NULL,
  cartItems_id BIGSERIAL NOT NULL,
  id           BIGSERIAL NOT NULL,
  PRIMARY KEY (Cart_id, id)
);

ALTER TABLE Cart_cartitem ADD CONSTRAINT FK_gqs34j70qjnongee4koahnnk5 FOREIGN KEY (cartItems_id) REFERENCES cartitem;
ALTER TABLE Cart_cartitem ADD CONSTRAINT FK_5tx9rvcj7bkf1hf8is4q0mkof FOREIGN KEY (Cart_id) REFERENCES Cart;

