
create table point_aggregated (
  id serial not null,
  tstamp int8 not null,
  lat float8 not null,
  lon float8 not null,
  map integer not null,
  visits integer not null DEFAULT 0,
  constraint point_aggregated_pkey primary key(id),
  constraint fk_point_aggregated_map FOREIGN KEY (map) REFERENCES map(id) on delete cascade

) with (OIDS=FALSE);

create table visit (
  point_aggregated integer not null,
  device integer not null,
  constraint visit_pkey primary key (point_aggregated, device),
  constraint fk_visit_point_aggregated foreign key (point_aggregated) references point_aggregated(id) on delete cascade,
  constraint fk_visit_device foreign key (device) references device(id) on delete cascade

) with (oids=false);

alter table map add column minLat float8 DEFAULT -1;
alter table map add column minLon float8 DEFAULT -1;
alter table map add column maxLat float8 DEFAULT -1;
alter table map add column maxLon float8 DEFAULT -1;
alter table map add column originLat float8 DEFAULT -1;
alter table map add column originLon float8 DEFAULT -1;
alter table map add column scale float8 DEFAULT -1;

create index point_aggregated_index on point_aggregated (map, tstamp);

create index visit_index on visit(point_aggregated);
