CREATE TYPE transmitter_enum AS ENUM ('wifi', 'btle');


CREATE TABLE map
(
  id serial NOT NULL,
  name character varying(255),
  CONSTRAINT map_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);



-- We differentiate fingerprints for wifi and btle
CREATE TABLE fingerprint
(
  id serial NOT NULL,
  map_id integer NOT NULL,
  name character varying(255),
  lat double precision NOT NULL,
  lon double precision NOT NULL,
  fingerprint_type transmitter_enum NOT NULL,
  CONSTRAINT fingerprint_pkey PRIMARY KEY (id),
  CONSTRAINT fk_fingerprint_map FOREIGN KEY (map_id)
      REFERENCES map (id) ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);




--A transmitter belongs to one specific map.
CREATE TABLE transmitter
(
  id serial NOT NULL,
  map_id integer NOT NULL,
  identifier character varying(255) NOT NULL,
  active boolean NOT NULL,
  transmitter_type transmitter_enum NOT NULL,
  CONSTRAINT transmitter_pkey PRIMARY KEY (id),
  CONSTRAINT fk_transmitter_map FOREIGN KEY (map_id)
      REFERENCES map (id) ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);



-- Is a relation between transmitter and fingerprints. It groups all the meassurements since a transmitter belongs to more than one fingerprint.
CREATE TABLE histogram
(
  id serial NOT NULL,
  transmitter_id integer NOT NULL,
  fingerprint_id integer NOT NULL,

  CONSTRAINT histogram_pkey PRIMARY KEY (id),
  CONSTRAINT fk_histogram_transmitter FOREIGN KEY (transmitter_id)
      REFERENCES transmitter (id) ON DELETE CASCADE,
  CONSTRAINT fk_histogram_fingerprint FOREIGN KEY (fingerprint_id)
      REFERENCES fingerprint (id) ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);



-- We use count instead of probability because the count is more clear.
CREATE TABLE measurement
(
  histogram_id integer NOT NULL,
  strength smallint NOT NULL,
  probability double precision NOT NULL,

  CONSTRAINT measurement_pkey PRIMARY KEY (histogram_id, strength),
  CONSTRAINT fk_measurement_histogram FOREIGN KEY (histogram_id)
      REFERENCES histogram (id) ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
