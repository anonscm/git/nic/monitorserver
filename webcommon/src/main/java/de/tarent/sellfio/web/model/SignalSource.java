package de.tarent.sellfio.web.model;

/**
 * Created by mley on 29.10.15.
 */
public interface SignalSource {

    double getLat();
    double getLon();
    double getHeight();
    String getVendor();
    String getName();
}
