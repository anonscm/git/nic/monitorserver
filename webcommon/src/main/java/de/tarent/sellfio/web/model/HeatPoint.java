package de.tarent.sellfio.web.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mley on 21.07.15.
 * HeatPoint Object to count unique visitors within one raster cell.
 */
@Data
@JsonIgnoreProperties(value = {"countedIds", "visitCount"}, ignoreUnknown = true)
public class HeatPoint {

    /**
     * [lat, lng]
     */
    double[] pos;

    /**
     * does not need to be serialized. If set to null, the count() method will count all visits and treat the passed
     * value as number of visitors.
     */
    transient Set<Integer> countedIds = new HashSet<>();

    transient int visitCount = 0;

    /**
     * Count a visitor.
     *
     * @param value deviceId of visitor or the counted number of all visits. This is not the deviceId-string, but the
     *           unique DB-identifier. (The result would be the same in both cases.)
     */
    public void count(Integer value) {
        if (countedIds != null) {
            countedIds.add(value);
        } else {
            visitCount += value;
        }
    }

    /**
     * Returns the number of visitors
     *
     * @return visitor count
     */
    public int getCount() {
        if(countedIds != null) {
            return countedIds.size();
        } else {
            return visitCount;
        }
    }

}
