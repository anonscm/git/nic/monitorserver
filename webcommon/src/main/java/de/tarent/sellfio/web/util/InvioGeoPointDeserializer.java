package de.tarent.sellfio.web.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.tarent.sellfio.web.model.InvioGeoPoint;

import java.lang.reflect.Type;

/**
 * This is a simple deserializer for our InvioGeoPoint-interface which will be called by gson as needed.
 */
public class InvioGeoPointDeserializer implements JsonDeserializer<InvioGeoPoint> { // NOSONAR, gson wants instances.
    @Override
    public InvioGeoPoint deserialize(final JsonElement jsonElement,
                                     final Type type,
                                     final JsonDeserializationContext jsonDeserializationContext) {

        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        return new InvioGeoPoint(jsonObject.get("latitude").getAsDouble(),
                jsonObject.get("longitude").getAsDouble());
    }
}

