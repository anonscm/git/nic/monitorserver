package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.Map;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * The MapService holds a working set of known mappings between map-names and the
 * DB-id of the map. This is used to find the right id for new Points.
 * The MapService is also in charge of persisting new mappings.
 */
@Service
@Scope(value = "singleton")
public class MapService {

    private static final int NEW_ID_PLACEHOLDER = -1;

    // This Map maps a map name to the DB-id of that map.
    @Getter // for testing
    private ConcurrentMap<String, Integer> nameToId = new ConcurrentHashMap<>(100);

    // This map is the inverse of the other one: it maps the DB-id to the name.
    @Getter // for testing
    private ConcurrentMap<Integer, String> idToName = new ConcurrentHashMap<>(100);

    @Autowired
    private MapDAO mapDAO;


    /**
     * Get the name for a map with a given DB-id, if it exists, null otherwise
     * @param id the map's DB-id
     * @return name of the map
     */
    public String getName(final Integer id) {
        String mapName = idToName.get(id);

        if (mapName == null) {
            // We need to try and load it from the DB. There are no concurrency issues here (above the technical ones
            // solved by the ConcurrentHashMap) because it doesn't hurt if we accidentally query and store the mapping
            // multiple times.
            final Map map = mapDAO.getById(id);
            if (map != null) {
                storeMapMapping(map.getName(), id);
                mapName = map.getName();
            }
        }

        return mapName;
    }


    /**
     * Get the DB-id for a map with a given name, if it exists, null otherwise.
     * @param name the map's name
     * @return DB-id of the map
     */
    public Integer getId(final String name) {
        Integer mapId = nameToId.get(name);

        if (mapId == null) {
            // We need to try and load it from the DB. There are no concurrency issues here (above the technical ones
            // solved by the ConcurrentHashMap) because it doesn't hurt if we accidentally query and store the mapping
            // multiple times.
            final Map map = mapDAO.getByName(name);
            if (map != null) {
                storeMapMapping(name, map.getId());
                mapId = map.getId();
            }
        }

        return mapId;
    }


    /**
     * Get the DB-id for a given map name. If the name is new then a new mapping will be created in the DB
     * transparently.
     *
     * @param name the map name
     * @return the value of the corresponding column (map.id) from the DB. Null for some strange error cases...
     */
    public Integer mapNameToId(final String name) {
        Integer id = nameToId.putIfAbsent(name, NEW_ID_PLACEHOLDER);

        // The atomic putIfAbsent returns the previous value. If it _was_ null then it is now NEW_ID_PLACEHOLDER.
        // Therefore, only one thread can ever get null as a result. All others will get NEW_ID_PLACEHOLDER, if they
        // run "at the same time".
        // Otherwise it returned a proper id, which means the name was already known and the map didn't change.
        if (id == null) {
            // This thread/call is the first to deal with the new name.
            id = loadOrCreateMapMapping(name);

            // Now we overwrite the NEW_ID_PLACEHOLDER with the proper id and also store the inverse:
            storeMapMapping(name, id);
        }

        // If another thread is currently creating the new mapping, we need to wait a bit...
        while (id == NEW_ID_PLACEHOLDER) { // The == (instead of equals) is ok here because of the auto-unboxing of id.
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) { //NOSONAR - we just ignore the exception
                // We don't care, but hope that we now have a mapping.
            }
            id = nameToId.get(name);
        }

        return id;
    }


    /**
     * Load the id of the map from the DB or create it, if it doesn't exist.
     *
     * @param name the name of the map
     * @return the value of the corresponding column (map.id) from the DB
     */
    private int loadOrCreateMapMapping(final String name) {
        Map map = mapDAO.getByName(name);

        if (map == null) {
            map = new Map(name);

            mapDAO.store(map);

        }

        return map.getId();
    }

    /**
     * store the name->id and id->name mappings in both our hashmaps
     */
    private void storeMapMapping(final String name, final Integer id) {
        nameToId.put(name, id);
        idToName.put(id, name);
    }
}
