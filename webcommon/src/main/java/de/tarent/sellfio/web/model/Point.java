package de.tarent.sellfio.web.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Created by mley on 25.11.14.
 */
@Entity
@Data
@NamedQueries({
        @NamedQuery(name = Point.BY_TIME, query = "SELECT p FROM Point p where p.map = :map and p.timestamp >= :tsStart " +
                "and p.timestamp <= :tsEnd"),
        @NamedQuery(name = Point.BY_DEVICEID, query = "SELECT p FROM Point p where p.map = :map " +
                "and p.device = :device and p.timestamp >= :tsStart and p.timestamp <= :tsEnd")

})
public class Point {

    public static final String BY_TIME = "Point.byTime";
    public static final String BY_DEVICEID = "Point.byDeviceId";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long timestamp;
    private Integer device; // This is the DB-id of the device. Will be null for new, unpersisted points.
    private Integer map;    // This is the DB-id of the map. Will be null for new, unpersisted points.
    private double lat;
    private double lng;


    public Point() {
    }

    /**
     * Creates a new Point
     *
     * @param timestamp timestamp
     * @param mapId     the DB-id of the map, or null if the ID is not yet known
     * @param deviceId     the DB-id of the map, or null if the ID is not yet known
     * @param latlng    latitude, longitude
     */
    public Point(long timestamp, Integer mapId, Integer deviceId, double[] latlng) {
        this.timestamp = timestamp;
        this.map = mapId;
        this.device = deviceId;
        this.lat = latlng[0];
        this.lng = latlng[1];
    }

    public double[] toArray() {
        return new double[]{lat, lng};
    }
}
