package de.tarent.sellfio.web.util;

import de.tarent.sellfio.web.model.Fingerprint;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by mley on 02.11.15.
 */
public final class FingerprintsUtil {


    private FingerprintsUtil() {
    }

    /**
     * Calculate the probability.
     *
     * @param fingerprints List of fingerprints
     * @return fingerprints with probability{};
     */
    public static List<Fingerprint> getFingerprintsWithCalculatedProbability(final List<Fingerprint> fingerprints) {
        final List<Fingerprint> fingerprintsWithCalculatedProbability = new ArrayList<>();

        for (final Fingerprint fingerprint : fingerprints) {
            if (fingerprint.getScanCount() != 0) { // Check if scanCount was set for wifi fingerprints.
                // TODO: What about scanTime for btle?
                // Calculate real probability
                final Fingerprint fingerprintRealProbability = calculateRealProbability(fingerprint);
                fingerprintsWithCalculatedProbability.add(fingerprintRealProbability);
            } else {
                fingerprintsWithCalculatedProbability.add(fingerprint);
            }
        }

        return fingerprintsWithCalculatedProbability;
    }

    /**
     * Calculate the probability.
     *
     * @param fingerprint fingerprint
     * @return fingerprint with probability
     */
    public static Fingerprint calculateRealProbability(final Fingerprint fingerprint) {
        for (final Map<Integer, Float> transmitterValues : fingerprint.getHistogram().values()) {
            final Set<Integer> keys = transmitterValues.keySet();

            int transmitterSignalCount = 0;

            for (final Integer key : keys) {
                transmitterSignalCount += transmitterValues.get(key);
            }

            for (final Integer key : keys) {
                final float value = transmitterValues.get(key) / transmitterSignalCount;
                transmitterValues.put(key, value);
            }
        }

        return fingerprint;
    }
}
