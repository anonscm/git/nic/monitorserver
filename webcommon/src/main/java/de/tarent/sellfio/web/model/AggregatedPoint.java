package de.tarent.sellfio.web.model;

import lombok.Data;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mley on 03.09.15.
 */
@Data
@Entity
@Table(name = "point_aggregated")
public class AggregatedPoint {

    /**
     * ID of the point
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Map id
     */
    private int map;

    /**
     * timestamp in milliseconds from epoch in UTC
     */
    private long tstamp;

    /**
     * Latitude of the center of the area that is covered by this point.
     */
    private double lat;

    /**
     * Longitude of the center of the area that is covered by this point.
     */
    private double lon;

    /**
     * number of all visits
     */
    private int visits;

    // this is currently only required for the automagic schema creation for the tests. All DAO operations are
    // done "by hand"
    @ElementCollection(fetch = FetchType.EAGER, targetClass = Integer.class)
    @CollectionTable(name="visit", joinColumns = @JoinColumn(name="point_aggregated"))
    @Column(name="device")
    private Set<Integer> deviceIds = new HashSet<>();


    public AggregatedPoint(int map, double lat, double lon, long tstamp) {
        this.map = map;
        this.lat = lat;
        this.lon = lon;
        this.tstamp = tstamp;
    }


    /**
     * Visit this point in the heat map. The visits counter is increased and the device id is stored in a Set to count
     * unique visits.
     *
     * @param device device id.
     */
    public void visit(Integer device) {
        visits++;
        deviceIds.add(device);
    }


    public int getNumberOfUniqueVisits() {
        return deviceIds.size();
    }

}
