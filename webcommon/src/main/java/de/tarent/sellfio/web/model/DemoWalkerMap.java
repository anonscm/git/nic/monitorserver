package de.tarent.sellfio.web.model;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Created by mley on 20.02.15.
 */
@Data
public class DemoWalkerMap {

    private String name;
    private int min;
    private int max;

    private List<List<RoutePoint>> standingBy = new Vector<>();

    private List<List<RoutePoint>> walking = new Vector<>();

    private Map<String, Customer> customers = new HashMap<>();


}
