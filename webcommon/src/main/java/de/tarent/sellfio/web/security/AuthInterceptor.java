package de.tarent.sellfio.web.security;

import org.osiam.resources.scim.Role;
import org.osiam.resources.scim.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by mley on 14.10.14.
 */
@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {


    public static final String OSIAM_USER = "OSIAM_USER";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";

    @Value("${osiam.enabled}")
    private boolean osiamEnabled;


    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws IOException {
        if (handler instanceof HandlerMethod) {
            final HandlerMethod hm = (HandlerMethod) handler;

            final RolesAllowed ra = hm.getMethod().getAnnotation(RolesAllowed.class);
            if (ra != null) {
                final String[] roles = ra.value();
                final User user = (User) request.getSession().getAttribute(OSIAM_USER);

                if (userHasRole(user, roles)) {
                    return true;
                }

                return fail(response, 401, "Not authorized");
            }

            // no annotation on method
            return true;
        }

        // static resources
        return true;

    }

    private boolean fail(HttpServletResponse response, int status, String message) throws IOException {
        response.sendError(status, message);
        return false;
    }

    /**
     * Test if user has one of allowed roles
     *
     * @param user         user object
     * @param allowedRoles list of allowed roles
     * @return true if user has at least one of the allowed roles
     */
    public boolean userHasRole(User user, String[] allowedRoles) {
        if (!osiamEnabled) {
            return true;
        }

        Arrays.sort(allowedRoles);

        if (user == null) {
            return false;
        }

        if (Arrays.binarySearch(allowedRoles, "") >= 0) {
            // empty role, just login required
            return true;
        }

        for (Role role : user.getRoles()) {
            if (Arrays.binarySearch(allowedRoles, role.getDisplay()) >= 0) {
                return true;
            }
        }

        return false;
    }
}
