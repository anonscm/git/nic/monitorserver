package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.RoutePoint;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * DAO for demo routes
 */
@Repository
@Transactional("txName")
public class DemoRouteDao {

    @Autowired
    protected SessionFactory sessionFactory;

    /**
     * Save demo walker routes.
     * @param routes Array of array of route points.
     */
    @Transactional(readOnly = false)
    public void saveRoutes(RoutePoint[][] routes) {
        if(routes == null || routes.length == 0 || routes[0].length == 0) {
            return;
        }

        final String map = routes[0][0].getMap();

        // remove old entries first
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(RoutePoint.DELETE_MAP);
        query.setString("map", map);
        query.executeUpdate();

        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();

        for(RoutePoint[] route : routes) {
            for(RoutePoint rp : route) {
                rp.setId(0);
                sessionFactory.getCurrentSession().persist(rp);
            }
        }
    }

    /**
     * Get all demo routes for a map.
     * @param map map name
     * @return List of List of RoutePoint
     */
    public List<List<RoutePoint>> getRoutes(String map) {
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(RoutePoint.ORDERED);
        query.setString("map", map);
        final List<List<RoutePoint>> result = new ArrayList<>();
        final List<RoutePoint> routePoints = query.list();

        int routeId = 0;
        List<RoutePoint> route = new ArrayList<>();
        for(RoutePoint rp : routePoints) {
            if(routeId != rp.getRouteId()) {
                if(!route.isEmpty()) {
                    result.add(route);
                }
                route = new ArrayList<>();
                routeId = rp.getRouteId();
            }
            route.add(rp);
        }
        if(!route.isEmpty()) {
            result.add(route);
        }

        return result;
    }
}
