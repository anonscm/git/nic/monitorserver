package de.tarent.sellfio.web.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * A NetworkNode represents one specific Wifi-network on one specific AP.
 */
@Data
@Entity
@Table(name = "networknode")
public class NetworkNode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;


    @OneToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "transmitter_id")
    private Transmitter transmitter;

    private String macAddress; // = BSSID

    private String name; // = the ssid of the network.

    private int channelNumber;

    private double band; // frequency band

    public NetworkNode() {
    }

    public NetworkNode(Transmitter t) {
        this.transmitter = t;
        this.macAddress = t.getIdentifier();
    }

}
