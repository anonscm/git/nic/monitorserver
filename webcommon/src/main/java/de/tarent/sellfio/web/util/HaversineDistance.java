package de.tarent.sellfio.web.util;

import de.tarent.sellfio.web.model.InvioGeoPoint;

/**
 * TODO: Get rid of this class, as InvioGeoPoint already implements haversine distance
 *
 * An equation in navigation to calculate the distance between two points on a sphere based on their latitude and
 * longitude.
 * <br/>
 * <i>(See: http://en.wikipedia.org/wiki/Haversine_formula)</i>
 */
public final class HaversineDistance {
    /**
     * Private constructor.
     */
    private HaversineDistance() {
    }

    /**
     * Calculate the distance between to coordinates.
     *
     * @param p1 first point [lat,lng]
     * @param p2 second point [lat, lng]
     * @return distance in meters
     */
    public static double calcDistance(final double[] p1, final double[] p2) {
        // http://www.movable-type.co.uk/scripts/latlong.html

        final InvioGeoPoint gp1 = new InvioGeoPoint(p1[0], p1[1]);
        final InvioGeoPoint gp2 = new InvioGeoPoint(p2[0], p2[1]);

        return gp1.calculateDistanceTo(gp2);
    }

    /**
     * Calculates the distance between two points
     * @param lat1 latitude point a
     * @param lon1 longitude point a
     * @param lat2 latitude point b
     * @param lon2 longitude point b
     * @return the distance in meters
     */
    public static double calcDistance(final double lat1, final double lon1, final double lat2, final double lon2) {

        final InvioGeoPoint gp1 = new InvioGeoPoint(lat1, lon1);
        final InvioGeoPoint gp2 = new InvioGeoPoint(lat2, lon2);

        return gp1.calculateDistanceTo(gp2);
    }

    /**
     * Calculate the distance from a list of points.
     *
     * @param route array of [lat,lng]
     * @return distance in meters
     */
    public static double calcRouteDistance(final double[][] route) {
        double distance = 0;

        for (int i = 0; i < route.length - 1; i++) {
            final double d = calcDistance(route[i], route[i + 1]);
            distance += d;
        }

        return distance;
    }
}
