package de.tarent.sellfio.web.dao;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.Histogram;
import de.tarent.sellfio.web.model.InvioGeoPoint;
import org.apache.log4j.Logger;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Utility methods to convert Fingerprint objects from/to JSON etc.
 */
public final class FingerprintDAOUtils {

    private static final Logger LOGGER = Logger.getLogger(FingerprintDAOUtils.class);


    // We don't want any instances of this utility class.
    private FingerprintDAOUtils() {
    }

    ;


    /**
     * Transform a JSON String to a List of Fingerprints.
     *
     * @param json JSON String
     * @return a List of Fingerprints; will be empty if the json is invalid or doesn't contain any fingerprints.
     */
    public static List<Fingerprint> fromJson(final String json) { //NOSONAR
        final Type genericFingerprintArrayListType = new TypeToken<ArrayList<Fingerprint>>() {
        }.getType();
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(InvioGeoPoint.class, new JsonDeserializer<InvioGeoPoint>() {
            @Override
            public InvioGeoPoint deserialize(final JsonElement jsonElement,
                                             final Type type,
                                             final JsonDeserializationContext jsonDeserializationContext) {

                final JsonObject jsonObject = jsonElement.getAsJsonObject();
                return new InvioGeoPoint(jsonObject.get("latitude").getAsDouble(),
                        jsonObject.get("longitude").getAsDouble());
            }
        });

        gsonBuilder.registerTypeAdapter(Histogram.class, new JsonDeserializer<Histogram>() {
            @Override
            public Histogram deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                    throws JsonParseException {
                final Histogram h = new Histogram();
                final JsonObject j = json.getAsJsonObject();
                final Set<Map.Entry<String, JsonElement>> entries = j.entrySet();
                for (Map.Entry<String, JsonElement> entry : entries) {
                    if ("id".equals(entry.getKey())) {
                        h.setId(entry.getValue().getAsString());
                    } else {
                        final Map<Integer, Float> v = new HashMap<Integer, Float>();
                        final JsonObject o = entry.getValue().getAsJsonObject();
                        for(Map.Entry<String, JsonElement> hvalues : o.entrySet()) {
                            v.put(Integer.parseInt(hvalues.getKey()), hvalues.getValue().getAsFloat());
                        }
                        h.put(entry.getKey(), v);
                    }
                }

                return h;
            }
        });
        List<Fingerprint> fingerprints = new ArrayList<>();
        try {
            fingerprints = gsonBuilder.create().fromJson(json, genericFingerprintArrayListType);
        } catch (final JsonSyntaxException e) {
            // Don't crash on broken input. Just leave the list empty.
            LOGGER.warn("JsonSyntaxException for input '" + json + "': ", e);
        }

        if (fingerprints == null) {
            LOGGER.warn("No fingerprints in input '" + json + "' to FingerprintDAOUtils.fromJson.");
            fingerprints = new ArrayList<>();
        }

        return fingerprints;
    }


    /**
     * Transform a List of fingerprints into a JSON String.
     *
     * @param fingerprints List of Fingerprints
     * @return a JSON String
     */
    public static String toJson(final List<Fingerprint> fingerprints) {
        if (fingerprints == null || fingerprints.isEmpty()) {
            return "[]";
        }

        final Gson gson = new Gson();
        return gson.toJson(fingerprints);
    }

    /**
     * Go through a List of Fingerprints and convert all probabilities to equivalent counts.
     * This is used for backwards compatibility in order to still use old fingerprint data.
     *
     * @param fingerprints the List, which will be modified in place
     */
    public static void convertProbabilitiesToCounts(final List<Fingerprint> fingerprints) {
        for (Fingerprint fingerprint : fingerprints) {
            int maxScanCount = 0;
            for (Map<Integer, Float> transmitter : fingerprint.getHistogram().values()) {
                // See which number of scans could have produced these probabilities. (This is assumed to be the
                // number of scans where this transmitter showed up at all - the total number of scans for the whole
                // fingerprints could have been higher (-> maxScanCount).
                for (int scanCount = 1; scanCount <= 50; scanCount++) {
                    if (scanCountMatches(scanCount, transmitter)) {
                        if (scanCount > maxScanCount) {
                            maxScanCount = scanCount;
                        }

                        for (Map.Entry<Integer, Float> measurement : transmitter.entrySet()) {
                            // We use an additional factor of 100 so that we can distinguish these artificial counts
                            // from the real ones. It makes no difference for the clients because they work on the
                            // relative probabilities anyway.
                            measurement.setValue((float) Math.round(measurement.getValue() * scanCount * 100));
                        }

                        break;
                    }
                }
            }
            fingerprint.setScanCount(maxScanCount * 100);
        }
    }

    /**
     * Test whether the recorded signal strength probabilities for a transmitter could have been produced by a
     * specific number of scans.
     *
     * @param scanCount   the hypothetical number of scans to test
     * @param transmitter the map of strength->probability for the transmitter
     * @return true: yes, that could have been the scan count; false: no, that count doesn't match all probabilities.
     */
    private static boolean scanCountMatches(final int scanCount, final Map<Integer, Float> transmitter) {
        for (Float probability : transmitter.values()) {
            final float testInt = scanCount * probability;
            final float delta = Math.abs(testInt - Math.round(testInt));
            if (delta > 0.001f) {
                return false;
            }
        }

        return true;
    }

}
