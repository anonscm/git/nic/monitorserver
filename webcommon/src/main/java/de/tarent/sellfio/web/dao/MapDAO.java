package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.Map;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DAO for maps
 */
@Repository
@Transactional(value = "txName")
public class MapDAO {

    @Autowired
    protected SessionFactory sessionFactory;

    /**
     * Save a map in the database.
     *
     * @param map the map to persist
     */
    public void store(final Map map) {
        sessionFactory.getCurrentSession().persist(map);
    }

    /**
     * Update a map in the database
     * @param map Map object to be updated.
     */
    public void update(final Map map) {
        sessionFactory.getCurrentSession().update(map);
    }


    /**
     * Load a map, identified by its name, from the database.
     *
     * @param name the name of the map we're looking for
     * @return the Map entity
     */
    public Map getByName(final String name) {
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Map.BY_NAME);
        query.setString("name", name);

        final List<Map> result = (List<Map>) query.list();

        if (result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }


    /**
     * Load a map, identified by its (DB-)id, from the database.
     *
     * @param id the id of the map we're looking for
     * @return the map entity
     */
    public Map getById(final Integer id) {
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Map.BY_ID);
        query.setInteger("id", id);

        final List<Map> result = (List<Map>) query.list();

        if (result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }





}
