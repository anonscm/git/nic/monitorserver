package de.tarent.sellfio.web.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by domano on 12.11.14.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
        @NamedQuery(name = Customer.LAST_CART, query = "SELECT c FROM Customer c where c.deviceId = :deviceId " +
                "and c.map = :map and c.timestamp < :timestamp order by c.timestamp desc ")
})
@Entity
public class Customer implements Serializable {

    public static final String LAST_CART = "LAST_CART";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String deviceId;
    private String map;
    private long timestamp;
    private String state;
    private double lat;
    private double lng;
    private double trust;

    @Transient
    private double confidence;

    @OneToMany(cascade = CascadeType.ALL)
    @OrderColumn(name = "id")
    private List<CartItem> cartItems = new ArrayList<>();


    public void addCartItem(CartItem i) {
        cartItems.add(i);
    }

    /**
     * Get the sum of all items in shopping cart
     *
     * @return sum value
     */
    @Transient
    public int getSum() {
        int sum = 0;

        for (CartItem item : cartItems) {
            sum += item.getTotalPrice();
        }

        return sum;
    }

    @Transient
    public void setLocation(double[] location) {
        lat = location[0];
        lng = location[1];
    }

    @Transient
    public double[] getLocation() {
        return new double[]{lat, lng};
    }
}
