package de.tarent.sellfio.web.rest;


import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Error handler for REST controllers.
 */
@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = Logger.getLogger(ErrorHandler.class);

    /**
     * Catch all for any other exceptions.
     *
     * @param e thrown exception
     * @param r WebRequest
     * @return Response
     */
    @ExceptionHandler({Exception.class})
    @ResponseBody
    public ResponseEntity<Object> handleAllException(Exception e, WebRequest r) {
        LOG.error("Internal Server Error: ", e);
        return super.handleException(e, r);
    }


}
