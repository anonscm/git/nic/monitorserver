package de.tarent.sellfio.web.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;


/**
 * An InvioGeoPoint is basically just a latitude/longitude tuple.
 */
@Embeddable
public class InvioGeoPoint implements Comparable<InvioGeoPoint> {


    /**
     * The compatibility scale to calculate the old and obsolete E6 format.
     */
    public static final long COMPATIBILITY_SCALE = (long)1E6;

    /**
     * The static distance in meters between two lines of latitude.
     */
    protected static final double LATITUDE_LINE_DISTANCE = 111325; //111325

    /**
     * Three constant values are used in the calculation because of the earth's irregular (not completely spherical)
     * shape.
     * <p/>
     * Calculation source: http://www.nymanz.org/sandcollection/swaplist/Latitude%20and%20Longitude.pdf
     */
    private static final double[] EARTH_SHAPE_CONSTANTS = {111412.84, -93.5, 0.118};


    @Column(name="lat")
    private double latitude;

    @Column(name="lon")
    private double longitude;



    /**
     * Construct a new InvioGeoPoint at (0,0).
     */
    public InvioGeoPoint() {
        latitude = 0.0;
        longitude = 0.0;
    }



    /**
     * @deprecated we should switch to the higher precision E10 format. This is just for backwards compatibility.
     *
     * Construct a new InvioGeoPoint from individual E6 coordinates.
     *
     * @param latitudeE6  the latitude in E6 format
     * @param longitudeE6 the longitude in E6 format
     */
    public InvioGeoPoint(final int latitudeE6, final int longitudeE6) {
        latitude = ((double)latitudeE6) / COMPATIBILITY_SCALE;
        longitude = ((double)longitudeE6) / COMPATIBILITY_SCALE;
    }


    /**
     * Constructor.
     *
     * @param latitude  the latitude in degrees format (-90 to 90)
     * @param longitude the longitude in degrees format (-180 to 180)
     */
    public InvioGeoPoint(final double latitude, final double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public InvioGeoPoint(final InvioGeoPoint point) {
        latitude = point.getLatitude();
        longitude = point.getLongitude();
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(final double lat) {
        latitude = lat;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(final double lon) {
        longitude = lon;
    }


    public int getLatitudeE6() {
        return (int)(latitude * COMPATIBILITY_SCALE);
    }

    public void setLatitudeE6(final int lat) {
        latitude = ((double)lat) / COMPATIBILITY_SCALE;
    }

    public int getLongitudeE6() {
        return (int)(longitude * COMPATIBILITY_SCALE);
    }

    public void setLongitudeE6(final int lon) {
        longitude = ((double)lon) / COMPATIBILITY_SCALE;
    }


    /**
     * Converts the longitude into an x value that is better understood by the tracker. This x value represents the
     * distance from Longitude 0 to the GeoPoints location as a Longitude value. This will also prevent
     * problems because the world is round, not flat and therefore cannot be easily split into even squares.
     *
     * NB: This is not part of the public API anymore and will disappear soon
     *
     * @return the x position on map, measured in meters
     */
    @Deprecated
    public double getX() {
        return longitude * getLongitudeDistanceInMeters();
    }

    /**
     * Converts the latitude into an y value that is better understood by the tracker. This y value represents the
     * distance from Latitude 0 to the GeoPoints location as a Latitude value. This will also prevent
     * problems because the world is round, not flat and therefore cannot be easily split into even squares.
     *
     * NB: This is not part of the public API anymore and will disappear soon
     *
     * @return the y position on the map, measured in meters
     */
    @Deprecated
    public double getY() {
        return latitude * LATITUDE_LINE_DISTANCE;
    }


    public double calculateDistanceTo(final InvioGeoPoint that) {
        // http://www.movable-type.co.uk/scripts/latlong.html
        final double phi1 = this.getLatitudeInRadians();
        final double phi2 = that.getLatitudeInRadians();
        final double deltaPhi = Math.toRadians(that.getLatitude() - this.getLatitude());
        final double deltaLambda = Math.toRadians(that.getLongitude() - this.getLongitude());

        final double sinHalfDeltaPhi = Math.sin(deltaPhi / 2);
        final double sinHalfDeltaLambda = Math.sin(deltaLambda / 2);

        final double a = sinHalfDeltaPhi * sinHalfDeltaPhi +
                Math.cos(phi1) * Math.cos(phi2) *
                        sinHalfDeltaLambda * sinHalfDeltaLambda;
        final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return 6371000.0 * c; // EARTH_RADIUS
    }


    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof InvioGeoPoint)) {
            return false;
        }
        final InvioGeoPoint invioGeoPoint = (InvioGeoPoint) o;

        return ((getY() == invioGeoPoint.getY()) &&
                (getX() == invioGeoPoint.getX()));
    }


    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }


    @Override
    public int compareTo(final InvioGeoPoint other) {
        // We define an arbitrary order based on the coordinates, in order to be consistent
        // with equals (i.e. compareTo should not return 0 when equals returns false).
        if (latitude > other.getLatitude()) {
            return 1;
        }
        if (latitude < other.getLatitude()) {
            return -1;
        }
        if (longitude > other.getLongitude()) {
            return 1;
        }
        if (longitude < other.getLongitude()) {
            return -1;
        }
        return 0;
    }


    @Override
    public String toString() {
        return "LatLonE10: " + latitude + ", " + longitude + "; XY: " + this.getX() + ", " + this.getY();
    }


    public double getLatitudeInRadians() {
        return Math.toRadians(latitude);
    }

    public double getLongitudeInRadians() {
        return Math.toRadians(longitude);
    }


    private double getLongitudeDistanceInMeters() {
        final double latitudeInRadians = getLatitudeInRadians();

        final double longitudeDistanceInMeters = (EARTH_SHAPE_CONSTANTS[0] * Math.cos(latitudeInRadians)) +
                (EARTH_SHAPE_CONSTANTS[1] * Math.cos(3 * latitudeInRadians)) +
                (EARTH_SHAPE_CONSTANTS[2] * Math.cos(5 * latitudeInRadians));

        return longitudeDistanceInMeters;
    }

}
