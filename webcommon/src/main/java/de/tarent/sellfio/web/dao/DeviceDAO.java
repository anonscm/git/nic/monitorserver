package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.Device;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DAO for devices
 */
@Repository
public class DeviceDAO {

    @Autowired
    protected SessionFactory sessionFactory;


    /**
     * Save a device in the database.
     *
     * @param device Device to persist
     */
    @Transactional(value = "txName", readOnly = false)
    public void store(final Device device) {
        sessionFactory.getCurrentSession().persist(device);
    }


    /**
     * Load a device, identified by its name, from the database.
     * @param name the name of the device we're looking for
     * @return the Device entity
     */
    @Transactional(value = "txName", readOnly = true)
    public Device getByName(final String name) {
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Device.BY_NAME);
        query.setString("name", name);

        final List<Device> result = (List<Device>) query.list();

        if (result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }


    /**
     * Load a device, identified by its (DB-)id, from the database.
     * @param id the name of the device we're looking for
     * @return the Device entity
     */
    @Transactional(value = "txName", readOnly = true)
    public Device getById(final Integer id) {
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Device.BY_ID);
        query.setInteger("id", id);

        final List<Device> result = (List<Device>) query.list();

        if (result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }

}
