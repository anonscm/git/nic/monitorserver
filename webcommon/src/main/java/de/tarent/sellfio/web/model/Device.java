package de.tarent.sellfio.web.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Data
@NamedQueries({
        @NamedQuery(name = Device.BY_NAME, query = "SELECT d FROM Device d WHERE d.name = :name"),
        @NamedQuery(name = Device.BY_ID, query = "SELECT d FROM Device d WHERE d.id = :id")
})public class Device {

    public static final String BY_NAME = "Device.BY_NAME";
    public static final String BY_ID = "Device.BY_ID";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // This is called deviceId in the app.
    private String name;


    public Device() {
    }

    public Device(String name) {
        this.name = name;
    }

}
