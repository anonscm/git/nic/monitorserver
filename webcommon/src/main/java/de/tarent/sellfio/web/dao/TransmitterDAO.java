package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.BtleTransmitter;
import de.tarent.sellfio.web.model.Map;
import de.tarent.sellfio.web.model.NetworkNode;
import de.tarent.sellfio.web.model.Transmitter;
import de.tarent.sellfio.web.model.WifiAp;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mley on 22.10.15.
 */
@Repository
@Transactional("txName")
public class TransmitterDAO extends AbstractDAO {

    public static final String BTLE = "btle";

    //TODO: This SQL query should take into account the transmitter id. This is called from Monitor-localization setup
    private static final String UPDATE_TRANSMITTER_STATUS =
            "update transmitter " +
                    "set active = :active " +
                    "where map_id = :mapId " +
                    "      and :identifier = identifier";

    private static final String MAP_ID = "map_id";

    @Autowired
    private MapDAO mapDAO;

    /**
     * Get all transmitters of one type for a map
     *
     * @param map  the name of the map
     * @param type type of transmitter
     * @return List of transmitters
     */
    public List<Transmitter> getTransmitters(final String map, final String type) {
        final Map m = mapDAO.getByName(map);
        if (m == null) {
            return new ArrayList<>();
        }

        final Query namedQuery = sessionFactory.getCurrentSession().getNamedQuery(Transmitter.GET_BY_MAP_AND_TYPE);
        namedQuery.setInteger(MAP_ID, m.getId());
        namedQuery.setString("transmitter_type", type);

        return namedQuery.list();

    }

    /**
     * Saves a new transmitter in the database. Might be only used for testing.
     *
     * @param t the transmitter
     */
    public void save(Transmitter t) {
        sessionFactory.getCurrentSession().persist(t);
    }

    /**
     * Stores a BtleTransmitter.
     *
     * @param b the transmitter.
     */
    public void save(BtleTransmitter b) {
        if (b.getTransmitter().getId() == null) {
            save(b.getTransmitter());
        } else {
            // entity must be re-attached to session
            sessionFactory.getCurrentSession().merge(b.getTransmitter());
        }
        sessionFactory.getCurrentSession().persist(b);
    }

    /**
     * Stores a WifiAp.
     *
     * @param w the WifiAp.
     */
    public void save(WifiAp w) {
        for (NetworkNode n : w.getNetworks()) {
            if (n.getTransmitter().getId() == null) {
                save(n.getTransmitter());
            } else {
                sessionFactory.getCurrentSession().merge(n.getTransmitter());
            }
            if (n.getId() == null) {
                sessionFactory.getCurrentSession().persist(n);
            }

        }
        sessionFactory.getCurrentSession().persist(w);
    }

    /**
     * Removes all BtleTransmitters and WifiAps of a map from the DB.
     * @param map name of the map
     */
    public void removeAllSignalSources(String map) {
        final Map m = mapDAO.getByName(map);
        if (m == null) {
            return;
        }
        final String[] queries = new String[]{"delete from btletransmitter where transmitter_id in " +
                "(select id from transmitter where transmitter_type = 'btle' and map_id = :mapId )",
                "delete from wifiap where map_id = :mapId "};
        for (String q : queries) {
            final SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(q);
            sqlQuery.setInteger("mapId", m.getId());
            sqlQuery.executeUpdate();
        }
        sessionFactory.getCurrentSession().clear();

    }

    /**
     * Gets all BtleTransmitters of a map.
     *
     * @param mapName the name of the map.
     * @return List of BtleTransmitter objects.
     */
    public List<BtleTransmitter> getIBeacons(String mapName) {
        final Map map = mapDAO.getByName(mapName);
        if (map == null) {
            return null;
        }

        final Query query = sessionFactory.getCurrentSession().getNamedQuery("btle_by_map");
        query.setInteger(MAP_ID, map.getId());

        return query.list();
    }

    /**
     * Gets all WifiAps of a map.
     *
     * @param mapName name of the map.
     * @return List of WifiAp objects.
     */
    public List<WifiAp> getWifiAps(String mapName) {
        final Map map = mapDAO.getByName(mapName);
        if (map == null) {
            return null;
        }

        final Query query = sessionFactory.getCurrentSession().getNamedQuery("wifi_by_map");
        query.setInteger(MAP_ID, map.getId());

        return query.list();
    }

    /**
     * Get transmitter id by MapId, type and identifier.
     *
     * @param mapId           id of the map
     * @param transmitterType 'wifi' or 'btle'
     * @param identifier      mac or uuid
     * @return the transmitter id
     */
    public Transmitter getTransmitterByMapIdTypeIdentifier(final long mapId,
                                                           final String transmitterType,
                                                           final String identifier) {

        final Query namedQuery = sessionFactory.getCurrentSession()
                .getNamedQuery(Transmitter.TRANSMITTER_BY_MAP_TYPE_IDENTIFIER);
        namedQuery.setLong(MAP_ID, mapId);
        namedQuery.setString("transmitter_type", transmitterType);
        namedQuery.setString("identifier", identifier);

        final List<Transmitter> result = (List<Transmitter>) namedQuery.list();
        if (result.size() == 1) {
            return result.get(0);
        }

        return null;
    }


    /**
     * Activate the transmitter "transmitterIdentifier" from map "mapName".
     *
     * @param mapName    Name of the map
     * @param identifier Identifier of the transmitter
     * @param active     true: transmitter will be activated; false: transmitter will be deactivated
     * @return false: the transmitter was not found;
     * true: the transmitter was found and updated (unless it already had the same value).
     */
    @Transactional(value = "txName", readOnly = false)
    public boolean updateTransmitter(final String mapName, final String identifier, final boolean active) {
        // Get mapId from mapName.
        final Map map = mapDAO.getByName(mapName);
        if (map == null) {
            return false;
        }

        final Query query = sessionFactory.getCurrentSession().createSQLQuery(UPDATE_TRANSMITTER_STATUS);
        query.setLong("mapId", map.getId());
        query.setString("identifier", identifier);
        query.setBoolean("active", active);
        query.executeUpdate();
        return true;
    }


    /**
     * Get a list of identifiers of transmitters by map name, transmitterType.
     *
     * @param mapName               name of the map
     * @param transmitterType       'wifi' or 'btle'
     * @param onlyActiveTransmitter Flag to set if we want all transmitters or only those active
     * @return a list of identifiers of transmitters
     */
    public List<String> getTransmitterIdsByMapAndType(final String mapName,
                                                      final String transmitterType,
                                                      final boolean onlyActiveTransmitter) {
        final List<String> result = new ArrayList<>();
        final List<Transmitter> transmitters = getTransmitters(mapName, transmitterType);
        for (Transmitter t : transmitters) {
            if (onlyActiveTransmitter) {
                if (t.isActive()) {
                    result.add(t.getIdentifier());
                }
            } else {
                result.add(t.getIdentifier());
            }
        }


        return result;
    }


}
