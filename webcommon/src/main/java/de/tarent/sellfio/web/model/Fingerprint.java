package de.tarent.sellfio.web.model;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * A Fingerprint is a wifi Histogram at a specific position.
 */
@Data
@AllArgsConstructor
public class Fingerprint {

    /**
     * The histogram that was measured at some point.
     */
    private Histogram histogram;

    /**
     * The point where the histogram was measured.
     */
    private InvioGeoPoint point;

    /**
     * An ID, for display, ordering, etc.
     */
    private String id;

    private int scanCount;

    private int scanTime;

    public Fingerprint(Histogram histogram, InvioGeoPoint invioGeoPoint, int scanCount, int scanTime) {
        this.histogram = histogram;
        this.point = invioGeoPoint;
        this.scanCount = scanCount;
        this.scanTime = scanTime;

    }

    public Fingerprint(Histogram h, InvioGeoPoint p ) {
        this.histogram = h;
        this.point = p;
    }



}
