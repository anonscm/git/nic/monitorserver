package de.tarent.sellfio.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Created by mley on 17.02.15.
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@NamedQueries(value = {
        @NamedQuery(name = RoutePoint.ORDERED, query = "select r from RoutePoint r where r.map = :map order by routeId, pointInRoute"),
        @NamedQuery(name = RoutePoint.DELETE_MAP, query = "delete from RoutePoint r where r.map = :map")

})
public class RoutePoint {

    public static final String ORDERED = "ORDERED";
    public static final String DELETE_MAP = "DELETE_MAP";



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int routeId;
    private int pointInRoute;
    private String map;
    private String name;
    private String deviceId;
    private double lat;
    private double lng;
    private int status;
    private int product;
}
