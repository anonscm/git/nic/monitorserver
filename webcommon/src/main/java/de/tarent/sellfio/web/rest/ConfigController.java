package de.tarent.sellfio.web.rest;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mley on 11.09.14.
 */

@RestController
@RequestMapping("/config")
public class ConfigController {

    @Value("${map.baseurl}")
    private String baseUrl;

    @Value("${map.defaultMap}")
    private String defaultMap;

    /**
     * Gets the configuration for the monitor application
     *
     * @return Map of configuration items.
     */
    @RolesAllowed("")
    @RequestMapping(method = RequestMethod.GET)
    public Map<String, Object> getConfig()  {
        final Map<String, Object> m = new HashMap<>();
        m.put("mapUrl", baseUrl);
        m.put("selectedMap", defaultMap);
        return m;
    }


}
