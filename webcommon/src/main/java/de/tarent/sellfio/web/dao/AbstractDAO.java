package de.tarent.sellfio.web.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


/**
 * Base class for our DAOs which holds the SessionFactory.
 *
 * TODO: isn't this a bit too much...?
 */
@Transactional
public abstract class AbstractDAO {

    @Autowired
    protected SessionFactory sessionFactory;


}
