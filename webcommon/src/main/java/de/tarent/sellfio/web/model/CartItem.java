package de.tarent.sellfio.web.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by mley on 12.09.14.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "cartitem")
public class CartItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    private long cartId;

    private String name;
    private int articleId;
    private String ean;
    private int unitPrice;
    private int amount;

    public CartItem() {

    }

    public CartItem(String n, int a, int u) {
        this.name = n;
        this.amount = a;
        this.unitPrice = u;
    }

    /**
     * Returns the gross price of the item(s).
     *
     * @return gross price
     */
    public int getTotalPrice() {
        return unitPrice * amount;
    }

    /**
     * "Sets" the total value. Needed for proper deserialization. This method has no effect.
     *
     * @param totalPrice the total price. Value is ignored.
     */
    public void setTotalPrice(int totalPrice) {
        //NOSONAR setter value is ignored.
    }

}
