package de.tarent.sellfio.web.security;

import org.osiam.client.OsiamConnector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by mley on 14.10.14.
 */
@Component
public class OsiamSpring {

    @Value("${osiam.endpoint}")
    private String endpoint;

    @Value("${osiam.clientId}")
    private String clientId;

    @Value("${osiam.clientSecret}")
    private String clientSecret;

    @Value("${osiam.clientRedirectUri}")
    private String clientRedirectUri;

    /**
     * Gets an OsiamConnector.Builder with clientId, clientSecret and endpoint set
     *
     * @return OsiamConnector.Builder object
     */
    public OsiamConnector getConnector() {
        return new OsiamConnector.Builder()
                .setEndpoint(endpoint)
                .setClientId(clientId)
                .setClientSecret(clientSecret)
                .setClientRedirectUri(clientRedirectUri)
                .build();

    }



}
