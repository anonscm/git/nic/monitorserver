package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.Histogram;
import de.tarent.sellfio.web.model.InvioGeoPoint;
import de.tarent.sellfio.web.model.Transmitter;
import org.hibernate.Query;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * This class manages the actions with the database related to Fingerprints and Transmitters and Histograms and
 * Measurements and Maps...
 * TODO: should maybe be multiple classes? Or we should use more JPA-abstractions?
 */
@Repository
// SupressWarnings:
// unchecked: we don't have typed queries, so we can only get List :(
// AvoidDuplicateLiterals: The SQL is more readable with pure literals. (Scheint leider nicht zu wirken!)
@SuppressWarnings({"unchecked", "PMD:AvoidDuplicateLiterals"})
public class FingerprintDAO extends AbstractDAO {


    private static final String FINGERPRINTS_BY_MAP_AND_TYPE_WITH_ACTIVETRANSMITTER =
            "select distinct f.id, f.map_id, f.name, f.lat, f.lon, fingerprint_type, f.scan_count, f.scan_time  " +
                    "from fingerprint f " +
                    "     inner join histogram h on h.fingerprint_id = f.id " +
                    "     inner join measurement m on m.histogram_id = h.id " + // NOPMD AvoidDuplicateLiterals
                    "     inner join transmitter t on t.id = h.transmitter_id " +
                    "where f.fingerprint_type = :fingerprintType " +
                    "      and t.active = true " +
                    "      and f.map_id = :mapId " +
                    "ORDER by f.name";


    private static final String FINGERPRINTS_BY_MAP_AND_TYPE =
            "select distinct f.id, f.map_id, f.name, f.lat, f.lon, fingerprint_type, f.scan_count, f.scan_time " +
                    "from fingerprint f " +
                    "     inner join histogram h on h.fingerprint_id = f.id " +
                    "     inner join measurement m on m.histogram_id = h.id " +
                    "where f.fingerprint_type = :fingerprintType " +
                    "      and f.map_id = :mapId " +
                    "ORDER by f.name";


    private static final String HISTOGRAMS_BY_MAP_AND_FINGERPRINT_WITH_ACTIVETRANSMITTER =
            "select t.identifier, " +
                    "       string_agg( cast(m.strength as text) || ',' || cast(m.probability as text), ';') " +
                    "from transmitter t " +
                    "     inner join histogram h on h.transmitter_id = t.id " +
                    "     inner join measurement m on m.histogram_id = h.id " +
                    "where h.fingerprint_id = :fingerprintId " +
                    "      and t.active = true " +
                    "      and t.map_id = :mapId " +
                    "group by t.identifier";


    private static final String HISTOGRAMS_BY_MAP_AND_FINGERPRINT =
            "select t.identifier, " +
                    "       string_agg( cast(m.strength as text) || ',' || cast(m.probability as text), ';') " +
                    "from transmitter t " +
                    "     inner join histogram h on h.transmitter_id = t.id " +
                    "     inner join measurement m on m.histogram_id = h.id " +
                    "where h.fingerprint_id = :fingerprintId " +
                    "      and t.map_id = :mapId " +
                    "group by t.identifier";


    private static final String DELETE_FINGERPRINTS_BY_MAPID =
            "delete from fingerprint " +
                    "where map_id = :mapId " +
                    "      and cast(fingerprint_type as text) = :fingerprintType";


    private static final String INSERT_FINGERPRINT =
            "insert into fingerprint (map_id, name, lat, lon, fingerprint_type, scan_count, scan_time) " +
                    "values (?, ?, ?, ?, ?, ?, ?) ";


    private static final String INSERT_HISTOGRAM =
            "insert into histogram (transmitter_id, fingerprint_id) " +
                    "values (?, ?)";


    private static final String INSERT_MEASUREMENT =
            "insert into measurement (histogram_id, strength, probability) " +
                    "values (:histogramId, :strength, :probability)";


    @Autowired
    private MapDAO mapDAO;

    @Autowired
    private TransmitterDAO transmitterDAO;

    /**
     * Get the Fingerprints of one type, for one map.
     *
     * @param mapName               Name of the map
     * @param fingerprintType       'wifi' or 'btle'
     * @param onlyActiveTransmitter Flag to set if we want all transmitters or only those active
     * @return a list of fingerprints, possibly empty.
     */
    @Transactional(value = "txName", readOnly = true) // NOPMD AvoidDuplicateLiterals
    public List<Fingerprint> getByMapAndType(final String mapName,
                                             final String fingerprintType,
                                             final boolean onlyActiveTransmitter) {

        final Integer mapId = getMapIdByName(mapName);
        if (mapId != null) {
            return getByMapAndType(mapId, fingerprintType, onlyActiveTransmitter);
        }
        return new ArrayList<>();
    }


    /**
     * Save fingerprints into the database from a JSON String. This function is called from the controller.
     *
     * @param mapName         Name of the map
     * @param fingerprintType 'wifi' or 'btle'
     * @param json            JSON String
     * @throws FingerprintStoreException when the fingerprints can not be stored.
     */
    @Transactional(value = "txName", readOnly = false)
    public void replaceFingerprintsFromJson(final String mapName, final String fingerprintType, final String json)
            throws FingerprintStoreException {

        // Get object list from json:
        final List<Fingerprint> fingerprints = FingerprintDAOUtils.fromJson(json);

        replaceFingerprints(mapName, fingerprintType, fingerprints);
    }


    /**
     * Save fingerprints into the database.
     *
     * @param mapName         Name of the map
     * @param fingerprintType 'wifi' or 'btle'
     * @param fingerprints    the List of Fingerprints
     * @throws FingerprintStoreException when the fingerprints can not be stored.
     */
    @Transactional(value = "txName", readOnly = false)
    public void replaceFingerprints(final String mapName,
                                    final String fingerprintType,
                                    final List<Fingerprint> fingerprints)
            throws FingerprintStoreException {

        // Get mapId from mapName:
        Integer mapId = getMapIdByName(mapName);

        // If map does not exist then create a new map:
        if (mapId == null) {
            mapId = createMap(mapName);
        }

        // Delete old fingerprints by mapId:
        deleteFingerprintsByMapId(mapId, fingerprintType);

        // Insert fingerprints, transmitters, histogram, measurement:
        saveFingerprints(fingerprints, mapId, fingerprintType);
    }


    private Integer getMapIdByName(String name) {
        final de.tarent.sellfio.web.model.Map map = mapDAO.getByName(name);
        if (map == null) {
            return null;
        }
        return map.getId();
    }

    private Integer createMap(String name) {
        final de.tarent.sellfio.web.model.Map map = new de.tarent.sellfio.web.model.Map(name);
        mapDAO.store(map);
        return map.getId();
    }

    /**
     * Get fingerprints filtered by map_id and fingerprint_type (wifi or btle)
     * <p/>
     * TODO: we seriously need a fingerprint-entity :-)
     * The result:
     * [0]: (integer)   id
     * [1]: (integer)   map_id
     * [2]: (String)    name
     * [3]: (double)    lat
     * [4]: (double)    lon
     * [5]: (String)    fingerprint_type ('wifi' or 'btle')
     * [6]: (integer)   scan_count
     * [7]: (integer)   scan_time
     *
     * @param mapId           id of the map
     * @param fingerprintType 'wifi' or 'btle'
     * @return return a list of fingerprint in List<Object[]>
     */
    private List getFingerPrintsByMapAndType(final long mapId,
                                             final String fingerprintType,
                                             final boolean onlyActiveTransmitter) {

        final Query query;
        if (onlyActiveTransmitter) {
            query = sessionFactory.getCurrentSession().createSQLQuery(
                    FINGERPRINTS_BY_MAP_AND_TYPE_WITH_ACTIVETRANSMITTER);
        } else {
            query = sessionFactory.getCurrentSession().createSQLQuery(
                    FINGERPRINTS_BY_MAP_AND_TYPE);
        }
        query.setString("fingerprintType", fingerprintType);
        query.setLong("mapId", mapId); // NOPMD AvoidDuplicateLiterals
        return query.list();
    }


    /**
     * Get histograms filtered by map_id and fingerprint_id
     * <p/>
     * The result:
     * [0]: (String)    identifier (z.b. "00:17:0f:e7:e8:83")
     * [1]: (String)    strengthAndProbability (z.b. "-60,0.5;-55,0.5")
     *
     * @param mapId         id of the map
     * @param fingerprintId id of the fingerprint
     * @return return a list of histograms in List<Object[]>
     */
    private List getHistogramsByMapAndFingerprint(final long mapId,
                                                  final long fingerprintId,
                                                  final boolean onlyActiveTransmitter) {

        final Query query;
        if (onlyActiveTransmitter) {
            query = sessionFactory.getCurrentSession().createSQLQuery(
                    HISTOGRAMS_BY_MAP_AND_FINGERPRINT_WITH_ACTIVETRANSMITTER);
        } else {
            query = sessionFactory.getCurrentSession().createSQLQuery(
                    HISTOGRAMS_BY_MAP_AND_FINGERPRINT);
        }

        query.setLong("fingerprintId", fingerprintId);
        query.setLong("mapId", mapId);
        return query.list();
    }


    /**
     * Parse the native list of Histogram gotten from DB into Histogram object,
     * <p/>
     * // TODO: List of Object[] is not very nice and should be replaced by something more natural.
     *
     * @param dbHistograms list of histograms like List<Object[]>
     * @param name         name of the histogram
     * @return Return a Histogram object
     */
    private Histogram parseHistogram(final List<? extends Object[]> dbHistograms, final String name) {
        final Histogram histogram = new Histogram(name);

        for (Object[] dbHistogram : dbHistograms) {
            final String identifier = ((String) dbHistogram[0]);
            final String measuresStr = ((String) dbHistogram[1]);

            final String[] strengthProbabilityStr = measuresStr.split(";");
            final Map<Integer, Float> result = new TreeMap<>();

            for (String str : strengthProbabilityStr) {
                final String[] measures = str.split(",");

                final int strength = Integer.parseInt(measures[0]);
                final float probability = Float.parseFloat(measures[1]);

                result.put(strength, probability);
            }

            histogram.put(identifier, result);
        }
        return histogram;
    }


    /**
     * Get a List of Fingerprints filtered by map_id and fingerprint_type ('wifi' or 'btle').
     * <p/>
     * TODO: replace the whole "List<? extends Object[]>"-stuff with something prettier.
     *
     * @param mapId           Id of the map
     * @param fingerprintType 'wifi' or 'btle'
     * @return Return a list of Fingerprint objects
     */
    private List<Fingerprint> getByMapAndType(final long mapId,
                                              final String fingerprintType,
                                              final boolean onlyActiveTransmitter) {

        final List<Fingerprint> fingerprints = new ArrayList<>();

        final List<? extends Object[]> dbFingerprints = (List<? extends Object[]>) getFingerPrintsByMapAndType(
                mapId, fingerprintType, onlyActiveTransmitter);

        for (Object[] dbFingerprint : dbFingerprints) {
            final long fingerprintId = ((Integer) dbFingerprint[0]).longValue();
            final String name = ((String) dbFingerprint[2]);
            final double latitude = ((Double) dbFingerprint[3]);
            final double longitude = ((Double) dbFingerprint[4]);

            int scanCount = 0;
            int scanTime = 0;
            if (dbFingerprint[6] != null) {
                scanCount = ((Integer) dbFingerprint[6]);
            }
            if (dbFingerprint[7] != null) {
                scanTime = ((Integer) dbFingerprint[7]);
            }

            final List<? extends Object[]> dbHistograms = (List<? extends Object[]>) getHistogramsByMapAndFingerprint(
                    mapId, fingerprintId, onlyActiveTransmitter);
            final Histogram histogram = parseHistogram(dbHistograms, name);

            final InvioGeoPoint invioGeoPoint = new InvioGeoPoint(latitude, longitude);
            fingerprints.add(new Fingerprint(histogram, invioGeoPoint, name, scanCount, scanTime));
        }

        return fingerprints;
    }


    /**
     * Delete Fingerprints by mapId.
     *
     * @param mapId           Id of the map
     * @param fingerprintType 'wifi' or 'btle'
     */
    private void deleteFingerprintsByMapId(final long mapId, final String fingerprintType) {
        final Query query = sessionFactory.getCurrentSession().createSQLQuery(DELETE_FINGERPRINTS_BY_MAPID);
        query.setLong("mapId", mapId);
        query.setString("fingerprintType", fingerprintType);
        query.executeUpdate();
    }


    /**
     * Insert a fingerprint into database
     *
     * @param fingerprint     Fingerprint object
     * @param mapId           Id of the map
     * @param fingerprintType 'wifi' or 'btle'
     * @return new id of the fingerprint
     */
    private long saveFingerprint(final Fingerprint fingerprint, final long mapId, final String fingerprintType)
            throws FingerprintStoreException {

        final Long id = sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<Long>() {
            @Override
            public Long execute(Connection connection) throws SQLException {
                try (final PreparedStatement query = connection.prepareStatement(INSERT_FINGERPRINT,
                        Statement.RETURN_GENERATED_KEYS)) {
                    query.setLong(1, mapId);
                    query.setString(2, fingerprint.getId());
                    query.setDouble(3, fingerprint.getPoint().getLatitude());
                    query.setDouble(4, fingerprint.getPoint().getLongitude());
                    query.setString(5, fingerprintType);
                    query.setInt(6, fingerprint.getScanCount());
                    query.setInt(7, fingerprint.getScanTime());
                    query.execute();

                    return getGeneratedIdFromStatement(query);
                }
            }
        });

        if (id == -1L) {
            throw new FingerprintStoreException("Could not save fingerprint into database.");
        }

        return id;
    }


    private long getGeneratedIdFromStatement(PreparedStatement statement) throws SQLException {
        try (final ResultSet rs = statement.getGeneratedKeys()) {
            if (rs.next()) {
                return rs.getLong(1);
            }
        }
        return -1L;
    }

    /**
     * Insert a transmitter into database.
     *
     * @param mapId           Id of the map
     * @param identifier      of the transmitter (Ex. mac for wifi)
     * @param isActive        Is a boolean to set whether it is active
     * @param transmitterType 'wifi' or 'btle'
     * @return new id of the transmitter
     */
    private long saveTransmitter(final int mapId,
                                 final String identifier,
                                 final boolean isActive,
                                 final String transmitterType) throws FingerprintStoreException {


        final Transmitter t = new Transmitter(mapId, isActive, transmitterType, identifier);
        transmitterDAO.save(t);
        return t.getId();
    }


    /**
     * Save a Histogram in the database. In the database a histogram is a relation between fingerprint and transmitter.
     *
     * @param transmitterId Id of the transmitter
     * @param fingerprintId Id of the fingerprint
     * @return new id of the histogram
     */
    private long saveHistogram(final long transmitterId, final long fingerprintId) throws FingerprintStoreException {

        final Long id = sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<Long>() {
            @Override
            public Long execute(Connection connection) throws SQLException {
                try (final PreparedStatement query = connection.prepareStatement(INSERT_HISTOGRAM,
                        Statement.RETURN_GENERATED_KEYS)) {
                    query.setLong(1, transmitterId);
                    query.setLong(2, fingerprintId);
                    query.execute();

                    return getGeneratedIdFromStatement(query);
                }
            }
        });

        if (id == -1L) {
            throw new FingerprintStoreException("Could not save histogram into database.");
        }

        return id;
    }


    /**
     * Save a Measurement in the database. (Strength + Probability).
     *
     * @param histogramId Id of the histogram
     * @param strength    Strength of the signal
     * @param probability Probability of the signal
     */
    private void saveMeasurement(final long histogramId, final long strength, final float probability) {
        final Query query = sessionFactory.getCurrentSession().createSQLQuery(INSERT_MEASUREMENT);

        query.setLong("histogramId", histogramId);
        query.setLong("strength", strength);
        query.setDouble("probability", probability);

        query.executeUpdate();
    }


    /**
     * Save a list of fingerprint objects in the database.
     *
     * @param fingerprints    List of fingerprint objects
     * @param mapId           Id of the map
     * @param fingerprintType 'wifi' or 'btle'
     * @throws FingerprintStoreException
     */
    private void saveFingerprints(final List<Fingerprint> fingerprints, final int mapId, final String fingerprintType)
            throws FingerprintStoreException {

        for (Fingerprint fingerprint : fingerprints) {
            if (fingerprint == null) {
                continue;
            }
            final long fingerprintId = saveFingerprint(fingerprint, mapId, fingerprintType);

            final Histogram histogram = fingerprint.getHistogram();
            for (Map.Entry transmitter : histogram.entrySet()) {
                final String identifier = transmitter.getKey().toString();
                final Transmitter t = transmitterDAO.getTransmitterByMapIdTypeIdentifier(mapId, fingerprintType,
                        identifier);

                Long transmitterId;
                if (t == null) {
                    transmitterId = saveTransmitter(mapId, transmitter.getKey().toString(), true, fingerprintType);
                } else {
                    transmitterId = t.getId();
                }

                final long histogramId = saveHistogram(transmitterId, fingerprintId);

                final Map<Integer, Float> measurements = (Map<Integer, Float>) transmitter.getValue();
                saveMeasurements(measurements, histogramId);
            }
        }
    }


    private void saveMeasurements(final Map<Integer, Float> measurements, final long histogramId) {
        for (Map.Entry<Integer, Float> measurement : measurements.entrySet()) {
            final Integer strength = measurement.getKey();
            final Float probability = measurement.getValue();
            saveMeasurement(histogramId, strength, probability);
        }
    }

}