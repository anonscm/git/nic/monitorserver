package de.tarent.sellfio.web.rest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mley on 04.03.15.
 */
@RestController
@RequestMapping("/version")
public class VersionController {

    private static final Logger LOG = Logger.getLogger(VersionController.class);

    private Map<String, String> version;

    @Autowired
    private Environment env;

    /**
     * Initialize version controller.
     *
     * @throws IOException if property file could be read
     */
    @PostConstruct
    public void init() throws IOException {
        final String build = env.getProperty("version.number") + "-" + env.getProperty("version.build");
        final String buildDate = env.getProperty("version.date");
        final String codeName = env.getProperty("version.codename");

        version = new HashMap<>();
        version.put("version", build);
        version.put("date", buildDate);
        version.put("codename", codeName);

        LOG.info("Server Version " + build + " Codename " + codeName +
                " built on " + buildDate);
    }

    /**
     * Returns the version of the monitor app.
     *
     * @return version string.
     */
    @RequestMapping(method = RequestMethod.GET)
    public Map<String, String> getVersion() {
        return version;
    }


}
