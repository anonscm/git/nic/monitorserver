package de.tarent.sellfio.web.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by mley on 28.10.15.
 */
@Data
@Entity
@Table(name = "transmitter")
@NamedQueries({
        @NamedQuery(name = Transmitter.GET_BY_MAP_AND_TYPE, query = "select t from Transmitter t where t.map_id = :map_id and t.transmitter_type = :transmitter_type" ),
    @NamedQuery(name= Transmitter.TRANSMITTER_BY_MAP_TYPE_IDENTIFIER, query = "select t from Transmitter t where t.map_id = :map_id and t.transmitter_type = :transmitter_type and t.identifier = :identifier")
})
public class Transmitter {

    public static final String GET_BY_MAP_AND_TYPE = "GET_BY_MAP_AND_TYPE";

    public static final String TRANSMITTER_BY_MAP_TYPE_IDENTIFIER= "TRANSMITTER_BY_MAP_TYPE_IDENTIFIER";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int map_id;

    private boolean active;

    private String transmitter_type;

    private String identifier;

    public Transmitter() {
    }

    public Transmitter(int map_id, boolean active, String type, String identifier) {
        this.map_id = map_id;
        this.active = active;
        this.transmitter_type = type;
        this.identifier = identifier;
    }

}
