package de.tarent.sellfio.web.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;


/**
 * A physical Wifi-AccessPoint (AP), which can contain any number of Virtual-Access-Points.
 */
@Data
@Entity
@Table(name = "wifiap")
@NamedQueries({@NamedQuery(name="wifi_by_map",
        query="select w from WifiAp w where w.map_id = :map_id order by w.name, w.id")})

public class WifiAp implements SignalSource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long id;
    private int map_id;
    private double lat;
    private double lon;
    private double height;
    private String vendor;
    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name="wifinetworks", joinColumns = {@JoinColumn(name="wifiap", referencedColumnName = "id")},
    inverseJoinColumns = {@JoinColumn(name="networknode", referencedColumnName = "id")})
    private List<NetworkNode> networks = new ArrayList<>();


}
