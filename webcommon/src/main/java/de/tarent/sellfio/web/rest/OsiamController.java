package de.tarent.sellfio.web.rest;


import de.tarent.sellfio.web.security.AuthInterceptor;
import de.tarent.sellfio.web.security.OsiamSpring;
import lombok.Data;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.osiam.client.OsiamConnector;
import org.osiam.client.oauth.AccessToken;
import org.osiam.client.oauth.Scope;
import org.osiam.resources.scim.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by mley on 14.10.14.
 */
@RestController
@RequestMapping("/osiam")
public class OsiamController {

    private static final Logger LOG = Logger.getLogger(OsiamController.class);

    @Value("${osiam.enabled}")
    private boolean osiamEnabled;

    @Value("${applicationUri}")
    private String applicationUri;

    @Autowired
    @Setter // for testing
    private OsiamSpring osiamSpring;

    /**
     * Test if user is logged in
     *
     * @param session HttpSession
     * @return true user object and accesstoken are stored in session object
     */
    @RequestMapping(value = "/loggedIn", method = RequestMethod.GET)
    public LoginResponse loggedIn(HttpSession session) {

        final LoginResponse response = new LoginResponse();

        // return the accesstoken if user is logged or OSIAM is disabled. If not logged in, return the redirect URI
        // to the login page.
        if (!osiamEnabled) {
            LOG.warn("OSIAM disabled. Logging in");
            response.setAccessToken("ACCESS_TOKEN");
            response.setUsername("OSIAM_disabled");

        } else if (session.getAttribute(AuthInterceptor.OSIAM_USER) != null &&
                session.getAttribute(AuthInterceptor.ACCESS_TOKEN) != null) {

            final User user = (User) session.getAttribute(AuthInterceptor.OSIAM_USER);

            LOG.debug("User logged in: " + user.getUserName());

            response.setUsername(user.getUserName());
            response.setAccessToken((String) session.getAttribute(AuthInterceptor.ACCESS_TOKEN));

        } else {
            LOG.debug("User not logged in. Redirecting to login page.");
            final String redirectUri = osiamSpring.getConnector().getAuthorizationUri(Scope.ME).toString();
            response.setRedirectUri(redirectUri);

        }

        return response;
    }

    /**
     * Handles the last part of the 3-legged OAuth authorization. This resource is called by the browser after the user
     * logged in successfully. With the auth code, the acccesstoken can be retrieved. If that succeeded, a redirect back
     * to the application is made.
     *
     * @param session  the HTTP session object
     * @param response the HttpServletResponse
     * @param authCode the auth-code from OSIAM auth-server
     * @param state    the state field (currently unused)
     * @throws IOException if sendRedirect fails
     */
    @RequestMapping(value = "/oauth2", method = RequestMethod.GET)
    public void oauth2(HttpSession session, HttpServletResponse response, @RequestParam("code") String authCode,
                       @RequestParam(value = "state", required = false) String state) throws IOException {
        final OsiamConnector oc = osiamSpring.getConnector();

        final AccessToken at = oc.retrieveAccessToken(authCode);
        final User currentUser = oc.getCurrentUser(at);

        session.setAttribute(AuthInterceptor.OSIAM_USER, currentUser);
        session.setAttribute(AuthInterceptor.ACCESS_TOKEN, at.getToken());

        response.sendRedirect(applicationUri);

    }


    /**
     * Logs the logged-in user out.
     *
     * @param session HttpSession object
     * @return true
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public boolean logout(HttpSession session) {
        final String accessToken = (String) session.getAttribute(AuthInterceptor.ACCESS_TOKEN);
        final User user = (User) session.getAttribute(AuthInterceptor.OSIAM_USER);
        if (accessToken != null) {
            osiamSpring.getConnector().revokeAllAccessTokens(user.getId(),
                    new AccessToken.Builder(accessToken).build());
        }

        session.removeAttribute(AuthInterceptor.OSIAM_USER);
        session.removeAttribute(AuthInterceptor.ACCESS_TOKEN);
        session.invalidate();

        return true;
    }

    /**
     * Simple Response class for loggedIn method.
     */
    @Data
    private static class LoginResponse {
        private String accessToken;
        private String username; //NOSONAR lombok snafu
        private String redirectUri;
    }
}
