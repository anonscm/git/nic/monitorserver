package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.Point;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * DAO for points
 */
@Repository
@Transactional(value = "txName")
public class PointDAO {

    @Autowired
    protected SessionFactory sessionFactory;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private MapService mapService;


    /**
     * Save a point in the DB. The ID for map and device will be filled in by this method.
     *
     * @param point      Point to persist
     * @param mapName    the name of the map for which this point was reported
     * @param deviceName the name of the device ("deviceID") which reported this point
     */
    public void store(final Point point, final String mapName, final String deviceName) {

        final Integer device = deviceService.mapNameToId(deviceName);
        final Integer map = mapService.mapNameToId(mapName);
        // neither device nor map can be null

        point.setDevice(device);
        point.setMap(map);

        sessionFactory.getCurrentSession().persist(point);
    }

    /**
     * Save a point in the DB.
     *
     * @param point Point to persist
     */
    public void store(final Point point) {

        if (point.getDevice() == -1 || point.getMap() == -1) {
            throw new IllegalArgumentException("device and map must be set");
        }
        sessionFactory.getCurrentSession().persist(point);
    }

    /**
     * Load Points from DB.
     *
     * @param map   map name
     * @param start start timestamp (millis from 1.1.1970 0:00 UTC)
     * @param end   end timestamp (millis from 1.1.1970 0:00 UTC)
     * @return List of Point objects
     */
    public List<Point> get(final String map, final long start, final long end) {
        final Integer mapId = mapService.getId(map);
        if (mapId == null) {
            return new ArrayList<>();
        }

        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Point.BY_TIME);
        query.setInteger("map", mapId);
        query.setLong("tsStart", start);
        query.setLong("tsEnd", end);

        return (List<Point>) query.list();
    }


    /**
     * Load Points from DB.
     *
     * @param map    map name
     * @param device the device's id
     * @param start  start timestamp
     * @param end    end timestamp
     * @return List of Point objects
     */
    public List<Point> getPointsByDeviceId(final String map, final String device, final long start, final long end) {
        final Integer mapId = mapService.getId(map);
        if (mapId == null) {
            return new ArrayList<>();
        }
        final Integer deviceId = deviceService.getId(device);
        if (deviceId == null) {
            return new ArrayList<>();
        }

        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Point.BY_DEVICEID);
        query.setInteger("map", mapId);
        query.setInteger("device", deviceId);
        query.setLong("tsStart", start);
        query.setLong("tsEnd", end);

        return (List<Point>) query.list();
    }


}
