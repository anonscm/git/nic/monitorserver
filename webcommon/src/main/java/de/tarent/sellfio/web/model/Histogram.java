package de.tarent.sellfio.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * The Histogram stores the distribution of wifi signal strength levels (over multiple measurements) for a set of
 * access points which are identified by their BSSID.
 * It is as Map<String bssid, Map<Integer dB, Float fraction>> with a name (id).
 * The keySet of this map is the set of all encountered BSSIDs.
 * The values are a map of signal strength level distributions.
 */
@Data
@AllArgsConstructor
public class Histogram extends HashMap<String, Map<Integer, Float>> {

    private String id;

    public Histogram() {
        super();
    }

    /**
     * Returns a {@link Set} of keys that exist in both given histograms.
     *
     * @param histogram1 the first histogram
     * @param histogram2 the second histogram
     * @return the {@link Set} of common keys
     */
    public static Set<String> getCommonKeys(final Histogram histogram1, final Histogram histogram2) {
        final Set<String> result = new HashSet<String>();
        result.addAll(histogram1.keySet());
        result.retainAll(histogram2.keySet());

        return result;
    }


}
