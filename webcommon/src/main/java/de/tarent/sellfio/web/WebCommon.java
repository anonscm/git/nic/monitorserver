package de.tarent.sellfio.web;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by mley on 09.06.15.
 */
@Configuration
@ComponentScan("de.tarent.sellfio.web")
@EnableWebMvc
public class WebCommon extends WebMvcConfigurerAdapter {
    public static final String PACKAGE = "de.tarent.sellfio.web";

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
