package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * The DeviceService holds a working set of known mappings between deviceId-strings (as reported by the app) and the
 * DB-id of the device. This is used to find the right id for new Points. In most cases, later we don't care what the
 * deviceId was, only if it is different or the same as that of some other point.
 * The DeviceService is also in charge of persisting new mappings.
 */
@Service
@Scope(value = "singleton")
public class DeviceService {

    private static final int NEW_ID_PLACEHOLDER = -1;

    // This Map maps a deviceId-string to the DB-id of that device.
    private final ConcurrentMap<String, Integer> nameToId = new ConcurrentHashMap<>(100);

    // This map is the inverse of the other one: it maps the DB-id to the name.
    private final ConcurrentMap<Integer, String> idToName = new ConcurrentHashMap<>(100);

    @Autowired
    private DeviceDAO deviceDAO;


    /**
     * Get the name for a device with a given DB-id, if it exists, null otherwise
     * @param id the device's DB-id
     * @return the device name
     */
    public String getName(final Integer id) {
        String deviceName = idToName.get(id);

        if (deviceName == null) {
            // We need to try and load it from the DB. There are no concurrency issues here (above the technical ones
            // solved by the ConcurrentHashMap) because it doesn't hurt if we accidentally query and store the mapping
            // multiple times.
            final Device device = deviceDAO.getById(id);
            if (device != null) {
                storeDeviceMapping(device.getName(), id);
                deviceName = device.getName();
            }
        }

        return deviceName;
    }


    /**
     * Get the DB-id for a device with a given name, if it exists, null otherwise.
     * @param name the devices's name
     * @return the device id
     */
    public Integer getId(final String name) {
        Integer deviceId = nameToId.get(name);

        if (deviceId == null) {
            // We need to try and load it from the DB. There are no concurrency issues here (above the technical ones
            // solved by the ConcurrentHashMap) because it doesn't hurt if we accidentally query and store the mapping
            // multiple times.
            final Device device = deviceDAO.getByName(name);
            if (device != null) {
                storeDeviceMapping(name, device.getId());
                deviceId = device.getId();
            }
        }

        return deviceId;
    }


    /**
     * Get the DB-id for a given device name. If the name is new then a new mapping will be created in the DB
     * transparently.
     *
     * @param name the deviceId string (as reported by the app)
     * @return the value of the corresponding column (device.id) from the DB. Null for some strange error cases...
     */
    public Integer mapNameToId(final String name) {
        Integer id = nameToId.putIfAbsent(name, NEW_ID_PLACEHOLDER);

        // The atomic putIfAbsent returns the previous value. If it _was_ null then it is now NEW_ID_PLACEHOLDER.
        // Therefore, only one thread can ever get null as a result. All others will get NEW_ID_PLACEHOLDER, if they
        // run "at the same time".
        // Otherwise it returned a proper id, which means the deviceId was already known and the map didn't change.
        if (id == null) {
            // This thread/call is the first to deal with the new deviceId.
            id = loadOrCreateDeviceMapping(name);

            // Now we overwrite the NEW_ID_PLACEHOLDER with the proper id and also store the inverse:
            storeDeviceMapping(name, id);
        }

        // If another thread is currently creating the new mapping, we need to wait a bit...
        while (id == NEW_ID_PLACEHOLDER) { // The == (instead of equals) is ok here because of the auto-unboxing of id.
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) { //NOSONAR - we just ignore the exception
                // We don't care, but hope that we now have a mapping...
            }
            id = nameToId.get(name);
        }

        return id;
    }

    /**
     * Load the id of the device from the DB or create it, if it doesn't exist.
     *
     * @param name the deviceId string (as reported by the app)
     * @return the value of the corresponding column (device.id) from the DB
     */
    private int loadOrCreateDeviceMapping(final String name) {
        Device device = deviceDAO.getByName(name);

        if (device == null) {
            device = new Device(name);
            deviceDAO.store(device);
        }

        return device.getId();
    }

    /**
     * store the name->id and id->name mappings in both our hashmaps
     */
    private void storeDeviceMapping(final String name, final Integer id) {
        nameToId.put(name, id);
        idToName.put(id, name);
    }

}
