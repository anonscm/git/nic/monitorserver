package de.tarent.sellfio.web.rest;


import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;

/**
 * Created by mley on 20.05.15.
 * Relays requests from monitor server to the map server.
 */
@RestController
@RequestMapping("/msr")
public class MapServerRelay {

    @Value("${map.host.internal}")
    private String baseUrl;

    /**
     * Gets a path from the map server and simply returns the result.
     * @param response HttpServletResponse object
     * @param path the path to be fetched from the map server.
     * @throws IOException when the path parameter is invalid or the communication with the mapserver fails
     */
    @RequestMapping(method = RequestMethod.GET)
    public void get(HttpServletResponse response, @RequestParam("path") String path) throws IOException {
        final URL url = new URL(baseUrl + "/" + path);
        response.setContentType("application/json");
        IOUtils.copy(url.openStream(), response.getOutputStream());
        response.flushBuffer();
    }
}
