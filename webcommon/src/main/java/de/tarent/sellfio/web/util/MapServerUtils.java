package de.tarent.sellfio.web.util;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import de.tarent.sellfio.web.model.BtleTransmitter;
import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.InvioGeoPoint;
import de.tarent.sellfio.web.model.WifiAp;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * Collection of methods to access the json files on the mapserver and deserialize them into objects.
 */
@Component
public class MapServerUtils {

    public static final String WIFI = "wifi";
    public static final String BTLE = "btle";

    private static final String MAPSERVER = "/mapserver/";
    private static final String MAPS_DIRECTORY = "/maps/";

    private static final Logger LOG = Logger.getLogger(MapServerUtils.class);

    // The number, in parentheses, is matched non-greedy (indicated by the "*?"), otherwise it would consume as many
    // characters as possible, and the following ' would match the last ' in the file. But we want it to match the
    // next '. (Alternatively we could have described the number more precisely, instead of using just a dot.)
    // The Pattern.DOTALL is necessary because the string will have multiple lines and we need the . at the beginning
    // and end of the pattern to match the newlines as well, which it wouldn't by default.
    Pattern scaleXMLPattern = Pattern.compile(".*<tag k='indoor_scale' v='(.*?)' />.*", Pattern.DOTALL);

    private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

    @Value("${map.host.internal}")
    private String mapServerEndpoint;


    /**
     * Download the wifi fingerprints json from the mapserver and deserialize it into nice objects.
     *
     * @param mapName the name of the map, e.g. "tarent3og"
     * @return the list of fingerprints
     * @throws IOException when required resource could not be download from mapserver
     */
    public List<Fingerprint> getWifiFingerprints(String mapName) throws IOException {
        final String url = mapServerEndpoint + MAPSERVER + "fingerprints/download?mapName=" + mapName +
                "&fingerprintsType=" + WIFI + "&onlyactive=false";
        final String json = downloadTextFile(url);
        if (json.isEmpty()) {
            LOG.warn("JSON string for wifi fingerprints from " + url + " is empty.");
            return new ArrayList<>();
        }
        final List<Fingerprint> fingerprints = jsonToFingerprints(json);

        return fingerprints;
    }


    /**
     * Download the btle fingerprints json from the mapserver and deserialize it into nice objects.
     *
     * @param mapName the name of the map, e.g. "tarent3og"
     * @return the list of fingerprints
     * @throws IOException when required resource could not be download from mapserver
     */
    public List<Fingerprint> getBtleFingerprints(String mapName) throws IOException {
        final String url = mapServerEndpoint + MAPSERVER + "fingerprints/download?mapName=" + mapName +
                "&fingerprintsType=" + BTLE + "&onlyactive=false";
        final String json = downloadTextFile(url);
        if (json.isEmpty()) {
            LOG.warn("JSON string for btle fingerprints from " + url + " is empty.");
            return new ArrayList<>();
        }
        final List<Fingerprint> fingerprints = jsonToFingerprints(json);

        return fingerprints;
    }


    /**
     * Parse a JSON string and extract the fingerprints that it describes.
     *
     * @param json the JSON string
     * @return a List of Fingerprint objects
     */
    public List<Fingerprint> jsonToFingerprints(String json) {
        final Type fingerprintListType = new TypeToken<ArrayList<Fingerprint>>() {
        }.getType();
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(InvioGeoPoint.class, new InvioGeoPointDeserializer());
        final List<Fingerprint> fingerprints = gsonBuilder.create().fromJson(json, fingerprintListType);
        return fingerprints;
    }


    /**
     * Download the file with the WIFI transmitters from the mapserver and parse it to get WifiTransmitter objects.
     *
     * @param mapName the name of the map. The server is configured as ${map.baseurl}.
     * @return list of WifiTransmitter objects.
     * @throws IOException when required resource could not be download from mapserver
     */
    public List<WifiAp> getWifiTransmitters(String mapName) throws IOException {
        final String url = mapServerEndpoint + MAPS_DIRECTORY + mapName + "/data/transmitter_wifi.json";
        return getTransmitters(url, new TypeToken<ArrayList<WifiAp>>() {
        }.getType());
    }


    /**
     * Download the file with the BTLE transmitters from the mapserver and parse it to get BtleTransmitter objects.
     *
     * @param mapName the name of the map. The server is configured as ${map.baseurl}.
     * @return list of BtleTransmitter objects.
     * @throws IOException when required resource could not be download from mapserver
     */
    public List<BtleTransmitter> getBtleTransmitters(String mapName) throws IOException {
        final String url = mapServerEndpoint + MAPS_DIRECTORY + mapName + "/data/transmitter_btle.json";
        return getTransmitters(url, new TypeToken<ArrayList<BtleTransmitter>>() {
        }.getType());
    }


    /**
     * Download the transmitter json from the mapserver and deserialize it into nice objects of the requested type.
     *
     * @param url  the complete mapserver url from which to download the json.
     * @param type the Type that can be used by gson to deserialize the correct type of fingerprints.
     * @return the list of transmitters.
     * @throws IOException when required resource could not be download from mapserver
     */
    private <T> List<T> getTransmitters(String url, Type type) throws IOException {
        final String transmitter = downloadTextFile(url);

        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(InvioGeoPoint.class, new InvioGeoPointDeserializer());
        //TODO: Get the transmitter information from the database
        final List<T> transmitters = gsonBuilder.create().fromJson(transmitter, type);

        return transmitters;
    }

    /**
     * Gets the origin and bounding box for a map. This method will load the tilemapresource.xml for the given map
     * from the mapserver and extract the coordinates of the origin and bounding box.
     *
     * @param mapName name of the map
     * @return array with lat/lon coordinates: origin, boundingbox min, boundingbox max
     * @throws IOException                  if map data could not be loaded
     * @throws SAXException                 if XML is malformed
     * @throws ParserConfigurationException if XML is malformed
     */
    public double[] getOriginAndBoundingBox(final String mapName) throws IOException, SAXException,
            ParserConfigurationException {
        final String url = mapServerEndpoint + MAPS_DIRECTORY + mapName + "/tiles/tilemapresource.xml";
        final String tileMapResource = downloadTextFile(url);

        return getOriginAndBoundingBoxFromXml(tileMapResource);
    }

    /**
     * Gets the origin and bounding box for a map. This method will load the tilemapresource.xml for the given map
     * from the mapserver and extract the coordinates of the origin and bounding box.
     *
     * @param tileMapResource XML of the tilemapresource file
     * @return array with lat/lon coordinates: origin, boundingbox min, boundingbox max
     * @throws ParserConfigurationException if XML is malformed
     * @throws IOException                  if map data could not be loaded
     * @throws SAXException                 if XML is malformed
     */
    public double[] getOriginAndBoundingBoxFromXml(String tileMapResource) throws ParserConfigurationException,
            IOException, SAXException {
        if (tileMapResource.isEmpty()) {
            throw new IllegalArgumentException("input must not be empty");
        }

        final double[] result = new double[6];

        final Document d = dbf.newDocumentBuilder().parse(new InputSource(new StringReader(tileMapResource)));
        final Element element = d.getDocumentElement();
        final Element origin = (Element) element.getElementsByTagName("Origin").item(0);
        result[0] = Double.parseDouble(origin.getAttribute("x"));
        result[1] = Double.parseDouble(origin.getAttribute("y"));

        final Element boundingBox = (Element) element.getElementsByTagName("BoundingBox").item(0);
        result[2] = Double.parseDouble(boundingBox.getAttribute("minx"));
        result[3] = Double.parseDouble(boundingBox.getAttribute("miny"));
        result[4] = Double.parseDouble(boundingBox.getAttribute("maxx"));
        result[5] = Double.parseDouble(boundingBox.getAttribute("maxy"));


        return result;
    }


    /**
     * Download the scale of the map (extracted from the osm-file on the mapserver).
     *
     * @param mapName the name of the map. The server is configured as ${map.baseurl}.
     * @return the scale
     * @throws IOException when required resource could not be download from mapserver
     */
    public double getScale(final String mapName) throws IOException {
        final String osmXMLFile = downloadMapData(mapName);
        final Matcher matcher = scaleXMLPattern.matcher(osmXMLFile);
        if (matcher.matches()) {
            final String match = matcher.group(1);
            return Double.parseDouble(match);
        }

        throw new IOException("scale not found in OSM file");
    }


    /**
     * Download the map data file from the mapserver (aka the osm file).
     *
     * @param mapName the name of the map. The server is configured as ${map.baseurl}.
     * @return the XML string
     * @throws IOException when required resource could not be download from mapserver
     */
    public String downloadMapData(final String mapName) throws IOException {
        final String url = mapServerEndpoint + MAPS_DIRECTORY + mapName + "/data/" + mapName + ".osm";
        final String osmXMLFile = downloadTextFile(url);
        return osmXMLFile;
    }


    /**
     * Make an HTTP-GET request to download a text file from a specific/complete URL.
     *
     * @param url the URL to get
     * @return a String containing the response body.
     * @throws IOException when required resource could not be download from mapserver
     */
    public String downloadTextFile(String url) throws IOException {
        final CloseableHttpClient httpclient = HttpClients.createDefault();

        final HttpGet httpget = configureGetRequest(url);
        String json = "";

        //close() is automatically called, if it throws an IOException,
        // it will be supressed (as specified in the Java Language Specification 14.20.3)
        //https://docs.oracle.com/javase/specs/jls/se8/html/jls-14.html#jls-14.20.3
        try (CloseableHttpResponse response = httpclient.execute(httpget);) {
            final int status = response.getStatusLine().getStatusCode();
            if (status == HttpStatus.OK.value()) {
                json = getContentFromResponse(response);
            } else {
                LOG.warn("Download of " + url + " failed with status " + status);
                throw new IOException("Download of " + url + "failed with: " + response.getStatusLine().toString());
            }
        } catch (IOException e) {
            LOG.error("Exception while downloading json from " + url + ": " + e);
            throw e;
        }

        return json;
    }


    /**
     * Create HttpGet request for this URL and set default config (i.e. timeouts).
     *
     * @param url the URL to get
     * @return the usable request
     */
    private HttpGet configureGetRequest(String url) {

        // The timeout for establishing the connection with the remote host:
        final int connectTimeout = 1; //Seconds

        // The timeout while waiting for data - after the connection was established:
        final int socketTimeout = 10; //Seconds

        final RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(socketTimeout * 1000)
                .setConnectTimeout(connectTimeout * 1000)
                .build();

        final HttpGet httpget = new HttpGet(url);
        httpget.setConfig(requestConfig);
        return httpget;
    }


    /**
     * Extract the UTF-8 string content from the response object. Also wraps gzip-content, if necessary.
     *
     * @param response the response from the mapserver
     * @return the string content
     * @throws IOException when stream problems occur
     */
    private String getContentFromResponse(HttpResponse response) throws IOException {
        String content = "";
        final HttpEntity responseEntity = response.getEntity();
        if (responseEntity != null) {
            InputStream instream = responseEntity.getContent();
            final Header contentEncoding = response.getFirstHeader("Content-Encoding");
            if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                instream = new GZIPInputStream(instream);
            }
            final StringWriter writer = new StringWriter();
            IOUtils.copy(instream, writer, "UTF-8");
            content = writer.toString();
        }
        return content;
    }


    public String getMapServerEndpoint() {
        return mapServerEndpoint;
    }

    public void setMapServerEndpoint(String mapServerEndpoint) {
        this.mapServerEndpoint = mapServerEndpoint;
    }

}
