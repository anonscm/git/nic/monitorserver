package de.tarent.sellfio.web.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * A Bluetooth-Beacon.
 */
@Data
@Entity
@Table(name="btletransmitter")
@NamedQueries({@NamedQuery(name="btle_by_map",
        query="select b from BtleTransmitter b where b.transmitter.map_id = :map_id order by b.uuid, b.majorId, b.minorId")})

public class BtleTransmitter implements SignalSource{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @OneToOne(cascade = CascadeType.REMOVE, optional = false, fetch = FetchType.EAGER, orphanRemoval = false)
    @JoinColumn(name="transmitter_id")
    private Transmitter transmitter;

    private double lat;

    private double lon;
    private double height;
    private String vendor;
    private String name;

    private String uuid;

    private int majorId;

    private int minorId;

    private String powerLevel;

    private int sendInterval;

    public BtleTransmitter(){}

    public BtleTransmitter(Transmitter t) {
        this.transmitter = t;
        setIdentifier(t.getIdentifier());
    }

    private final void setIdentifier(String identifier) {
        final String[] parts = identifier.split("_");
        uuid = parts[0];
        majorId = Integer.parseInt(parts[1]);
        minorId = Integer.parseInt(parts[2]);
    }
}
