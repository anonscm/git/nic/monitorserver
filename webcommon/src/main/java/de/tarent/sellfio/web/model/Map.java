package de.tarent.sellfio.web.model;

import de.tarent.sellfio.web.util.HaversineDistance;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Data
@NamedQueries({
        @NamedQuery(name = Map.BY_NAME, query = "SELECT m FROM Map m WHERE m.name = :name"),
        @NamedQuery(name = Map.BY_ID, query = "SELECT m FROM Map m WHERE m.id = :id")
})
public class Map {

    public static final String BY_NAME = "Map.BY_NAME";
    public static final String BY_ID = "Map.BY_ID";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    /**
     * Bounding box of the map
     */
    private double minLat, minLon, maxLat, maxLon;

    /**
     * Origin of the map.
     */
    private double originLat, originLon;

    /**
     * Scale.
     */
    private double scale;

    public Map() {
    }

    public Map(String name) {
        this.name = name;
    }

    /**
     * Get the width of the map in meters.
     *
     * @return the width of the map in meters.
     */
    public double getWidth() {
        return HaversineDistance.calcDistance(minLat, minLon, minLat, maxLon) / scale;
    }

    /**
     * Get the height of the map in meters.
     *
     * @return the height of the map in meters.
     */
    public double getHeight() {
        return HaversineDistance.calcDistance(minLat, minLon, maxLat, minLon) / scale;
    }

    public double[][] getBounds() {
        return new double[][]{{minLat, minLon}, {maxLat, maxLon}};
    }

}
