package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.InvioGeoPoint;
import de.tarent.sellfio.web.model.AggregatedPoint;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AggregatedPointDAO. Stores and retrieves AggregatedPoint objects.
 */
@Repository
@Transactional
public class AggregatedPointDAO {


    /**
     * Query to get aggregated heat points by map and timespan
     */
    private static final String GET_FROM_TO = "select p.id as id, p.lat as lat, p.lon as lon, p.visits as visits, " +
            "v.device as device, p.tstamp as tstamp " +
            "from point_aggregated p " +
            "join visit v on v.point_aggregated = p.id " +
            "where map = ? and tstamp >= ? and tstamp <= ? " +
            "order by p.id";

    @Autowired
    protected SessionFactory sessionFactory;

    /**
     * Save a List of Heatmaps.
     *
     * @param heatmaps List of 2d AggregatedPoint arrays
     */
    public void saveHeatMaps(final List<AggregatedPoint[][]> heatmaps) {
        for (AggregatedPoint[][] heatmap : heatmaps) {
            for (AggregatedPoint[] points : heatmap) {
                for (AggregatedPoint p : points) {
                    if (p != null) {
                        sessionFactory.getCurrentSession().persist(p);
                    }
                }
            }
        }
    }

    /**
     * Get aggregated points for map and timespan.
     *
     * @param mapId Map id
     * @param from  from timestamp in UTC
     * @param to    to timestamp in UTC
     * @return Map of Map of AggregatedPoints. The keys of the maps are lat and lon
     */
    @Transactional(readOnly = true)
    public Map<InvioGeoPoint, AggregatedPoint> get(final int mapId, final long from, final long to) {

        return sessionFactory.getCurrentSession()
                .doReturningWork(new ReturningWork<Map<InvioGeoPoint, AggregatedPoint>>() {
                    @Override
                    public Map<InvioGeoPoint, AggregatedPoint> execute(Connection connection)
                            throws SQLException {

                        final Map<InvioGeoPoint, AggregatedPoint> result = new HashMap<>();
                        try (final PreparedStatement preparedStatement = connection.prepareStatement(GET_FROM_TO)) {
                            preparedStatement.setInt(1, mapId);
                            preparedStatement.setLong(2, from);
                            preparedStatement.setLong(3, to);

                            preparedStatement.execute();

                            try (final ResultSet resultSet = preparedStatement.getResultSet()) {
                                getDataFromResultSet(mapId, result, resultSet);
                            }
                        }
                        return result;

                    }
                });
    }

    private void getDataFromResultSet(int mapId, Map<InvioGeoPoint, AggregatedPoint> result, ResultSet resultSet)
            throws SQLException {
        long lastId = -1;

        while (resultSet.next()) {
            final long id = resultSet.getLong("id");
            final double lat = resultSet.getDouble("lat");
            final double lon = resultSet.getDouble("lon");
            final long tstamp = resultSet.getLong("tstamp");

            // Due to the join we might get multiple results from the same entry in point_aggregated.
            // This can be ignored, since we distinct different entries by their coordinates, which we
            // want to aggregate here.
            final AggregatedPoint point = getPointAt(result, mapId, tstamp, lat, lon);
            point.getDeviceIds().add(resultSet.getInt("device"));

            //For the all visits counter, we have to check, that we do not count visits from points with same id twice.
            //This works, because the result set is ordered by ids. If the id is different, but the Point ( and the
            // location) is the same, we have to add the number of visits.
            if (lastId != id) {
                point.setVisits(point.getVisits() + resultSet.getInt("visits"));
                lastId = id;
            }

        }
    }


    /**
     * Gets an AggregatedPoint object for a specific position. If it does not exist in the pointMap, it is created and
     * added to the map
     *
     * Public only for testing purposes. You probably don't need to call it from outside (but it doesn't hurt either).
     *
     * @param pointMap Map of points
     * @param mapId    mapId
     * @param tstamp   timestamp, required when new AggregatedPoint objects are created
     * @param lat      latitude
     * @param lon      longitude
     * @return the AggregatedPoint object
     */
    public AggregatedPoint getPointAt(Map<InvioGeoPoint, AggregatedPoint> pointMap, int mapId, long tstamp,
                                       double lat, double lon) {

        final InvioGeoPoint keyPoint = new InvioGeoPoint(lat, lon);

        if (!pointMap.containsKey(keyPoint)) {
            pointMap.put(keyPoint, new AggregatedPoint(mapId, lat, lon, tstamp));
        }

        return pointMap.get(keyPoint);
    }

}
