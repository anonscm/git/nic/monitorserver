package de.tarent.sellfio.web.dao;

/**
 * Class to manage errors in FingerprintDAO.
 * TODO: this exception is used often, for different entities. Maybe it doesn't say anything useful at all? Or should
 *       be made into multiple different exceptions? Or be renamed to something more generic and less misleading?
 */
public class FingerprintStoreException extends Exception {

    /**
     * Create a new FingerprintStoreException.
     *
     * @param message the reason for this exception.
     */
    public FingerprintStoreException(String message) {
        super(message);
    }

}
