package de.tarent.sellfio.web.model;

/**
 * Created by mley on 12.11.14.
 */
public class State {
    public static final String ZOMBIE = "ZOMBIE";
    public static final String PAID = "PAID";
    public static final String CHECKOUT = "CHECKOUT";
    public static final String SHOPPING = "SHOPPING";

    public static String fromKey(int state) {
        switch (state) {
            case 0:
                return ZOMBIE;
            case 1:
                return PAID;
            case 2:
                return CHECKOUT;
            case 3:
                return SHOPPING;
        }

        return ZOMBIE;
    }

}
