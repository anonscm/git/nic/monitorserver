package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.Customer;
import de.tarent.sellfio.web.model.CartItem;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DAO for customers
 */
@Repository
public class CustomerDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Save a cart
     *
     * @param customer the {@link Customer}
     */
    @Transactional(value = "txName", readOnly = false)
    public void storeCart(Customer customer) {

        // sadly Hibernate+H2 does not fill CartItem.cartId automatically, so we have to do this manually
        final List<CartItem> items = customer.getCartItems();
        customer.setCartItems(null);

        // we always store new customers, never update them. To make sure of this, we set id to 0
        customer.setId(0);

        final long cartId = (long) sessionFactory.getCurrentSession().save(customer);

        for (final CartItem ci : items) {
            ci.setId(0);
            ci.setCartId(cartId);
            sessionFactory.getCurrentSession().save(ci);
        }
        customer.setCartItems(items);
        sessionFactory.getCurrentSession().update(customer);
    }

    /**
     * Gets the last version of the shopping cart stored before the given timestamp.
     *
     * @param map       Name of map
     * @param deviceId  device id
     * @param timeStamp timestamp
     * @return the last Cart object or null if none was stored.
     */
    @Transactional(value = "txName", readOnly = true)
    public Customer getLastCartBefore(String map, String deviceId, long timeStamp) {
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Customer.LAST_CART);
        query.setString("deviceId", deviceId);
        query.setString("map", map);
        query.setLong("timestamp", timeStamp);
        final List<Customer> result = (List<Customer>) query.list();

        if (result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }
}
