package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.AbstractTest;
import de.tarent.sellfio.web.model.Point;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.ConcurrentMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class PointDAOTest extends AbstractTest {

    @Autowired
    PointDAO pointDAO;
    @Autowired
    DeviceService deviceService;

    Point p1, p2, p3, p4;


    @Before
    public void setup() {
        p1 = new Point(System.currentTimeMillis(), null, null, new double[]{50.0, 7.0});
        p2 = new Point(System.currentTimeMillis(), null, null, new double[]{50.1, 7.1});
        p3 = new Point(System.currentTimeMillis(), null, null, new double[]{50.2, 7.2});
        p4 = new Point(System.currentTimeMillis(), null, null, new double[]{51.9, 6.0});
    }


    @Test
    @Transactional(value="txName", readOnly = false)
    public void testStoreAndGet() {
        ((ConcurrentMap<Integer, String>) ReflectionTestUtils.getField(deviceService, "idToName")).clear();
        ((ConcurrentMap<String, Integer>)ReflectionTestUtils.getField(deviceService, "nameToId")).clear();

        pointDAO.store(p1, "map-1", "device-1");
        pointDAO.store(p2, "map-1", "device-1");
        pointDAO.store(p3, "map-1", "device-2");
        pointDAO.store(p4, "map-2", "device-1");

        System.out.println(p1.getDevice());
        System.out.println(p3.getDevice());
        // Same name => same ID, different name => different ID:
        assertEquals(p1.getDevice(), p4.getDevice());
        assertEquals(p1.getMap(), p3.getMap());
        assertNotEquals(p1.getDevice(), p3.getDevice());
        assertNotEquals(p1.getMap(), p4.getMap());

        List<Point> points;

        points = pointDAO.get("map-1", Long.MIN_VALUE, Long.MAX_VALUE);
        assertEquals(3, points.size());

        points = pointDAO.get("map-2", Long.MIN_VALUE, Long.MAX_VALUE);
        assertEquals(1, points.size());

        points = pointDAO.get("map-3", Long.MIN_VALUE, Long.MAX_VALUE);
        assertEquals(0, points.size());

        points = pointDAO.get("map-1", Long.MIN_VALUE, Long.MIN_VALUE + 1);
        assertEquals(0, points.size());


        points = pointDAO.getPointsByDeviceId("map-1", "device-1", Long.MIN_VALUE, Long.MAX_VALUE);
        assertEquals(2, points.size());

        points = pointDAO.getPointsByDeviceId("map-1", "device-3", Long.MIN_VALUE, Long.MAX_VALUE);
        assertEquals(0, points.size());

        points = pointDAO.getPointsByDeviceId("map-3", "device-1", Long.MIN_VALUE, Long.MAX_VALUE);
        assertEquals(0, points.size());
    }

}
