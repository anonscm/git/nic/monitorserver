package de.tarent.sellfio.web.util;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;

/**
 * Created by mley on 19.03.15.
 */
public class HaversineDistanceTest {
    @Test
    public void testCalcDistance() {
        double distance = 34850.37;
        double[] start = new double[]{50.911604, 7.456994};
        double[] end = new double[]{50.721704, 7.062352};

        double result = HaversineDistance.calcDistance(start, end);
        assertEquals(distance, result, 0.01);
    }

    @Test
    public void testInvalidInput() {
        double[] start = new double[]{Double.NaN, 7.4569942};
        double[] end = new double[]{50.7217044, 7.0623527};

        double result;

        start[0] = Double.MAX_VALUE;

        result = HaversineDistance.calcDistance(start, end);
        assertFalse(Double.isNaN(result));

        start[0] = -0.0d;

        result = HaversineDistance.calcDistance(start, end);
        assertFalse(Double.isNaN(result));

        start = new double[]{0,0};
        result = HaversineDistance.calcDistance(start, start);
        assertEquals(0, result, 1E-10);
    }

    @Test
    public void testCalcRoute() {
        double distance = 34850.37;
        double[] start = new double[]{50.911604, 7.456994};
        double[] end = new double[]{50.721704, 7.062352};

        // 4 points
        double result = HaversineDistance.calcRouteDistance(new double[][]{start, end, start, end});
        assertEquals(distance * 3, result, 0.02);

        // 2 points, same as calcDistance
        result = HaversineDistance.calcRouteDistance(new double[][]{start, end});
        assertEquals(distance, result, 0.01);

        // 1 point: distance 0
        result = HaversineDistance.calcRouteDistance(new double[][]{start});
        assertEquals(0, result, 1E-10);
    }
}
