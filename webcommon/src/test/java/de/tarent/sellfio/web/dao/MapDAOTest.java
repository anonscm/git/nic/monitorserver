package de.tarent.sellfio.web.dao;


import de.tarent.sellfio.web.AbstractTest;
import de.tarent.sellfio.web.model.Map;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

@Transactional(value = "txName", readOnly = false)
public class MapDAOTest extends AbstractTest {

    @Autowired
    MapDAO mapDAO;

    Map m1, m2, m3;

    @Before
    public void setup() {
        m1 = new Map("map-one");
        m2 = new Map("map-two");
        m3 = new Map("map-three");
    }

    @Test
    public void testStoreAndGet() {
        mapDAO.store(m1);
        mapDAO.store(m2);
        mapDAO.store(m3);

        // Saving the Map to the DB should provide it with an ID:
        assertNotNull(m3.getId());

        Map m = mapDAO.getByName(m1.getName());
        assertEquals(m1.getId(), m.getId());
        assertEquals(m1.getName(), m.getName());

        m = mapDAO.getById(m2.getId());
        assertEquals(m2.getId(), m.getId());
        assertEquals(m2.getName(), m.getName());

        Map m3a = mapDAO.getById(m3.getId());
        assertNotEquals(m3a.getId(), m1.getId());
    }

    /*
    @Test
    public void testFillMetaData() throws IOException, ParserConfigurationException, SAXException {
        Map m = new Map();
        m.setName("mapdaotestmap");

        MapDAO mapDAO = new MapDAO();

        MapServerUtils msu = mock(MapServerUtils.class);
        when(msu.getScale(any(String.class))).thenReturn(2.0);
        when(msu.getOriginAndBoundingBox(any(String.class))).thenReturn(new double[]{1.0, 2.0, 3.0, 4.0, 5.0, 6.0});

        ReflectionTestUtils.setField(mapDAO, "mapServerUtils", msu);

        assertTrue(mapDAO.fillMetaData(m));
        assertEquals(2.0, m.getScale(), 0.00001);
        assertEquals(1.0, m.getOriginLat(), 0.000001);
        assertEquals(3.0, m.getMinLat(), 0.000001);

        when(msu.getScale(any(String.class))).thenThrow(new IOException("FAILZ"));

        assertFalse(mapDAO.fillMetaData(m));


    }
*/
}
