package de.tarent.sellfio.web.dao;


import de.tarent.sellfio.web.AbstractTest;
import de.tarent.sellfio.web.model.Device;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;


public class DeviceDAOTest extends AbstractTest {

    @Autowired
    DeviceDAO deviceDAO;

    Device d1, d2, d3;

    @Before
    public void setup() {
        d1 = new Device("device-one");
        d2 = new Device("device-two");
        d3 = new Device("device-three");
    }

    @Test
    @Transactional(value="txName", readOnly = false)
    public void testStoreAndGet() {
        deviceDAO.store(d1);
        deviceDAO.store(d2);
        deviceDAO.store(d3);

        // Saving the Device to the DB should provide it with an ID:
        assertNotNull(d3.getId());

        Device d = deviceDAO.getByName(d1.getName());
        assertEquals(d1.getId(), d.getId());
        assertEquals(d1.getName(), d.getName());

        d = deviceDAO.getById(d2.getId());
        assertEquals(d2.getId(), d.getId());
        assertEquals(d2.getName(), d.getName());

        Device d3a = deviceDAO.getByName(d3.getName());
        assertNotEquals(d3a.getId(), d1.getId());
    }

}
