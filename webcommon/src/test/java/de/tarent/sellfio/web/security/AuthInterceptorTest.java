package de.tarent.sellfio.web.security;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osiam.resources.scim.Role;
import org.osiam.resources.scim.User;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.method.HandlerMethod;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Created by mley on 04.03.15.
 */
public class AuthInterceptorTest {

    AuthInterceptor ai = new AuthInterceptor();

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    HandlerMethod handler;

    @Mock
    HttpSession session;

    User user;

    @Before
    public void before() {

        MockitoAnnotations.initMocks(this);

        when(request.getSession()).thenReturn(session);

        ReflectionTestUtils.setField(ai, "osiamEnabled", Boolean.TRUE);

    }

    @Test
    public void testNonMethod() throws IOException {
        assertTrue(ai.preHandle(request, response, this));
    }

    @Test
    public void testNoRoleRequired() throws IOException, NoSuchMethodException {
        when(handler.getMethod()).thenReturn(getMethod("none"));

        assertTrue(ai.preHandle(request, response, handler));
    }


    @Test
    public void testAuthWithEmptyRole() throws NoSuchMethodException, IOException {
        when(handler.getMethod()).thenReturn(getMethod("empty"));

        // just login required, but not logged in
        assertFalse(ai.preHandle(request, response, handler));


        user = new User.Builder("user").build();
        when(session.getAttribute(eq(AuthInterceptor.OSIAM_USER))).thenReturn(user);

        // now logged in
        assertTrue(ai.preHandle(request, response, handler));

    }

    @Test
    public void testAuthWithRole() throws NoSuchMethodException, IOException {
        user = new User.Builder("user").addRole(new Role.Builder().setDisplay("role").build()).build();
        when(session.getAttribute(eq(AuthInterceptor.OSIAM_USER))).thenReturn(user);

        when(handler.getMethod()).thenReturn(getMethod("role"));

        assertTrue(ai.preHandle(request, response, handler));

        when(handler.getMethod()).thenReturn(getMethod("other"));

        assertFalse(ai.preHandle(request, response, handler));
    }


    private Method getMethod(String name) throws NoSuchMethodException {
        return this.getClass().getMethod(name);

    }


    @RolesAllowed("role")
    public void role() {

    }

    @RolesAllowed("other")
    public void other() {

    }


    public void none() {

    }

    @RolesAllowed("")
    public void empty() {

    }
}
