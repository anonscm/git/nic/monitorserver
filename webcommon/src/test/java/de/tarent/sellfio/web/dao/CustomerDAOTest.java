package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.AbstractTest;
import de.tarent.sellfio.web.model.CartItem;
import de.tarent.sellfio.web.model.Customer;
import de.tarent.sellfio.web.model.State;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by mley on 18.03.15.
 */


public class CustomerDAOTest extends AbstractTest {

    @Autowired
    CustomerDAO customerDAO;

    Customer c1, c2, c3;

    @Test
    @Transactional(value="txName", readOnly = false)
    public void testCartDAO() {
        c1 = new Customer();
        c1.setMap("map");
        c1.setDeviceId("abc");
        c1.setState(State.SHOPPING);

        c1.setTimestamp(1);
        c1.setLocation(new double[]{1, 0});

        c2 =new Customer();
        c2.setMap("map");
        c2.setDeviceId("abc");
        c2.setState(State.SHOPPING);
        c2.getCartItems().add(new CartItem("ding", 1, 1));
        c2.setTimestamp(2);
        c2.setLocation(new double[]{2, 0});

        c3 =new Customer();
        c3.setMap("map");
        c3.setDeviceId("abc");
        c3.setState(State.SHOPPING);
        c3.getCartItems().add(new CartItem("ding", 1, 1));
        c3.getCartItems().add(new CartItem("dong", 2, 2));
        c3.setTimestamp(2);
        c3.setLocation(new double[]{3, 0});


        // save 3 different customers
        customerDAO.storeCart(c1);
        customerDAO.storeCart(c2);
        customerDAO.storeCart(c3);

        Customer c = customerDAO.getLastCartBefore("map","abc", 1);
        assertNull(c);

        // we want the cart right before cart with timestamp 3
        c = customerDAO.getLastCartBefore("map", "abc", 3);
        assertEquals(c, c2);

    }
}
