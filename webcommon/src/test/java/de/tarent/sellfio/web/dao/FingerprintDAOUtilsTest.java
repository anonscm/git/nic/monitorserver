package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.Histogram;
import de.tarent.sellfio.web.model.InvioGeoPoint;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(MockitoJUnitRunner.class)
public class FingerprintDAOUtilsTest {

    @Test
    public void testJsonConversion() {
        Map<Integer, Float> m1 = new HashMap<>();
        m1.put(-46, 0.5f);
        m1.put(-49, 0.5f);
        Map<Integer, Float> m2 = new HashMap<>();
        m2.put(-46, 1f);
        Map<Integer, Float> m3 = new HashMap<>();
        m3.put(-32, 1f);

        Histogram h1 = new Histogram("h1");
        Histogram h2 = new Histogram("h2");
        h1.put("ap-1", m1);
        h1.put("ap-2", m2);
        h2.put("ap-2", m3);

        InvioGeoPoint p1 = new InvioGeoPoint(50721512, 7060331);
        InvioGeoPoint p2 = new InvioGeoPoint(50723077, 7061954);

        Fingerprint fp1 = new Fingerprint(h1, p1);
        Fingerprint fp2 = new Fingerprint(h2, p2);

        List<Fingerprint> fpsIn = new ArrayList<>();
        fpsIn.add(fp1);
        fpsIn.add(fp2);

        String json = FingerprintDAOUtils.toJson(fpsIn);

        List<Fingerprint> fpsOut = FingerprintDAOUtils.fromJson(json);

        assertEquals(fpsIn.size(), fpsOut.size());

        // Just a small sample...
        if (fpsOut.get(0).getId() != null && fpsOut.get(0).getId().equals(fpsIn.get(0).getId())) {
            Assert.assertEquals(fpsOut.get(0).getId(), fpsIn.get(0).getId());
            Assert.assertEquals(fpsOut.get(0).getHistogram().get("ap-2"), fpsOut.get(0).getHistogram().get("ap-2"));
        } else {
            Assert.assertEquals(fpsOut.get(0).getId(), fpsIn.get(1).getId());
            Assert.assertEquals(fpsOut.get(1).getHistogram().get("ap-2"), fpsIn.get(1).getHistogram().get("ap-2"));
        }
    }

    @Test
    public void emptyList() {
        String json = FingerprintDAOUtils.toJson(new ArrayList<Fingerprint>());

        assertEquals("[]", json);
    }

    @Test
    public void emptyJsonArray() {
        List<Fingerprint> fps = FingerprintDAOUtils.fromJson("[]");

        assertTrue(fps.isEmpty());
    }

    @Test
    public void emptyJsonString() {
        List<Fingerprint> fps = FingerprintDAOUtils.fromJson("");

        assertTrue(fps.isEmpty());
    }


    @Test
    public void testConvertProbabilitiesToCounts() {
        String json = "[{\"histogram\":{\"00:1b:8f:92:72:b2\":{\"-70\":0.16666667,\"-63\":0.16666667,\"-62\":0.16666667,\"-61\":0.16666667,\"-59\":0.16666667,\"-57\":0.16666667}," +
                "                       \"00:1b:8f:8a:82:90\":{\"-64\":0.33333334,\"-63\":0.16666667,\"-62\":0.5}," +
                "                       \"00:1b:8f:92:72:b1\":{\"-71\":1}," +
                "                       \"00:1b:8f:92:72:b0\":{\"-69\":0.2,\"-62\":0.4,\"-60\":0.4}}," +
                "  \"point\":{\"latitude\":50.733229,\"longitude\":7.055979},\"id\":\"FP-90\",\"scanCount\":0,\"scanTime\":0}," +
                " {\"histogram\":{\"00:1b:8f:92:72:b0\":{\"-82\":0.25,\"-59\":0.75}}," +
                "  \"point\":{\"latitude\":50.733229,\"longitude\":7.055979},\"id\":\"FP-91\",\"scanCount\":0,\"scanTime\":0}]";
        List<Fingerprint> fingerprints = FingerprintDAOUtils.fromJson(json);
        FingerprintDAOUtils.convertProbabilitiesToCounts(fingerprints);

        // The conversion inflates the counts by a factor of 100 so that we can distinguish these artificial counts
        // from the real ones. It makes no difference for the clients because they work on the relative probabilities
        // anyway.
        assertEquals(600, fingerprints.get(0).getScanCount());

        assertEquals(100f, fingerprints.get(0).getHistogram().get("00:1b:8f:92:72:b2").get(-70), 0.0000001);
        assertEquals(100f, fingerprints.get(0).getHistogram().get("00:1b:8f:92:72:b2").get(-63), 0.0000001);

        assertEquals(200f, fingerprints.get(0).getHistogram().get("00:1b:8f:8a:82:90").get(-64), 0.0000001);
        assertEquals(100f, fingerprints.get(0).getHistogram().get("00:1b:8f:8a:82:90").get(-63), 0.0000001);
        assertEquals(300f, fingerprints.get(0).getHistogram().get("00:1b:8f:8a:82:90").get(-62), 0.0000001);

        assertEquals(100f, fingerprints.get(0).getHistogram().get("00:1b:8f:92:72:b1").get(-71), 0.0000001);

        assertEquals(100f, fingerprints.get(0).getHistogram().get("00:1b:8f:92:72:b0").get(-69), 0.0000001);
        assertEquals(200f, fingerprints.get(0).getHistogram().get("00:1b:8f:92:72:b0").get(-62), 0.0000001);
        assertEquals(200f, fingerprints.get(0).getHistogram().get("00:1b:8f:92:72:b0").get(-60), 0.0000001);

        assertEquals(400, fingerprints.get(1).getScanCount());

        assertEquals(100f, fingerprints.get(1).getHistogram().get("00:1b:8f:92:72:b0").get(-82), 0.0000001);
        assertEquals(300f, fingerprints.get(1).getHistogram().get("00:1b:8f:92:72:b0").get(-59), 0.0000001);
    }

}
