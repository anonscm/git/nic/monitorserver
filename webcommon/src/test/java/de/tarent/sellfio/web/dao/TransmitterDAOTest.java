package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.AbstractTest;
import de.tarent.sellfio.web.model.BtleTransmitter;
import de.tarent.sellfio.web.model.Map;
import de.tarent.sellfio.web.model.NetworkNode;
import de.tarent.sellfio.web.model.Transmitter;
import de.tarent.sellfio.web.model.WifiAp;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by mley on 28.10.15.
 */
@Transactional("txName")
public class TransmitterDAOTest extends AbstractTest {

    @Autowired
    TransmitterDAO transmitterDAO;

    @Autowired
    MapDAO mapDAO;
    int mapId;

    @Autowired
    SessionFactory sessionFactory;

    @Before
    public void setup() {
        clean();

        Map m = new Map("testmap");
        mapDAO.store(m);
        mapId = m.getId();
    }

    @After
    public void clean() {
        sessionFactory.getCurrentSession().createSQLQuery("delete from btletransmitter").executeUpdate();
        sessionFactory.getCurrentSession().createSQLQuery("delete from wifinetworks").executeUpdate();
        sessionFactory.getCurrentSession().createSQLQuery("delete from networknode").executeUpdate();
        sessionFactory.getCurrentSession().createSQLQuery("delete from wifiap").executeUpdate();
        sessionFactory.getCurrentSession().createSQLQuery("delete from transmitter").executeUpdate();
        sessionFactory.getCurrentSession().createSQLQuery("delete from map").executeUpdate();
        sessionFactory.getCurrentSession().clear();

    }

    @Test
    public void testCascade1() {

        WifiAp ap = new WifiAp();
        Transmitter t = new Transmitter(mapId, true, "wifi", "ab:cd:ef");
        ap.getNetworks().add(new NetworkNode(t));

        transmitterDAO.save(ap);


        // delete ap
        sessionFactory.getCurrentSession().createQuery("delete from WifiAp").executeUpdate();

        List<NetworkNode> networks = (List<NetworkNode>) sessionFactory.getCurrentSession()
                .createQuery("select n from NetworkNode n").list();

        // yes, that is not 100% nice, but ok.
        assertEquals(1, networks.size());

        // now delete remaining node by hand
        sessionFactory.getCurrentSession().createQuery("delete from NetworkNode").executeUpdate();

        // transmitter shall not be deleted
        List<Transmitter> transmitters = (List<Transmitter>) sessionFactory.getCurrentSession()
                .createQuery("select t from Transmitter t").list();

        assertEquals(1, transmitters.size());
        assertEquals("ab:cd:ef", transmitters.get(0).getIdentifier());

        sessionFactory.getCurrentSession().createQuery("delete from Transmitter").executeUpdate();

    }

    @Test(expected = ConstraintViolationException.class)
    public void testCascade2() {

        WifiAp ap = new WifiAp();
        Transmitter t = new Transmitter(mapId, true, "wifi", "ab:cd:ef");
        ap.getNetworks().add(new NetworkNode(t));

        transmitterDAO.save(ap);

        // deleting transmitter shall not be possible, since it is still referenced in NetworkNode
        sessionFactory.getCurrentSession().createQuery("delete from Transmitter").executeUpdate();

    }


    @Test
    public void testRemoveByMap() {
        Map map1 = new Map("map1");
        Map map2 = new Map("map2");
        mapDAO.store(map1);
        mapDAO.store(map2);

        WifiAp ap1 = new WifiAp();
        ap1.setMap_id(map1.getId());
        Transmitter t1 = new Transmitter(map1.getId(), true, "wifi", "ab:cd:ef");
        ap1.getNetworks().add(new NetworkNode(t1));

        transmitterDAO.save(ap1);

        WifiAp ap2 = new WifiAp();
        ap2.setMap_id(map2.getId());
        Transmitter t2 = new Transmitter(map2.getId(), true, "wifi", "ab:cd:ef");
        ap2.getNetworks().add(new NetworkNode(t2));

        transmitterDAO.save(ap2);

        List<WifiAp> list1 = transmitterDAO.getWifiAps(map1.getName());
        List<WifiAp> list2 = transmitterDAO.getWifiAps(map2.getName());

        assertEquals(1, list1.size());
        assertEquals(1, list2.size());

        transmitterDAO.removeAllSignalSources(map1.getName());

        list1 = transmitterDAO.getWifiAps(map1.getName());
        list2 = transmitterDAO.getWifiAps(map2.getName());

        assertEquals(0, list1.size());
        assertEquals(1, list2.size());

    }

    @Test
    public void testStoreAndGetAllWifi() {

        Transmitter t = new Transmitter(mapId, true, "wifi", "abcd");

        transmitterDAO.save(t);

        List<Transmitter> l = transmitterDAO.getTransmitters("testmap", "wifi");

        assertEquals(t, l.get(0));

    }

    @Test
    public void testStoreBtleTransmitter() {
        // test save with new transmitter
        Transmitter t = new Transmitter(mapId, true, "btle", "abcd_1_1");
        BtleTransmitter b = new BtleTransmitter(t);

        transmitterDAO.save(b);

        // test save with existing transmitter
        t = new Transmitter(mapId, true, "btle", "abcd_1_2");
        transmitterDAO.save(t);
        t = transmitterDAO.getTransmitterByMapIdTypeIdentifier(mapId, "btle", "abcd_1_2");

        b = new BtleTransmitter(t);
        transmitterDAO.save(b);

        final List<BtleTransmitter> iBeacons = transmitterDAO.getIBeacons("testmap");
        assertEquals(2, iBeacons.size());
        assertEquals("abcd", iBeacons.get(0).getUuid());
        assertEquals("abcd", iBeacons.get(1).getUuid());
        assertNotEquals(iBeacons.get(0).getMinorId(), iBeacons.get(1).getMinorId());

    }

    @Test
    public void testStoreWifiAP() {
        WifiAp w = new WifiAp();
        w.setMap_id(mapId);
        Transmitter t = new Transmitter(mapId, true, "wifi", "ap1");
        transmitterDAO.save(t);
        w.getNetworks().add(new NetworkNode(t));

        t = new Transmitter(mapId, true, "wifi", "ap2");
        transmitterDAO.save(t);
        w.getNetworks().add(new NetworkNode(t));

        w.setName("wifi1");
        transmitterDAO.save(w);

        w = new WifiAp();
        w.setMap_id(mapId);
        w.getNetworks().add(new NetworkNode(new Transmitter(mapId, true, "wifi", "ap3")));
        w.getNetworks().add(new NetworkNode(new Transmitter(mapId, true, "wifi", "ap4")));
        w.getNetworks().add(new NetworkNode(new Transmitter(mapId, true, "wifi", "ap5")));
        w.setName("wifi2");

        transmitterDAO.save(w);

        final List<WifiAp> aps = transmitterDAO.getWifiAps("testmap");
        assertEquals(2, aps.size());
        WifiAp w1, w2;
        if (aps.get(0).getName().equals("wifi1")) {
            w1 = aps.get(0);
            w2 = aps.get(1);
        } else {
            w2 = aps.get(0);
            w1 = aps.get(1);
        }

        assertEquals(3, w2.getNetworks().size());
        assertEquals(2, w1.getNetworks().size());
    }


}
