package de.tarent.sellfio.web;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by mley on 27.02.15.
 */
@Configuration
@EnableTransactionManagement
//list all subpackages of de.tarent.sellfio.web to exclude PersistenceConfig, which we do not want to load when testing.
@ComponentScan(value = {"de.tarent.sellfio.web.dao", "de.tarent.sellfio.web.model", "de.tarent.sellfio.web.rest",
        "de.tarent.sellfio.web.security", "de.tarent.sellfio.web.util"})
@PropertySources({
        @PropertySource(value = "classpath:config.properties")
})
public class TestConfig {

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        return template;

    }

    /**
     * Get the datasource
     *
     * @return DataSource object
     */
    @Bean(name = "dataSource")
    public DataSource dataSource() throws PropertyVetoException, ClassNotFoundException {
        Class.forName("org.h2.Driver");

        ComboPooledDataSource ds = new ComboPooledDataSource();
        ds.setDriverClass("org.h2.Driver");
        ds.setJdbcUrl("jdbc:h2:mem:inmem-testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
        ds.setUser("h2");
        ds.setPassword("h2");

        ds.setAcquireIncrement(5);
        ds.setMinPoolSize(5);
        ds.setMaxPoolSize(50);
        ds.setMaxIdleTime(1800);
        ds.setIdleConnectionTestPeriod(1800);
        ds.setMaxStatements(50);

        return ds;
    }

    /**
     * Creates a Sessionfactory
     *
     * @return SessionFactory object
     */
    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) throws SQLException {

        final LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
        sessionBuilder.scanPackages("de.tarent.sellfio.web.model");

        //needed to create schema in H2
        sessionBuilder.getProperties().setProperty("hibernate.hbm2ddl.auto", "create-drop");
        sessionBuilder.getProperties().setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");

        SessionFactory sessionFactory = sessionBuilder.buildSessionFactory();

        TableCreator.createTablesIfNeeded(dataSource.getConnection());

        return sessionFactory;
    }

    @Bean(name = "txName")
    public HibernateTransactionManager txName(DataSource dataSource, SessionFactory sessionFactory) throws IOException {
        final HibernateTransactionManager txName = new HibernateTransactionManager();
        txName.setSessionFactory(sessionFactory);
        txName.setDataSource(dataSource);
        return txName;
    }

}
