package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.AbstractTest;
import de.tarent.sellfio.web.model.AggregatedPoint;
import de.tarent.sellfio.web.model.InvioGeoPoint;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

/**
 * Created by mley on 16.09.15.
 */
public class AggregatedPointDAOTest extends AbstractTest {

    @Autowired
    private AggregatedPointDAO aggregatedPointDAO;

    @Autowired
    private MapService mapService;

    @Test
    public void test() {
        List<AggregatedPoint[][]> heatmaps = new ArrayList<>();

        AggregatedPoint[][] heatmap = new AggregatedPoint[1][2];
        heatmaps.add(heatmap);
        int mapId = mapService.mapNameToId("aggregatedpointdaotestmap");
        
        
        heatmap[0][0] = new AggregatedPoint(mapId, 0.1, 0.1, 1);
        heatmap[0][1] = new AggregatedPoint(mapId, 0.2, 0.1, 1);

        heatmap[0][0].visit(1);
        heatmap[0][1].visit(1);
        
        aggregatedPointDAO.saveHeatMaps(heatmaps);

        heatmap[0][0] = new AggregatedPoint(mapId, 0.1, 0.1, 2);
        heatmap[0][1] = new AggregatedPoint(mapId, 0.2, 0.1, 2);

        heatmap[0][0].visit(1);
        heatmap[0][1].visit(2);

        aggregatedPointDAO.saveHeatMaps(heatmaps);

        final Map<InvioGeoPoint, AggregatedPoint> aggregatedPointMap = aggregatedPointDAO.get(mapId, 1, 2);

        // both fields have 2 visits
        assertEquals(2, aggregatedPointMap.get(new InvioGeoPoint(0.1, 0.1)).getVisits());
        assertEquals(2, aggregatedPointMap.get(new InvioGeoPoint(0.2, 0.1)).getVisits());

        // first field has 1 unique visit, second field 2 unique visits
        assertEquals(1, aggregatedPointMap.get(new InvioGeoPoint(0.1, 0.1)).getNumberOfUniqueVisits());
        assertEquals(2, aggregatedPointMap.get(new InvioGeoPoint(0.2, 0.1)).getNumberOfUniqueVisits());

    }

    
    // Just a little test to prove that the new Map works as planned.
    @Test
    public void testMap() {
        Map<InvioGeoPoint, AggregatedPoint> pointMap = new HashMap<>();

        AggregatedPoint point1 = aggregatedPointDAO.getPointAt(pointMap, 0, 0, 1.0, 1.0);
        AggregatedPoint point2 = aggregatedPointDAO.getPointAt(pointMap, 0, 0, 2.0, 1.0);
        assert(point1 != point2);

        AggregatedPoint point3 = aggregatedPointDAO.getPointAt(pointMap, 0, 0, 1.0, 1.0);
        assert(point1 == point3);

        assert(pointMap.size() == 2);
    }

}
