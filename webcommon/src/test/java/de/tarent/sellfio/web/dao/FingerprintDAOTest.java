package de.tarent.sellfio.web.dao;

import de.tarent.sellfio.web.AbstractTest;
import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.Histogram;
import de.tarent.sellfio.web.model.InvioGeoPoint;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by mley on 29.10.15.
 */
public class FingerprintDAOTest extends AbstractTest {

    @Autowired
    FingerprintDAO fingerprintDAO;

    @Test
    public void test() throws FingerprintStoreException {


        List<Fingerprint> fps = new ArrayList<>();
        Histogram h = new Histogram();
        h.put("a", new HashMap<Integer, Float>());
        h.get("a").put(1, 0.5f);
        h.get("a").put(2, 0.5f);
        h.put("b", new HashMap<Integer, Float>());
        h.get("b").put(1, 1.0f);

        fps.add(new Fingerprint(h, new InvioGeoPoint(1.0, 1.0), "FP-1", 1, 1));

        h = new Histogram();
        h.put("a", new HashMap<Integer, Float>());
        h.get("a").put(1, 0.6f);
        h.get("a").put(2, 0.4f);
        h.put("b", new HashMap<Integer, Float>());
        h.get("b").put(1, 1.0f);

        fps.add(new Fingerprint(h, new InvioGeoPoint(2.0, 2.0), "FP-2", 1, 1));

        fingerprintDAO.replaceFingerprints("fpdaotest", "wifi", fps);

        final List<Fingerprint> fingerprints = fingerprintDAO.getByMapAndType("fpdaotest", "wifi", false);

        assertEquals(fps.get(0).getId(), fingerprints.get(0).getId());

        fps.remove(0);

        fingerprintDAO.replaceFingerprints("fpdaotest", "wifi", fps);

        assertEquals(1, fingerprintDAO.getByMapAndType("fpdaotest", "wifi", false).size());

    }
}
