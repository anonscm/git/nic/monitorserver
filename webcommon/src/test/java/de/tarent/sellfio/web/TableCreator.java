package de.tarent.sellfio.web;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by mley on 29.10.15.
 */

public class TableCreator {

    private static final AtomicBoolean createdTables = new AtomicBoolean(false);


    public static void createTablesIfNeeded(Connection connection) throws SQLException {
        synchronized (createdTables) {
            if (!createdTables.get()) {

                final Statement statement = connection.createStatement();

                statement.executeUpdate("CREATE TABLE fingerprint ( " +
                        "  id serial NOT NULL, " +
                        "  map_id integer NOT NULL, " +
                        "  name character varying(255), " +
                        "  lat double precision NOT NULL, " +
                        "  lon double precision NOT NULL, " +
                        "  fingerprint_type VARCHAR(4) NOT NULL, " +
                        "  scan_count INTEGER, " +
                        "  scan_time INTEGER, " +
                        "  CONSTRAINT fingerprint_pkey PRIMARY KEY (id), " +
                        "  CONSTRAINT fk_fingerprint_map FOREIGN KEY (map_id) " +
                        "      REFERENCES map (id) ON DELETE CASCADE )");

                statement.execute("CREATE TABLE histogram ( " +
                        "  id serial NOT NULL, " +
                        "  transmitter_id integer NOT NULL, " +
                        "  fingerprint_id integer NOT NULL, " +
                        "  CONSTRAINT histogram_pkey PRIMARY KEY (id), " +
                        "  CONSTRAINT fk_histogram_transmitter FOREIGN KEY (transmitter_id) " +
                        "      REFERENCES transmitter (id) ON DELETE CASCADE, " +
                        "  CONSTRAINT fk_histogram_fingerprint FOREIGN KEY (fingerprint_id) " +
                        "      REFERENCES fingerprint (id) ON DELETE CASCADE )");

                statement.execute("CREATE TABLE measurement ( " +
                        "  histogram_id integer NOT NULL, " +
                        "  strength smallint NOT NULL, " +
                        "  probability double precision NOT NULL, " +
                        "  CONSTRAINT measurement_pkey PRIMARY KEY (histogram_id, strength), " +
                        "  CONSTRAINT fk_measurement_histogram FOREIGN KEY (histogram_id) " +
                        "      REFERENCES histogram (id) ON DELETE CASCADE )");

                createdTables.set(true);
            }
        }
    }
}
