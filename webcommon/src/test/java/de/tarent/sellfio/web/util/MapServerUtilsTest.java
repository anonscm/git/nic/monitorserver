package de.tarent.sellfio.web.security.util;

import de.tarent.sellfio.web.model.BtleTransmitter;
import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.WifiAp;
import de.tarent.sellfio.web.util.MapServerUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;



public class MapServerUtilsTest {

    MapServerUtils mapServerUtils;


    @Before
    public void setup() throws IOException {

        mapServerUtils = Mockito.spy(new MapServerUtils());

        // This URL is normally set by spring, from the config.properties:
        mapServerUtils.setMapServerEndpoint("https://sellfiopoc.tarent.de/");


        doAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                String url = (String) invocation.getArguments()[0];
                String filename  = url.substring(url.lastIndexOf("/")+1);


                StringWriter sw = new StringWriter();
                InputStream inputStream = ClassLoader.getSystemResourceAsStream(filename);

                if(inputStream == null) {
                    throw new IOException("FAILZ. File not found");
                }

                org.apache.commons.io.IOUtils.copy(inputStream, sw);
                return sw.toString();
            }
        }).when(mapServerUtils).downloadTextFile(anyString());

    }



    @Test
    public void getTransmittersTest() throws IOException {
        List<WifiAp> aps = mapServerUtils.getWifiTransmitters("tarent3og");

        assertEquals(5, aps.size());

        List<BtleTransmitter> ibeacons = mapServerUtils.getBtleTransmitters("tarent3og");

        assertEquals(11, ibeacons.size());
    }


    @Test
    public void getScale() throws IOException {
        double scale = mapServerUtils.getScale("tarent3og");

        assertEquals(105.54, scale, 0.00001);
    }

    @Test(expected = IOException.class)
    public void getFail() throws IOException {
        mapServerUtils.getScale("tarent3ug");
    }

    @Test
    public void testGetOriginAndBoundingBox() throws IOException, SAXException, ParserConfigurationException {
        String tileMapResource = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "        <TileMap version=\"1.0.0\" tilemapservice=\"http://tms.osgeo.org/1.0.0\">\n" +
                "          <Title>temp8354848763110485663.tif</Title>\n" +
                "          <Abstract></Abstract>\n" +
                "          <SRS>EPSG:900913</SRS>\n" +
                "          <BoundingBox minx=\"50.70474740822668\" miny=\"7.04938099999999\" maxx=\"50.73833500000001\" maxy=\"7.07100345662320\"/>\n" +
                "          <Origin x=\"50.70474740822668\" y=\"7.04938099999999\"/>\n" +
                "          <TileFormat width=\"256\" height=\"256\" mime-type=\"image/png\" extension=\"png\"/>\n" +
                "          <TileSets profile=\"mercator\">\n" +
                "            <TileSet href=\"12\" units-per-pixel=\"38.21851413574219\" order=\"12\"/>\n" +
                "            <TileSet href=\"13\" units-per-pixel=\"19.10925706787109\" order=\"13\"/>\n" +
                "            <TileSet href=\"14\" units-per-pixel=\"9.55462853393555\" order=\"14\"/>\n" +
                "            <TileSet href=\"15\" units-per-pixel=\"4.77731426696777\" order=\"15\"/>\n" +
                "            <TileSet href=\"16\" units-per-pixel=\"2.38865713348389\" order=\"16\"/>\n" +
                "            <TileSet href=\"17\" units-per-pixel=\"1.19432856674194\" order=\"17\"/>\n" +
                "          </TileSets>\n" +
                "        </TileMap>\n";

        double[] result = mapServerUtils.getOriginAndBoundingBoxFromXml(tileMapResource);
        assertEquals(50.70474740822668, result[0], 0.000001);
        assertEquals(7.04938099999999, result[1], 0.000001);
        assertEquals(50.70474740822668, result[2], 0.000001);
        assertEquals(7.04938099999999, result[3], 0.000001);
        assertEquals(50.73833500000001, result[4], 0.000001);
        assertEquals(7.07100345662320, result[5], 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOriginAndBoundingBoxWithEmptyString() throws IOException, SAXException, ParserConfigurationException {
        mapServerUtils.getOriginAndBoundingBoxFromXml("");
    }
}
